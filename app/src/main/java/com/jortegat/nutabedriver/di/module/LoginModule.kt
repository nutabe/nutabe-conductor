package com.jortegat.nutabedriver.di.module

import com.jortegat.data.db.NutabeDatabase
import com.jortegat.data.login.datasource.local.LoginLocalDataSource
import com.jortegat.data.login.datasource.local.LoginLocalDataSourceImpl
import com.jortegat.data.login.datasource.remote.LoginApi
import com.jortegat.data.login.datasource.remote.LoginRemoteDataSource
import com.jortegat.data.login.datasource.remote.LoginRemoteDataSourceImpl
import com.jortegat.data.login.repository.LoginRepositoryImpl
import com.jortegat.domain.helper.PreferencesManager
import com.jortegat.domain.login.repository.LoginRepository
import com.jortegat.domain.login.usecase.LoginUseCase
import com.jortegat.domain.login.usecase.LoginUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit

@Module
@InstallIn(ApplicationComponent::class)
object LoginModule {

    @Provides
    fun provideLoginUseCase(loginRepository: LoginRepository): LoginUseCase {
        return LoginUseCaseImpl(loginRepository)
    }

    @Provides
    fun provideLoginRepository(
        loginRemoteDataSource: LoginRemoteDataSource,
        loginLocalDataSource: LoginLocalDataSource
    ): LoginRepository {
        return LoginRepositoryImpl(loginRemoteDataSource, loginLocalDataSource)
    }

    @Provides
    fun provideLoginRemoteDataSource(
        loginApi: LoginApi,
        preferencesManager: PreferencesManager
    ): LoginRemoteDataSource {
        return LoginRemoteDataSourceImpl(loginApi, preferencesManager)
    }

    @Provides
    fun provideLoginLocalDataSource(
        preferencesManager: PreferencesManager,
        nutabeDatabase: NutabeDatabase
    ): LoginLocalDataSource {
        return LoginLocalDataSourceImpl(preferencesManager, nutabeDatabase.userDAO())
    }

    @Provides
    fun provideLoginApi(retrofit: Retrofit): LoginApi {
        return retrofit.create(LoginApi::class.java)
    }
}