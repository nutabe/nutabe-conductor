package com.jortegat.nutabedriver.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.jortegat.base.helpers.InternetManager
import com.jortegat.base.helpers.PREF_NAME
import com.jortegat.base.helpers.UploadFilesManager
import com.jortegat.data.db.NutabeDatabase
import com.jortegat.data.interceptor.TokenInterceptor
import com.jortegat.domain.helper.PreferencesManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    private const val PING_URL = "https://www.google.com"

    @Provides
    @Singleton
    fun provideBackgroundDispatcher() = Dispatchers.IO

    @Provides
    @Singleton
    fun providePreferences(application: Application): SharedPreferences {
        return application.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun providePreferenceManager(sharedPreferences: SharedPreferences): PreferencesManager {
        return PreferencesManager(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.nutabe.com/auth/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideTokenInterceptor(preferencesManager: PreferencesManager): TokenInterceptor {
        return TokenInterceptor(preferencesManager)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        tokenInterceptor: TokenInterceptor,
        networkFlipperPlugin: NetworkFlipperPlugin
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(tokenInterceptor)
            .addNetworkInterceptor(FlipperOkhttpInterceptor(networkFlipperPlugin))
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideLocalDatabase(
        application: Application
    ): NutabeDatabase {
        return NutabeDatabase.getInstance(application.applicationContext)
    }

    @Provides
    @Singleton
    fun provideUploadFilesManager(application: Application): UploadFilesManager {
        return UploadFilesManager(application)
    }

    @Provides
    @Singleton
    fun provideFlipperNetworkPlugin(): NetworkFlipperPlugin {
        return NetworkFlipperPlugin()
    }

    @Provides
    fun provideNetworkRequest(): NetworkRequest {
        val builder = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideConnectivityManager(application: Application): ConnectivityManager {
        return application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    @Provides
    @Singleton
    fun provideInternetManager(
        connectivityManager: ConnectivityManager,
        networkRequest: NetworkRequest
    ): InternetManager {
        return InternetManager(connectivityManager, networkRequest, PING_URL)
    }
}