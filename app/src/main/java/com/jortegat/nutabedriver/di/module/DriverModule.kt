package com.jortegat.nutabedriver.di.module

import com.jortegat.base.helpers.InternetManager
import com.jortegat.base.helpers.UploadFilesManager
import com.jortegat.data.db.NutabeDatabase
import com.jortegat.data.driver.datasource.local.DriverLocalDataSource
import com.jortegat.data.driver.datasource.local.DriverLocalDataSourceImpl
import com.jortegat.data.driver.datasource.remote.DriverApi
import com.jortegat.data.driver.datasource.remote.DriverRemoteDataSource
import com.jortegat.data.driver.datasource.remote.DriverRemoteDataSourceImpl
import com.jortegat.data.driver.repository.DriverRepositoryImpl
import com.jortegat.domain.driver.repository.DriverRepository
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.driver.usecase.DriverUseCaseImpl
import com.jortegat.domain.helper.PreferencesManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit

@Module
@InstallIn(ApplicationComponent::class)
object DriverModule {

    @Provides
    fun provideDriverUseCase(driverRepository: DriverRepository): DriverUseCase {
        return DriverUseCaseImpl(driverRepository)
    }

    @Provides
    fun provideDriverRepository(
        driverRemoteDataSource: DriverRemoteDataSource,
        driverLocalDataSource: DriverLocalDataSource,
        internetManager: InternetManager
    ): DriverRepository {
        return DriverRepositoryImpl(driverRemoteDataSource, driverLocalDataSource, internetManager)
    }

    @Provides
    fun provideDriverRemoteDataSource(
        driverApi: DriverApi,
        preferencesManager: PreferencesManager,
        uploadFilesManager: UploadFilesManager
    ): DriverRemoteDataSource {
        return DriverRemoteDataSourceImpl(driverApi, preferencesManager, uploadFilesManager)
    }

    @Provides
    fun provideDriverLocalDataSource(
        preferencesManager: PreferencesManager,
        nutabeDatabase: NutabeDatabase
    ): DriverLocalDataSource {
        return DriverLocalDataSourceImpl(
            preferencesManager,
            nutabeDatabase.buildingSiteDAO(),
            nutabeDatabase.materialDAO(),
            nutabeDatabase.vehicleDAO(),
            nutabeDatabase.journeyDAO(),
            nutabeDatabase.locationsDAO()
        )
    }

    @Provides
    fun provideDriverApi(retrofit: Retrofit): DriverApi {
        return retrofit.create(DriverApi::class.java)
    }
}