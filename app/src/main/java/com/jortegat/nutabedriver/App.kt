package com.jortegat.nutabedriver

import android.app.Application
import android.content.Intent
import androidx.hilt.work.HiltWorkerFactory
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.android.utils.FlipperUtils
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.facebook.soloader.SoLoader
import com.jortegat.presentation.ui.home.HomeActivity
import com.onesignal.OneSignal
import dagger.hilt.android.HiltAndroidApp
import java.io.File
import javax.inject.Inject


@HiltAndroidApp
//class App : Application(), Configuration.Provider {
class App : Application() {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    @Inject
    lateinit var networkManager: NetworkFlipperPlugin

    override fun onCreate() {
        super.onCreate()
        // initFirebase()
        //initStheto()

        // in case read/write permissions are somehow still revoked by backup system, attempt to restore them here
        try {
            File("${applicationContext.dataDir}/lib-main").setWritable(true, true)
            File("${applicationContext.dataDir}/lib-main").setReadable(true, true)
        } catch (e: Exception) {
        }
        SoLoader.init(this, false)

        if (BuildConfig.DEBUG && FlipperUtils.shouldEnableFlipper(this)) {
            val client = AndroidFlipperClient.getInstance(this)
            client.addPlugin(networkManager)
            client.start()
        }

        initOneSignal()
        oneSignalNotificationListener()
    }

    private fun initOneSignal() {
        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)

        OneSignal.initWithContext(this)
        OneSignal.setAppId(ONESIGNAL_APP_ID)
    }

    private fun oneSignalNotificationListener() {
        OneSignal.setNotificationOpenedHandler { result ->

            val title = result.notification.title
            val body = result.notification.body
            val data = result.notification.additionalData
            val url = result.notification.launchURL
            val id = data.getString("notification_id")
            val isInvitationToAVehicle = url.contains("Curriculum") //showCurriculumFragment

            try {
                val packageName: String? = data.getString("package_name")
                if (packageName != null && packageName == "com.jortegat.nutabedriver") {
                    val intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("title", "driver $title")
                    intent.putExtra("body", body)
                    intent.putExtra("vehicleInvitation", isInvitationToAVehicle)
                    intent.putExtra("notificationId", id)
                    if(isInvitationToAVehicle){
                        val invitationId = data.getString("truckDriverInvitationId")
                        intent.putExtra("vehicleInvitationId", invitationId)
                    }
                    startActivity(intent)
                }
            } catch (e: RuntimeException) {

            }
        }
    }

/*    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()*/

    companion object {
        const val ONESIGNAL_APP_ID = "b3e090fd-7207-4fff-9243-ce1298f15102"
    }

}