package com.jortegat.domain.login.usecase

import com.jortegat.base.helpers.Result
import com.jortegat.base.models.RegistrationCode
import com.jortegat.base.models.User
import com.jortegat.base.models.UserRegistrationForm

interface LoginUseCase {

    suspend fun login(user: String, pass: String): Result<Unit>

    suspend fun register(registration: UserRegistrationForm): Result<Unit>

    suspend fun confirm(registrationCode: RegistrationCode): Result<Unit>

    suspend fun requestResetCode(identification: String): Result<Unit>

    suspend fun requestCodeAgain(identification: String)

    suspend fun reestablishPassword(user: String, pass: String, code: String) : Result<Unit>

    suspend fun getLoggedUser(): Result<User>

    suspend fun validateRole(): Result<Unit>

    fun setUserLogged()

    fun isUserLogged(): Boolean

    suspend fun requestToken(): Result<Unit>

    suspend fun requestPeriodicToken()
}