package com.jortegat.domain.login.usecase

import com.jortegat.base.helpers.Result
import com.jortegat.base.models.RegistrationCode
import com.jortegat.base.models.User
import com.jortegat.base.models.UserRegistrationForm
import com.jortegat.domain.login.repository.LoginRepository

class LoginUseCaseImpl(private val loginRepository: LoginRepository) : LoginUseCase {

    override suspend fun login(user: String, pass: String): Result<Unit> {
        return loginRepository.login(user, pass)
    }

    override suspend fun register(registration: UserRegistrationForm): Result<Unit> {
        return loginRepository.register(registration)
    }

    override suspend fun confirm(registrationCode: RegistrationCode): Result<Unit> {
        return loginRepository.confirm(registrationCode)
    }

    override suspend fun requestResetCode(identification: String): Result<Unit> {
        return loginRepository.requestResetCode(identification)
    }

    override suspend fun requestCodeAgain(identification: String) {
        return loginRepository.requestCodeAgain(identification)
    }

    override suspend fun reestablishPassword(
        user: String,
        pass: String,
        code: String
    ): Result<Unit> {
        return loginRepository.reestablishPassword(user, pass, code)
    }

    override suspend fun getLoggedUser(): Result<User> {
        return loginRepository.getLoggedUser()
    }

    override suspend fun validateRole(): Result<Unit> {
        return loginRepository.validateRole()
    }

    override fun setUserLogged() {
        loginRepository.setUserLogged()
    }

    override fun isUserLogged(): Boolean {
        return loginRepository.isUserLogged()
    }

    override suspend fun requestToken(): Result<Unit> {
        return loginRepository.requestToken()
    }

    override suspend fun requestPeriodicToken() {
        loginRepository.requestPeriodicToken()
    }
}