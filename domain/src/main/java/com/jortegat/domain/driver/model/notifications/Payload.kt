package com.jortegat.domain.driver.model.notifications

data class Payload(
    val journeyId: Int?,
    val truckDriverInvitationId: Int?,
    val vehicleId: String?
)