package com.jortegat.domain.driver.model.services

import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.OrderGuide

data class Journey(
    val journeyId: Int,
    val serviceCreationDate: String,
    val serviceOrder: String,
    val vehicleId: String,
    val vehicleType: String,
    val serviceType: String,
    val company: String,
    var destination: Place,
    var origin: Place,
    var material: JourneyMaterial,
    val workdayId: Int,
    var workdayName: String,
    val ownerId: String,
    var ownerName: String,
    var journeyStatus: String,
    var driverStatus: String,
    val date: String,
    var receiptName: String?,
    var receiptNumber: String?,
    var receiptDate: String?,
    var remissionDispatcher: String?,
    var remissionNumber: String?,
    var remissionDate: String?,
    var remissionLoad: Long?,
    var remissionLoadType: String = "M3",
    val orderGuide: OrderGuide?,
    var hasVisitedOrigin: Boolean = false,
    var hasVisitedDestination: Boolean = false,
    val serial: Int
)