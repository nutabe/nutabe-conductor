package com.jortegat.domain.driver.model.services.create_on_demand_service

data class DriverFrontLoad(
    val assistant: String,
    val createdAt: String,
    val id: Int,
    val isActive: Boolean,
    val isDump: Boolean,
    val latitude: Double,
    val location: String,
    val longitude: Double
)