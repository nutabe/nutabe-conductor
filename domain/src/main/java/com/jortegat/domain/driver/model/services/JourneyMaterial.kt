package com.jortegat.domain.driver.model.services

data class JourneyMaterial(
    val id: Int,
    var name: String,
    var type: String
) {
    override fun toString(): String {
        return name
    }
}