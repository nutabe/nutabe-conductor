package com.jortegat.domain.driver.model.notifications

data class Notification(
    val _id: String,
    val body: String,
    val createdAt: String,
    val identification: String,
    val link : NotificationLink?,
    val payload: Payload?,
    val status: String,
    val title: String
)