package com.jortegat.domain.driver.model.pending_invitation

data class PendingInvitation(
    val id: Int,
    val truckDriverIdentification: String,
    val vehicleId: String,
    val status: String,
    val createdAt: String
)