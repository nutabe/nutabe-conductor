package com.jortegat.domain.driver.model.notifications

data class NotificationLink(
    val _id: String,
    val androidUrl: String,
    val createdAt: String,
    val `package`: String,
    val type: String,
    val userType: String,
    val webUrl: String?
)