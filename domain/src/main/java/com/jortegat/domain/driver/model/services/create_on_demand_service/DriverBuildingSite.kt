package com.jortegat.domain.driver.model.services.create_on_demand_service

data class DriverBuildingSite(
    val driverFrontLoads: List<DriverFrontLoad>,
    val id: Int,
    val name: String
)