package com.jortegat.domain.driver.model.services.create_on_demand_service

data class OnDemandDocument(
    val comment: String?,
    val id: Int,
    val images: List<String>,
    val status: String,
    val type: String
)
