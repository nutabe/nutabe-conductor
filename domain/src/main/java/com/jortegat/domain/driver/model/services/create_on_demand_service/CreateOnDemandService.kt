package com.jortegat.domain.driver.model.services.create_on_demand_service

data class CreateOnDemandService(
    val buildingSiteId: Int,
    val date: String,
    val destination: Long,
    val destinationType: String,
    val driverId: String,
    val endHorometer: Int?,
    val endHour: String,
    val endKilometers: Int,
    val endPk: String?,
    val load: Double,
    val materialId: Int,
    val origin: Long,
    val originType: String,
    val serviceType: String,
    val startHorometer: Int?,
    val startHour: String,
    val startKilometers: Int,
    val startPk: String?,
    val vehicleId: String
)