package com.jortegat.domain.driver.usecase

import androidx.lifecycle.LiveData
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.*
import com.jortegat.base.models.on_demand_material.OnDemandMaterial
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverBuildingSite
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandService
import com.jortegat.domain.driver.repository.DriverRepository

class DriverUseCaseImpl(private val driverRepository: DriverRepository) : DriverUseCase {

    override suspend fun uploadDriverData(
        licenseCategory: String,
        frontPhotoPath: String,
        backPhotoPath: String
    ): Result<Unit> {
        return driverRepository.uploadDriverData(licenseCategory, frontPhotoPath, backPhotoPath)
    }

    override fun saveFrontalPhotoPath(key: String, path: String?) {
        driverRepository.saveFrontalPhotoPath(key, path)
    }

    override fun saveBackPhotoPath(key: String, path: String?) {
        return driverRepository.saveBackPhotoPath(key, path)
    }

    override fun saveOnDemandOriginFilePath(key: String, path: String?) {
        return driverRepository.saveOnDemandOriginFilePath(key, path)
    }

    override fun saveOnDemandDestinationFilePath(key: String, path: String?) {
        return driverRepository.saveOnDemandDestinationFilePath(key, path)
    }

    override fun saveOnDemandSupportFilePath(key: String, path: String?) {
        return driverRepository.saveOnDemandSupportFilePath(key, path)
    }

    override fun getFrontPhotoPath(): String {
        return driverRepository.getFrontPhotoPath()
    }

    override fun getBackPhotoPath(): String {
        return driverRepository.getBackPhotoPath()
    }

    override fun getOnDemandOriginFilePath(): String {
        return driverRepository.getOnDemandOriginFilePath()
    }

    override fun getOnDemandDestinationFilePath(): String {
        return driverRepository.getOnDemandDestinationFilePath()
    }

    override suspend fun getListOfJourneys(vehicleId: String): LiveData<List<Journey>> {
        return driverRepository.getListOfJourneys(vehicleId)
    }

    override suspend fun getListOfFinishedJourneys(vehicleId: String): Result<List<Journey>> {
        return driverRepository.getListOfFinishedJourneys(vehicleId)
    }

    override suspend fun acceptJourney(id: String): Result<Unit> {
        return driverRepository.acceptJourney(id)
    }

    override suspend fun rejectJourney(id: String): Result<Unit> {
        return driverRepository.rejectJourney(id)
    }

    override suspend fun startJourney(id: String): Result<Unit> {
        return driverRepository.startJourney(id)
    }

    override suspend fun resumeJourney(id: String): Result<Unit> {
        return driverRepository.resumeJourney(id)
    }

    override suspend fun finishJourney(id: String): Result<Unit> {
        return driverRepository.finishJourney(id)
    }

    override suspend fun sendLocation(
        journeyId: String,
        vehicleLocation: VehicleLocation
    ) {
        driverRepository.sendLocation(journeyId, vehicleLocation)
    }

    override suspend fun sendPendingLocations() {
        driverRepository.sendPendingLocations()
    }

    override suspend fun getJourneyDetail(id: String): Result<CustomJourney?> {
        return driverRepository.getJourneyDetail(id)
    }

    override suspend fun getJourneyDestination(id: String): Result<JourneyDestination> {
        return driverRepository.getJourneyDestination(id)
    }

    override suspend fun getStartedJourney(): Result<CustomJourney?> {
        return driverRepository.getStartedJourney()
    }

    override suspend fun getDriverInformation(): Result<Driver> {
        return driverRepository.getDriverInformation()
    }

    override suspend fun validateDocumentsAreFilled(): Result<Boolean> {
        return driverRepository.validateDocumentsAreFilled()
    }

    override suspend fun getOnDemandServices(): Result<List<OnDemandService>> {
        return driverRepository.getOnDemandServices()
    }

    override suspend fun getBuildingSites(): Result<List<DriverBuildingSite>> {
        return driverRepository.getBuildingSites()
    }

    override suspend fun getPlacesByType(type: String): Result<List<Place>> {
        return driverRepository.getPlacesByType(type)
    }

    override suspend fun getMaterials(): Result<List<OnDemandMaterial>> {
        return driverRepository.getMaterials()
    }

    override suspend fun createOnDemandService(service: CreateOnDemandService): Result<Int> {
        return driverRepository.createOnDemandService(service)
    }

    override suspend fun uploadOriginDocument(serviceId: Int): Result<Unit> {
        return driverRepository.uploadOriginDocument(serviceId)
    }

    override suspend fun uploadDestinationDocument(serviceId: Int): Result<Unit> {
        return driverRepository.uploadDestinationDocument(serviceId)
    }

    override suspend fun uploadSupportDocument(serviceId: Int): Result<Unit> {
        return driverRepository.uploadSupportDocument(serviceId)
    }

    override suspend fun getVehicle(): Vehicle? {
        return driverRepository.getVehicle()
    }

    override suspend fun updateJourneyInDb(journey: Journey) {
        driverRepository.updateJourneyInDb(journey)
    }

    override suspend fun updateJourneyInDb(
        journey: String,
        visitedOrigin: Boolean,
        visitedDestination: Boolean
    ) {
        driverRepository.updateJourneyInDb(journey, visitedOrigin, visitedDestination)
    }

    override suspend fun updateJourneyWithRemission(journey: Journey): Result<Unit> {
        return driverRepository.updateJourneyWithRemission(journey)
    }

    override suspend fun updateJourneyWithReceipt(journey: Journey): Result<Unit> {
        return driverRepository.updateJourneyWithReceipt(journey)
    }

    override suspend fun getNotifications(): Result<List<Notification>> {
        return driverRepository.getNotifications()
    }

    override suspend fun updateNotificationsToSeen(unreadNotificationsId: List<String>) {
        driverRepository.updateNotificationsToSeen(unreadNotificationsId)
    }

    override suspend fun handleVehicleInvitation(
        acceptInvitation: Boolean,
        vehicleInvitationId: String
    ): Result<Unit> {
        return driverRepository.handleVehicleInvitation(acceptInvitation, vehicleInvitationId)
    }

    override suspend fun checkIfDriverHasPendingInvitations(): Result<List<PendingInvitation>> {
        return driverRepository.checkIfDriverHasPendingInvitations()

    }
}