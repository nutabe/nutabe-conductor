package com.jortegat.domain.driver.model.services.create_on_demand_service

data class OnDemandService(
    var buildingSiteName: String?,
    val buildingSiteId: Int,
    var materialName: String?,
    val materialId: Int,
    val volume: Int,
    val serviceUnitType: String,
    val journeyDate: String,
    val remission : OnDemandDocument?,
    val receipt : OnDemandDocument?,
    val support : OnDemandDocument?
)
