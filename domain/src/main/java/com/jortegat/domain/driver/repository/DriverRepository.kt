package com.jortegat.domain.driver.repository

import androidx.lifecycle.LiveData
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.*
import com.jortegat.base.models.on_demand_material.OnDemandMaterial
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverBuildingSite
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandService

interface DriverRepository {

    suspend fun uploadDriverData(
        licenseCategory: String,
        frontPhotoPath: String,
        backPhotoPath: String
    ): Result<Unit>

    fun saveFrontalPhotoPath(key: String, path: String?)

    fun saveBackPhotoPath(key: String, path: String?)

    fun saveOnDemandOriginFilePath(key: String, path: String?)

    fun saveOnDemandDestinationFilePath(key: String, path: String?)

    fun saveOnDemandSupportFilePath(key: String, path: String?)

    fun getFrontPhotoPath(): String

    fun getBackPhotoPath(): String

    fun getOnDemandOriginFilePath(): String

    fun getOnDemandDestinationFilePath(): String

    suspend fun getListOfJourneys(vehicleId: String): LiveData<List<Journey>>

    suspend fun getListOfFinishedJourneys(vehicleId: String): Result<List<Journey>>

    suspend fun acceptJourney(id: String): Result<Unit>

    suspend fun rejectJourney(id: String): Result<Unit>

    suspend fun startJourney(id: String): Result<Unit>

    suspend fun resumeJourney(id: String): Result<Unit>

    suspend fun finishJourney(id: String): Result<Unit>

    suspend fun sendLocation(journeyId: String, vehicleLocation: VehicleLocation)

    suspend fun sendPendingLocations()

    suspend fun getJourneyDetail(id: String): Result<CustomJourney?>

    suspend fun getJourneyDestination(id: String): Result<JourneyDestination>

    suspend fun getStartedJourney(): Result<CustomJourney?>

    suspend fun getDriverInformation(): Result<Driver>

    suspend fun validateDocumentsAreFilled(): Result<Boolean>

    suspend fun getOnDemandServices(): Result<List<OnDemandService>>

    suspend fun getBuildingSites(): Result<List<DriverBuildingSite>>

    suspend fun getPlacesByType(type: String): Result<List<Place>>

    suspend fun getMaterials(): Result<List<OnDemandMaterial>>

    suspend fun createOnDemandService(service: CreateOnDemandService): Result<Int>

    suspend fun uploadOriginDocument(serviceId: Int): Result<Unit>

    suspend fun uploadDestinationDocument(serviceId: Int): Result<Unit>

    suspend fun uploadSupportDocument(serviceId: Int): Result<Unit>

    suspend fun getVehicle(): Vehicle?

    suspend fun updateJourneyInDb(journey: Journey)

    suspend fun updateJourneyInDb(
        journey: String,
        visitedOrigin: Boolean,
        visitedDestination: Boolean
    )

    suspend fun updateJourneyWithRemission(journey: Journey): Result<Unit>

    suspend fun updateJourneyWithReceipt(journey: Journey): Result<Unit>

    suspend fun getNotifications(): Result<List<Notification>>

    suspend fun updateNotificationsToSeen(unreadNotificationsId: List<String>)

    suspend fun handleVehicleInvitation(
        acceptInvitation: Boolean,
        vehicleInvitationId: String
    ): Result<Unit>

    suspend fun checkIfDriverHasPendingInvitations(): Result<List<PendingInvitation>>
}