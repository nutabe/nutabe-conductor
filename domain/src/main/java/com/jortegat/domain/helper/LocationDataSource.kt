package com.jortegat.domain.helper

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager

@SuppressLint("MissingPermission")
class LocationDataSource(
    context: Context,
    private val locationRetriever: LocationRetriever
) {
    private var locationManager: LocationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    fun startTracking() {
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            5000,
            10f,
            locationRetriever
        )
    }

    fun stopTracking() {
        locationManager.removeUpdates(locationRetriever)
    }
}