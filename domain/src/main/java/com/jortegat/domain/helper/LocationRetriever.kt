package com.jortegat.domain.helper

import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import com.jortegat.base.models.domain.VehicleLocation

class LocationRetriever : LocationListener {

    var currentLocation: ((VehicleLocation) -> Unit)? = null

    override fun onLocationChanged(location: Location) {
        currentLocation?.invoke(
            VehicleLocation(
                latitude = location.latitude.toString(),
                longitude = location.longitude.toString()
            )
        )
    }

    override fun onProviderEnabled(provider: String) {}

    override fun onProviderDisabled(provider: String) {}

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
}