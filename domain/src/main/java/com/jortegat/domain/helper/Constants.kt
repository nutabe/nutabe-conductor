package com.jortegat.domain.helper

//Documents and photos
const val FRONTAL_LICENSE_PHOTO_PATH = "FrontalLicensePhotoPath"
const val BACK_LICENSE_PHOTO_PATH = "BackLicensePhotoPath"
const val ORIGIN_FILE = "originFile"
const val DESTINATION_FILE = "destinationFile"
const val SUPPORT_FILE = "supportFile"
const val ON_DEMAND_FILE_TYPE = "fileType"

const val ON_DEMAND_ORIGIN_FILE_PATH = "OnDemandOriginFilePath"
const val ON_DEMAND_DESTINATION_FILE_PATH = "OnDemandDestinationFilePath"
const val ON_DEMAND_SUPPORT_FILE_PATH = "OnDemandSupportFilePath"

const val DOCUMENTS_FILLED = "documentsFilled"
const val JOURNEY_ID = "journeyId"
const val JOURNEY_ORIGIN_LATITUDE = "journeyOriginLatitude"
const val JOURNEY_ORIGIN_LONGITUDE = "journeyOriginLongitude"
const val JOURNEY_DESTINATION_LATITUDE = "journeyDestinationLatitude"
const val JOURNEY_DESTINATION_LONGITUDE = "journeyDestinationLongitude"
const val JOURNEY_HAS_VISITED_ORIGIN = "journeyHasVisitedOrigin"
const val JOURNEY_HAS_VISITED_DESTINATION = "journeyHasVisitedDestination"

const val QUARRY_TYPE = "QUARRY"
const val DUMP_TYPE = "DUMP"
const val FRONT_LOAD_TYPE = "FRONTLOAD"

const val VEHICLE_TYPE_SENCILLA = "Sencilla"
const val VEHICLE_TYPE_DOBLE_TROQUE = "Doble Troque"
const val VEHICLE_TYPE_TRACTO_CAMION = "Tracto camión"
