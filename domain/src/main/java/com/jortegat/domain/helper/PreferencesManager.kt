package com.jortegat.domain.helper

import android.content.SharedPreferences
import android.os.Bundle
import com.jortegat.base.helpers.ACCESS_TOKEN
import com.jortegat.base.helpers.IS_FIRST_USE
import com.jortegat.base.helpers.IS_USER_LOGGED
import com.jortegat.base.helpers.REFRESH_TOKEN
import javax.inject.Inject

class PreferencesManager @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private val editor = sharedPreferences.edit()

    //SETTERS

    fun saveCredentials(bundle: Bundle) {
        //https://github.com/auth0/JWTDecode.Android
        saveToken(bundle.getString(ACCESS_TOKEN, ""))
        saveRefreshToken(bundle.getString(REFRESH_TOKEN, ""))
    }

    private fun saveToken(accessToken: String) {
        editor.putString(ACCESS_TOKEN, accessToken).apply()
    }

    fun token() = sharedPreferences.getString(ACCESS_TOKEN, "")

    private fun saveRefreshToken(refreshToken: String) {
        editor.putString(REFRESH_TOKEN, refreshToken).apply()
    }

    fun refreshToken() = sharedPreferences.getString(REFRESH_TOKEN, "")

    fun setNotFirstUse() {
        editor.putBoolean(IS_FIRST_USE, false).apply()
    }

    fun isFirstUse() = sharedPreferences.getBoolean(IS_FIRST_USE, true)

    fun setUserLogged() {
        editor.putBoolean(IS_USER_LOGGED, true).apply()
    }

    fun isUserLogged() = sharedPreferences.getBoolean(IS_USER_LOGGED, false)

    fun saveFrontalPhotoPath(key: String, path: String?) {
        editor.putString(key, path).apply()
    }

    fun saveBackPhotoPath(key: String, path: String?) {
        editor.putString(key, path).apply()
    }

    fun saveOnDemandOriginFilePath(key: String, path: String?) {
        editor.putString(key, path).apply()
    }

    fun saveOnDemandDestinationFilePath(key: String, path: String?) {
        editor.putString(key, path).apply()
    }

    fun saveOnDemandSupportFilePath(key: String, path: String?) {
        editor.putString(key, path).apply()
    }

    fun getFrontPhotoPath(): String? {
        return sharedPreferences.getString(FRONTAL_LICENSE_PHOTO_PATH, "")
    }

    fun getBackPhotoPath(): String? {
        return sharedPreferences.getString(BACK_LICENSE_PHOTO_PATH, "")
    }

    fun setHasDocumentsFilled() {
        editor.putBoolean(DOCUMENTS_FILLED, true).apply()
    }

    fun validateDocumentsAreFilled(): Boolean {
        return sharedPreferences.getBoolean(DOCUMENTS_FILLED, false)
    }

    fun getOnDemandOriginPath(): String? {
        return sharedPreferences.getString(ON_DEMAND_ORIGIN_FILE_PATH, null)
    }

    fun getOnDemandDestinationPath(): String? {
        return sharedPreferences.getString(ON_DEMAND_DESTINATION_FILE_PATH, null)
    }

    fun getOnDemandSupportPath(): String? {
        return sharedPreferences.getString(ON_DEMAND_SUPPORT_FILE_PATH, null)
    }
}

/*
   fun saveCompanyId(companyId: String) {
     editor.putString(COMPANY_ID, companyId).apply()
 }

 fun saveBuildingSiteId(buildingSiteId: String) {
     editor.putString(BUILDING_SITE_ID, buildingSiteId).apply()
 }

 fun saveAttendantId(id: String) {
     editor.putString(ATTENDANT_BUILDING_ID, id).apply()
 }

 fun saveFrontalPhotoPath(key: String, path: String?) {
     editor.putString(key, path).apply()
 }

 fun saveBackPhotoPath(key: String, path: String?) {
     editor.putString(key, path).apply()
 }

 fun saveUserRoles(roles: MutableSet<String>) {
     sharedPreferences.edit().putStringSet(ROLES, roles).apply()
 }

 fun saveUserSetupCompleted() {
     sharedPreferences.edit().putBoolean(USER_BASIC_DATA_COMPLETED, true).apply()
 }

 fun saveRUTPhotoPath(path: String?) {
     sharedPreferences.edit().putString(RUT_PHOTO_PATH, path).apply()
 }

 fun saveCCPhotoPath(path: String?) {
     editor.putString(CC_PHOTO_PATH, path).apply()
 }
 fun updateDriverDataProgress(value: Int) {
     editor.putInt(DRIVER_DATA_PROGRESS, value).apply()
 }

  fun getBuildingSiteId(): String? {
     return sharedPreferences.getString(BUILDING_SITE_ID, "")
 }



    fun isLogged() = sharedPreferences.getString(ACCESS_TOKEN, "") != ""

 fun getRUTPhotoPath(): String? {
     return sharedPreferences.getString(RUT_PHOTO_PATH, "")
 }

 fun getCCPhotoPath(): String? {
     return sharedPreferences.getString(CC_PHOTO_PATH, "")
 }

 fun getCompanyId(): String? {
     return sharedPreferences.getString(COMPANY_ID, "")
 }

 fun getFrontPhotoPath(): String? {
     return sharedPreferences.getString(FRONTAL_LICENSE_PHOTO_PATH, "")
 }

 fun getBackPhotoPath(): String? {
     return sharedPreferences.getString(BACK_LICENSE_PHOTO_PATH, "")
 }

 fun isUserBasicSetupCompleted(): Boolean {
     return sharedPreferences.getBoolean(USER_BASIC_DATA_COMPLETED, false)
 }

 fun getDriverProgress(): Int {
     return sharedPreferences.getInt(DRIVER_DATA_PROGRESS, 7)
 }
 */

