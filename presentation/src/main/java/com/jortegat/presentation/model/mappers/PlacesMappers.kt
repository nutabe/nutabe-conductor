package com.jortegat.presentation.model.mappers

import com.jortegat.base.models.Place
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverFrontLoad
import com.jortegat.domain.helper.FRONT_LOAD_TYPE


object PlacesMappers {

    fun mapFrontLoadToPlace(frontLoad: DriverFrontLoad): Place {
        return frontLoad.run {
            Place(
                identification = id.toString(),
                address = location,
                businessName = location,
                latitude = latitude,
                longitude = longitude,
                isQuarry = false,
                isDump = isDump,
                isActive = isActive,
                type = FRONT_LOAD_TYPE,
                createdAt = createdAt
            )
        }
    }
}