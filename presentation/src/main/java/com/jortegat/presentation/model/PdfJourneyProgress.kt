package com.jortegat.presentation.model

data class PdfJourneyProgress(
    var remissionOrderDispatched: Boolean,
    var remissionDispatcher: String,
    var remissionNumber: String,
    var remissionTime: String = "",
    val orderReceived: Boolean,
    var orderReceiverName: String,
    var orderReceiverNumber: String,
    var orderReceiverTime: String = ""
)