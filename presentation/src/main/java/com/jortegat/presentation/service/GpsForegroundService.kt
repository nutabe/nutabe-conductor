package com.jortegat.presentation.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.core.app.NotificationCompat
import com.jortegat.base.helpers.InternetManager
import com.jortegat.base.models.domain.VehicleLocation
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.helper.*
import com.jortegat.presentation.R
import com.jortegat.presentation.ui.splash.SplashActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


@AndroidEntryPoint
class GpsForegroundService : Service(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    @Inject
    lateinit var driverUseCase: DriverUseCase

    @Inject
    lateinit var internetManager: InternetManager

    private val coroutineScope = CoroutineScope(Dispatchers.Default)
    private lateinit var locationDataSource: LocationDataSource
    private lateinit var locationRetriever: LocationRetriever
    private var location: VehicleLocation? = null

    private var journeyId: String? = null
    private var originLatitude: Double? = null
    private var destinationLatitude: Double? = null
    private var originLongitude: Double? = null
    private var destinationLongitude: Double? = null
    private var startTracking = false

    private var handler = Handler(Looper.myLooper()!!)

    private var hasVisitedOrigin = false
    private var hasVisitedDestination = false

    private val periodicUpdate = object : Runnable {
        override fun run() {
            handler.postDelayed(this, UPDATE_FREQUENCY)
            location?.let { vehicleLocation ->
                if (startTracking) {
                    if (hasVisitedOrigin) {
                        if (hasArrivedToDestination(vehicleLocation)) {
                            hasVisitedDestination = true
                            sendProgressUpdate()
                            sendLocation(vehicleLocation)

                            startTracking = false
                            locationDataSource.stopTracking()
                        }
                    } else {
                        if (hasArrivedToOrigin(vehicleLocation)) {
                            hasVisitedOrigin = true
                            sendProgressUpdate()
                        }
                    }
                    sendLocation(vehicleLocation)
                }
            }
        }
    }

    private fun sendLocation(vehicleLocation: VehicleLocation) {
        launch {
            journeyId?.let {
                driverUseCase.sendLocation(it, vehicleLocation)
            }
        }
    }

    fun stopService() {
        stopForeground(true)
        stopSelf()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action.equals(STOP_SERVICE_ACTION)) {
            locationDataSource.stopTracking()
            stopService()
            startTracking = false
        }

        intent?.extras?.run {
            journeyId = getString(JOURNEY_ID)
            originLatitude = getDouble(JOURNEY_ORIGIN_LATITUDE)
            originLongitude = getDouble(JOURNEY_ORIGIN_LONGITUDE)
            destinationLatitude = getDouble(JOURNEY_DESTINATION_LATITUDE)
            destinationLongitude = getDouble(JOURNEY_DESTINATION_LONGITUDE)
            hasVisitedOrigin = getBoolean(JOURNEY_HAS_VISITED_ORIGIN)
            hasVisitedDestination = getBoolean(JOURNEY_HAS_VISITED_DESTINATION)

            locationDataSource.startTracking()
            startTracking = true
        }


        createNotificationChannel()
        createAndShowNotification()
        return START_NOT_STICKY
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    private fun createAndShowNotification() {
        val notificationIntent = Intent(this, SplashActivity::class.java)
        var pendingIntent: PendingIntent? = null
        pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_MUTABLE)
        } else {
            PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT)
        }
        val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setContentTitle(NOTIFICATION_NAME)
            .setContentText(NOTIFICATION_TEXT)
            .setSmallIcon(R.drawable.ic_logo)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(NOTIFICATION_ID, notification)
    }

    private fun hasArrivedToDestination(location: VehicleLocation): Boolean {
        var hasArrived = false

        val destination = Location("destination")
        destinationLatitude?.let { lat ->
            destinationLongitude?.let { lon ->
                destination.latitude = lat
                destination.longitude = lon

                val myLocation = Location("myLocation")
                myLocation.latitude = location.latitude.toDouble()
                myLocation.longitude = location.longitude.toDouble()

                hasArrived = myLocation.distanceTo(destination) < 500
            }
        }
        return hasArrived
    }

    private fun hasArrivedToOrigin(location: VehicleLocation): Boolean {
        val origin = Location("origin")
        var hasArrived = false
        originLatitude?.let { lat ->
            originLongitude?.let { lon ->
                origin.latitude = lat
                origin.longitude = lon

                val myLocation = Location("myLocation")
                myLocation.latitude = location.latitude.toDouble()
                myLocation.longitude = location.longitude.toDouble()

                hasArrived = myLocation.distanceTo(origin) < 500
            }
        }
        return hasArrived
    }

    private fun sendProgressUpdate() {
        /*       val intent = Intent(INTENT_UPDATE_JOURNEY_PROGRESS)
               intent.putExtra(HAS_VISITED_ORIGIN_EXTRA, hasVisitedOrigin)
               intent.putExtra(HAS_VISITED_DESTINATION_EXTRA, hasVisitedDestination)
               intent.putExtra(JOURNEY_IN_PROGRESS_ID, journeyId!!)
               LocalBroadcastManager.getInstance(this).sendBroadcast(intent)*/
        launch {
            driverUseCase.updateJourneyInDb(
                journeyId!!,
                hasVisitedOrigin,
                hasVisitedDestination
            )
        }
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
            createAndShowNotification()
        }
        locationRetriever = LocationRetriever()
        locationRetriever.currentLocation = {
            location = it
        }
        locationDataSource = LocationDataSource(this, locationRetriever)
        handler.post(periodicUpdate)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        locationDataSource.stopTracking()
        job.cancel()
    }

    companion object {
        private const val UPDATE_FREQUENCY = 10000L
        private const val NOTIFICATION_ID = 1
        private const val NOTIFICATION_NAME = "Nutabe"
        private const val NOTIFICATION_CHANNEL_ID = "1"
        private const val NOTIFICATION_TEXT = "El servicio de Nutabe está activo"

        const val STOP_SERVICE_ACTION = "StopService"
    }
}