package com.jortegat.presentation.ui.base

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.User
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.launch

open class BaseDriverActivityViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val loginUseCase: LoginUseCase,
) : ViewModel() {

    private val _loggedUser = MediatorLiveData<Result<User>>()
    val loggedUser: LiveData<Result<User>> get() = _loggedUser

    fun getLoggedUser() {
        viewModelScope.launch {
            when (
                val result = loginUseCase.getLoggedUser()) {
                is Result.Success -> {
                    _loggedUser.postValue(Result.Success(result.data))
                }
                is Result.Error -> _loggedUser.postValue(Result.Error(result.exception))
            }
        }
    }
}