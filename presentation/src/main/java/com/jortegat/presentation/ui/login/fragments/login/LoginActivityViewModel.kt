package com.jortegat.presentation.ui.login.fragments.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel

class LoginActivityViewModel @ViewModelInject constructor() : ViewModel()