package com.jortegat.presentation.ui.home.show_on_demand_services

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandService
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.ShowDriverOnDemandServiceItemBinding

class OnDemandServiceAdapter(private var onDemandServices: List<OnDemandService>) :
    RecyclerView.Adapter<OnDemandServiceAdapter.OnDemandServiceHolder>() {

    var onOpenDocument: ((String) -> Unit)? = null

    private val binding: ShowDriverOnDemandServiceItemBinding get() = _binding!!
    private var _binding: ShowDriverOnDemandServiceItemBinding? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OnDemandServiceHolder {
        _binding =
            ShowDriverOnDemandServiceItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return OnDemandServiceHolder(binding)
    }

    override fun onBindViewHolder(holder: OnDemandServiceHolder, position: Int) {
        val item = onDemandServices[position]
        holder.bindOnDemandService(item)
    }

    override fun getItemCount(): Int {
        return onDemandServices.size
    }

    inner class OnDemandServiceHolder(private val v: ShowDriverOnDemandServiceItemBinding) :
        RecyclerView.ViewHolder(v.root) {

        private var service: OnDemandService? = null

        fun bindOnDemandService(journeyItem: OnDemandService) {
            this.service = journeyItem
            service?.let {

                /*    val journeyDateParsed = element.date.subSequence(0, 10)*/

                v.apply {

                    parentCardView.startAnimation(
                        AnimationUtils.loadAnimation(
                            v.root.context,
                            R.anim.anim_recycler
                        )
                    )

                    it.run {
                        val dateWithoutTime = journeyDate.split("T").first()
                        etBuildingSiteName.text = buildingSiteName
                        etBuildingSiteName.setSingleLine()
                        etMaterial.text = materialName
                        etMaterial.setSingleLine()
                        etVolume.text = volume.toString()
                        etJourneyDate.text = dateWithoutTime

                        tvRemission.setOnClickListener {
                            remission?.let { doc ->
                                onOpenDocument?.invoke(
                                    doc.images.first()
                                )
                            }
                        }
                        tvReceived.setOnClickListener {
                            receipt?.let { doc ->
                                onOpenDocument?.invoke(
                                    doc.images.first()
                                )
                            }
                        }
                        tvOtherDocuments.setOnClickListener {
                            support?.let { doc ->
                                onOpenDocument?.invoke(
                                    doc.images.first()
                                )
                            }
                        }
                    }
                }
            }
        }

    }

}
