package com.jortegat.presentation.ui.login.fragments.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.launch

class LoginFragmentViewModel @ViewModelInject constructor(private val loginUseCase: LoginUseCase) :
    ViewModel() {

    private val _login = MediatorLiveData<Result<Unit>>()
    val login: LiveData<Result<Unit>> get() = _login

    private val _roleValidation = MediatorLiveData<Result<Unit>>()
    val roleValidation: LiveData<Result<Unit>> get() = _roleValidation

    fun login(user: String, pass: String) {
        viewModelScope.launch {
            when (
                val result = loginUseCase.login(user, pass)) {
                is Result.Success -> {
                    _login.postValue(Result.Success(Unit))
                }
                is Result.Error -> _login.postValue(Result.Error(result.exception))
            }
        }
    }

    /**
     * After a successful login, it is necessary to validate if both local and remote roles property
     * for the user are the same
     *
     *  1) request role to server, if any role is return, then validate if it is the same as local
     *  if they are the same then continue, otherwise, deny access.
     *
     *  2)after request to server, if none role is return, then send the local role to the server,
     *  then continue
     *
     */

    fun validateRole() {
        viewModelScope.launch {
            when (val result = loginUseCase.validateRole()) {
                is Result.Success -> {
                    _roleValidation.postValue(Result.Success(Unit))
                }
                is Result.Error -> _roleValidation.postValue(Result.Error(result.exception))
            }
        }
    }

    /**
     * Before going to the HomeActivity, save in preference that the user had a successful logged in
     * */
    fun setUserLogged() {
        loginUseCase.setUserLogged()
    }

    fun requestToken() {
        viewModelScope.launch {
            loginUseCase.requestToken()
        }
    }

}
