package com.jortegat.presentation.ui.home.show_notifications

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.jortegat.base.enums.NotificationStatus
import com.jortegat.base.helpers.*
import com.jortegat.base.models.ui.ConfirmationDialogInformation
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentNotificationsBinding
import com.jortegat.presentation.ui.base.BaseDriverFragment
import com.jortegat.presentation.ui.home.OnNotificationActionListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationsFragment : BaseDriverFragment() {

    private var notifications: List<Notification> = mutableListOf()
    private var pendingInvitations: List<PendingInvitation> = mutableListOf()
    private val viewModel: NotificationsFragmentViewModel by viewModels()

    private val binding: FragmentNotificationsBinding get() = _binding!!
    private var _binding: FragmentNotificationsBinding? = null

    private var listener: OnNotificationActionListener? = null

    private lateinit var adapter: NotificationsAdapter
    private lateinit var pendingInvitationAdapter: PendingInvitationAdapter

    private var title: String? = null
    private var body: String? = null
    private var vehicleInvitation: Boolean = false
    private var notificationId: String? = null
    private var vehicleInvitationId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.run {
            title = getString("title", "")
            body = getString("body")
            vehicleInvitation = getBoolean("vehicleInvitation")
            notificationId = getString("notificationId")
            vehicleInvitationId = getString("vehicleInvitationId", null)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.startShimmerAnimation()

        observers()
        requestToken()
    }

    private fun requestToken() {
        if (internetManager.isInternetOn()) {
            viewModel.requestToken()
        } else {
            showNoInternetLayout()
            showNoInternetMessage(requireView())
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnNotificationActionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnNotificationActionListener")
        }
        //update the bell icon to show no dot (no unseen notifications)
        listener?.onNotificationAction(false)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun observers() {
        viewModel.notificationsResult.observe(viewLifecycleOwner, ::onNotificationsResult)
        viewModel.vehicleInvitationResult.observe(viewLifecycleOwner, ::onVehicleInvitationResponse)
        viewModel.tokenResult.observe(viewLifecycleOwner, ::onTokenResponse)
        internetManager.observe(viewLifecycleOwner, ::onInternetChange)
        viewModel.pendingInvitations.observe(viewLifecycleOwner, ::onPendingInvitationsResult)
    }


    private fun getNotifications() {
        if (internetManager.isInternetOn()) {
            viewModel.getNotifications()
        } else {
            showNoInternetLayout()
            showNoInternetMessage(requireView())
        }
    }

    private fun onTokenResponse(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                if (internetManager.isInternetOn()) {
                    getNotifications()
                } else {
                    showNoInternetLayout()
                    showNoInternetMessage(requireView())
                }
            }
            doIfError {
                showErrorContainer()
                showMessage(it.message.toString(), requireView())
            }
        }
    }

    private fun onInternetChange(hasInternet: Boolean?) {
        if (hasInternet == true) {
            /*try to get the notifications only if they are not visible already*/
            if (binding.noInternetLayout.isVisible) viewModel.requestToken()
        }
    }

    private fun onVehicleInvitationResponse(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                showMessage(
                    "Respuesta enviada con exito",
                    requireView()
                )
                checkIfDriverHasPendingInvitations()
            }
            doIfError {
                showMessage(
                    it.message.toString(),
                    requireView()
                )
            }
        }
    }

    private fun onPendingInvitationsResult(result: Result<List<PendingInvitation>>) {
        with(result) {
            doIfSuccess { list ->
                if (list != null && list.isEmpty()) {
                    hidePendingInvitationsLayout()
                } else {
                    list?.apply {
                        showPendingInvitationsLayout()
                        pendingInvitations = this
                        pendingInvitationAdapter = PendingInvitationAdapter(this)
                        pendingInvitationAdapter.onAcceptClick = ::onAcceptInvitation
                        pendingInvitationAdapter.onRejectClick = ::onRejectInvitation
                        binding.recyclerPendingInvitations.adapter = pendingInvitationAdapter
                    }
                }
            }
            doIfError {
                showMessage(
                    it.message.toString(),
                    requireView()
                )
                showErrorContainer()
            }
        }
    }

    private fun onNotificationsResult(result: Result<List<Notification>>) {
        with(result) {
            doIfSuccess { list ->
                //checkIfDriverHasPushNotificationInvitations is when a push notification arrives to the device, the user
                //tap on it and it will open this fragment.
                checkIfDriverHasPushNotificationInvitations()

                //checkIfDriverHasPendingInvitations are the invitations that maybe the driver hasn't checked or ignored.
                checkIfDriverHasPendingInvitations()
                if (list != null) {
                    notifications = list.filter { it.link?.type != "DRIVER_INVITATION" }

                    if(notifications.isNotEmpty()) {
                        showNotificationsRecycler()
                        adapter = NotificationsAdapter(notifications)
                        adapter.onAcceptClick = ::onAcceptInvitation
                        adapter.onRejectClick = ::onRejectInvitation
                        binding.recyclerNotifications.adapter = adapter

                        updateNotificationsToSeen(notifications.filter { it.status == NotificationStatus.UNREAD.serverMessage }
                            .map { it._id })
                    } else {
                        showNoNotificationsLayout()
                    }
                }
            }
            doIfError {
                showMessage(
                    it.message.toString(),
                    requireView()
                )
                showErrorContainer()
            }
        }
    }

    private fun checkIfDriverHasPendingInvitations() {
        viewModel.checkIfDriverHasPendingInvitations()
    }

    private fun onAcceptInvitation(invitationId: String) {
        viewModel.handleVehicleInvitation(true, invitationId)
    }

    private fun onRejectInvitation(invitationId: String) {
        viewModel.handleVehicleInvitation(false, invitationId)
    }

    private fun checkIfDriverHasPushNotificationInvitations() {
        if (vehicleInvitation) {
            vehicleInvitation = false
            val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->
                dialog.dismiss()
                handleVehicleInvitation(ACCEPT_INVITATION)
            }
            val onReject: (DialogInterface, Int) -> Unit = { dialog, _ ->
                dialog.dismiss()
                handleVehicleInvitation(REJECT_INVITATION)
            }
            val information = ConfirmationDialogInformation(
                title = "Tienes una nueva invitación",
                message = body ?: "Has sido invitado a ser el conductor de un vehículo.",
                negativeButton = "Rechazar",
                positiveButton = "Aceptar",
                icon = R.drawable.ic_exclamation_circle,
                positiveCallback = onConfirm,
                negativeCallback = onReject
            )
            context?.showConfirmationDialog(information)
        }
    }

    private fun handleVehicleInvitation(acceptInvitation: Boolean) {
        vehicleInvitationId?.let { viewModel.handleVehicleInvitation(acceptInvitation, it) }
    }

    private fun updateNotificationsToSeen(unreadNotificationsId: List<String>) {
        viewModel.updateNotificationsToSeen(unreadNotificationsId)
    }

    private fun showNoNotificationsLayout() {
        binding.stopShimmerAnimation()
        binding.noNotificationsLayout.isVisible = true
        binding.notificationsLayout.isVisible = false
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = false
    }

    private fun showNotificationsRecycler() {
        binding.stopShimmerAnimation()
        binding.noNotificationsLayout.isVisible = false
        binding.notificationsLayout.isVisible = true
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = false
    }

    private fun showPendingInvitationsLayout() {
        binding.noNotificationsLayout.isVisible = false
        binding.pendingInvitationsLayout.isVisible = true
    }

    private fun hidePendingInvitationsLayout(){
        binding.pendingInvitationsLayout.isVisible = false
    }

    private fun showNoInternetLayout() {
        binding.stopShimmerAnimation()
        binding.noNotificationsLayout.isVisible = false
        binding.notificationsLayout.isVisible = false
        binding.layoutError.isVisible = false
        binding.noInternetLayout.isVisible = true
    }

    private fun showErrorContainer() {
        binding.stopShimmerAnimation()
        binding.noNotificationsLayout.isVisible = false
        binding.notificationsLayout.isVisible = false
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = true
    }

    private fun FragmentNotificationsBinding.startShimmerAnimation() {
        shimmerNotificationLayout.startShimmer()
        shimmerNotificationLayout.visibility = View.VISIBLE
    }

    private fun FragmentNotificationsBinding.stopShimmerAnimation() {
        shimmerNotificationLayout.stopShimmer()
        shimmerNotificationLayout.isVisible = false
    }

    companion object {
        const val ACCEPT_INVITATION = true
        const val REJECT_INVITATION = false
    }
}
