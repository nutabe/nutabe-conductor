package com.jortegat.presentation.ui.home.show_services

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jortegat.base.enums.JourneyStatus
import com.jortegat.base.helpers.*
import com.jortegat.base.models.domain.Destination
import com.jortegat.base.models.domain.Driver
import com.jortegat.base.models.domain.JourneyDestination
import com.jortegat.base.models.ui.ConfirmationDialogInformation
import com.jortegat.base.models.ui.RegistrationDialogConfirmation
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentActiveServicesBinding
import com.jortegat.presentation.ui.home.StartNavigationFragment
import com.jortegat.presentation.ui.home.show_services.service_guide.ServiceGuideDialogManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.sort_date.*

private const val ARG_JOURNEYS = "journeys"

@AndroidEntryPoint
class ActiveServicesFragment : StartNavigationFragment() {

    private val viewModel: ShowActiveServicesViewModel by viewModels()

    private lateinit var adapter: JourneyAdapter
    private var journeys: List<Journey> = mutableListOf()
    private var currentSelectedJourney: Journey? = null
    private var driverName: String? = null

    private var serviceGuideDialogManager: ServiceGuideDialogManager? = null

    private val binding: FragmentActiveServicesBinding get() = _binding!!
    private var _binding: FragmentActiveServicesBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val jsonString = it.getString(ARG_JOURNEYS)
            val listType = object : TypeToken<List<Journey>>() {}.type
            journeys = Gson().fromJson(jsonString, listType)
        }

        viewModel.getDriverInformation()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentActiveServicesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.initView()
        listeners()
        setObservers()
    }

    private fun FragmentActiveServicesBinding.initView() {
        if (journeys.isNotEmpty()) {
            tvSortByTitle.isVisible = true
            sortCardViewButton.isVisible = true
            rbDescendent.isSelected = true

            viewNoJourneys.isVisible = false
            textViewNoJourneys.isVisible = false

            svFilter.isVisible = true
            svFilter.clearFocus()
            svFilter.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.let { keyword ->
                        adapter = JourneyAdapter(journeys.filter { it.date.contains(keyword) })
                        adapter.onAcceptClick = ::acceptJourney
                        adapter.onRejectClick = ::rejectJourney
                        adapter.onStartClick = ::startJourney
                        adapter.onResumeClick = ::resumeJourney
                        adapter.onFinishClick = ::finishJourney
                        adapter.onPdfClick = ::showPdfOrderJourney
                        adapter.onJourneyClick = ::continueJourney
                        recyclerActiveServiceView.adapter = adapter
                    }
                    return true
                }

            })

        } else {
            viewNoJourneys.isVisible = true
            textViewNoJourneys.isVisible = true
            tvSortByTitle.isVisible = false
            sortCardViewButton.isVisible = false
            svFilter.isVisible = false
        }

        adapter = JourneyAdapter(journeys)
        adapter.onAcceptClick = ::acceptJourney
        adapter.onRejectClick = ::rejectJourney
        adapter.onStartClick = ::startJourney
        adapter.onResumeClick = ::resumeJourney
        adapter.onFinishClick = ::finishJourney
        adapter.onPdfClick = ::showPdfOrderJourney
        adapter.onJourneyClick = ::continueJourney
        recyclerActiveServiceView.adapter = adapter
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun listeners() {

        rbAscendent.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    binding.svFilter.setQuery("", true)
                    rbAscendent.isSelected = true
                    rbDescendent.isSelected = false
                    adapter = JourneyAdapter(journeys.sortedByDescending { journey -> journey.date }
                        .toMutableList())
                    adapter.onAcceptClick = ::acceptJourney
                    adapter.onRejectClick = ::rejectJourney
                    adapter.onStartClick = this::startJourney
                    adapter.onResumeClick = ::resumeJourney
                    adapter.onFinishClick = ::finishJourney
                    adapter.onPdfClick = ::showPdfOrderJourney
                    adapter.onJourneyClick = ::continueJourney
                    binding.recyclerActiveServiceView.adapter = adapter

                }
            }
            return@setOnTouchListener true
        }

        rbDescendent.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    binding.svFilter.setQuery("", true)
                    rbAscendent.isSelected = false
                    rbDescendent.isSelected = true

                    adapter = JourneyAdapter(journeys.sortedBy { journey -> journey.date }
                        .toMutableList())
                    adapter.onAcceptClick = ::acceptJourney
                    adapter.onRejectClick = ::rejectJourney
                    adapter.onStartClick = this::startJourney
                    adapter.onResumeClick = ::resumeJourney
                    adapter.onFinishClick = ::finishJourney
                    adapter.onPdfClick = ::showPdfOrderJourney
                    adapter.onJourneyClick = ::continueJourney
                    binding.recyclerActiveServiceView.adapter = adapter
                }
            }
            return@setOnTouchListener true
        }
    }

    private fun validateLocationPermission(isResumeJourney: Boolean) {
        val locationPermissionDialog = buildLocationConfirmationDialog(isResumeJourney)
        requireContext().showConfirmationDialog(locationPermissionDialog)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE_START_JOURNEY -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    currentSelectedJourney?.let {
                        viewModel.startJourney(it.journeyId.toString())
                    }
                } else {
                    showMessage(
                        "Para iniciar el viaje es necesario acceder a la ubicación del dispositivo. Por favor comunicate con tu contratista para mayor información.",
                        requireView()
                    )

                }
                return
            }
            LOCATION_PERMISSION_REQUEST_CODE_RESUME_JOURNEY -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    currentSelectedJourney?.let {
                        viewModel.resumeJourney(it.journeyId.toString())
                    }
                } else {
                    showMessage(
                        "Para continuar el viaje es necesario acceder a la ubicación del dispositivo. Por favor comunicate con tu contratista para mayor información.",
                        requireView()
                    )

                }
                return
            }
        }
    }

    private fun buildLocationConfirmationDialog(isResumeJourney: Boolean): ConfirmationDialogInformation {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            // Check location permission
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    if (isResumeJourney) LOCATION_PERMISSION_REQUEST_CODE_START_JOURNEY else LOCATION_PERMISSION_REQUEST_CODE_START_JOURNEY
                )
            } else {
                currentSelectedJourney?.let {
                    if (isResumeJourney) viewModel.resumeJourney(it.journeyId.toString())
                    else viewModel.startJourney(it.journeyId.toString())
                }
            }
        }

        val onBack: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            showMessage(
                "Para iniciar el viaje es necesario acceder a la ubicación del dispositivo. Por favor comunicate con tu contratista para mayor información.",
                requireView()
            )
        }

        return ConfirmationDialogInformation(
            title = "Nutabe Conductor necesita acceder a tu ubicación",
            message = "Mientras el viaje esté en progreso, necesitamos acceder a tu ubicación aun cuando la aplicación se encuentre cerrada o en segundo plano.",
            negativeButton = requireContext().getString(R.string.no_go_back),
            positiveButton = requireContext().getString(R.string.yes_confirm),
            icon = R.drawable.ic_exclamation_circle,
            positiveCallback = onConfirm,
            negativeCallback = onBack
        )
    }

    private fun acceptJourney(journey: Journey) {
        setCurrentSelectedJourney(journey)
        viewModel.acceptJourney(journey.journeyId.toString())
    }

    private fun rejectJourney(journey: Journey) {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            setCurrentSelectedJourney(journey)
            viewModel.rejectJourney(journey.journeyId.toString())

        }
        val information = ConfirmationDialogInformation(
            title = getString(R.string.important_rejecting_journey),
            message = getString(R.string.reject_journey_confirmation_message),
            negativeButton = getString(R.string.no_go_back),
            positiveButton = getString(R.string.yes_reject),
            icon = R.drawable.ic_exclamation_circle,
            positiveCallback = onConfirm
        )
        context?.showConfirmationDialog(information)
    }

    private fun startJourney(journey: Journey) {
        if (journeys.count { it.journeyStatus == "PROCESS" } == 0) {
            setCurrentSelectedJourney(journey)
            if (journey.journeyStatus == JourneyStatus.ACCEPTED.serverMessage) {
                viewModel.startJourney(journey.journeyId.toString())
            }
        } else showMessage(
            "No puedes tener mas de un viaje en progreso al tiempo.",
            requireView()
        )
    }

    private fun resumeJourney(journey: Journey) {
        if (journeys.count { it.journeyStatus == "PROCESS" } == 0) {
            setCurrentSelectedJourney(journey)
            viewModel.resumeJourney(journey.journeyId.toString())
        } else showMessage(
            "No puedes tener mas de un viaje en progreso al tiempo.",
            requireView()
        )
    }

    private fun setCurrentSelectedJourney(journey: Journey) {
        currentSelectedJourney = journey
    }

    private fun continueJourney(journey: Journey) {
        setCurrentSelectedJourney(journey)
        if (journey.journeyStatus == JourneyStatus.PROCESS.name) navigateJourney()
    }

    private fun finishJourney(journey: Journey) {
        if (journey.hasVisitedOrigin) {
            if (journey.hasVisitedDestination) {
                if (journey.remissionNumber.isNullOrEmpty()
                        .not() && journey.receiptNumber.isNullOrEmpty().not()
                ) {
                    if (internetManager.isInternetOn()) {
                        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->
                            dialog.dismiss()
                            viewModel.finishJourney(journey.journeyId.toString())
                            stopService()
                        }
                        val information = ConfirmationDialogInformation(
                            title = getString(R.string.important_finishing_journey),
                            message = getString(R.string.finish_journey_confirmation_message),
                            negativeButton = getString(R.string.no_go_back),
                            positiveButton = getString(R.string.yes_finish),
                            icon = R.drawable.ic_exclamation_circle,
                            positiveCallback = onConfirm
                        )
                        context?.showConfirmationDialog(information)
                    } else {
                        showNoInternetMessage(requireView())
                    }
                } else {
                    showGuideNotCompletedDialog(journey)
                }
            } else {
                showMessage(
                    "Este vehiculo no ha visitado el punto de control de destino.",
                    requireView()
                )
            }
        } else {
            showMessage(
                "Este vehiculo no ha visitado el punto de control de inicio ni destino.",
                requireView()
            )
        }
    }

    private fun showPdfOrderJourney(journey: Journey) {
        setCurrentSelectedJourney(journey)
        //make sure we send any pending information saved because the device had no internet before.
        //we need to do this before opening the guide in case the driver wasnt to send the remission or receipt and the server still
        // doesnt now the vehicle already passed the origin and destination
        viewModel.sendPendingLocations()

        serviceGuideDialogManager = ServiceGuideDialogManager(
            requireContext(),
            journey,
            driverName
        )

        serviceGuideDialogManager?.onRemissionDone =
            {
                if (internetManager.isInternetOn()) {
                    viewModel.updateJourneyWithRemission(it)
                } else showNoInternetMessage(requireView())
            }
        serviceGuideDialogManager?.onReceiptDone =
            {
                if (internetManager.isInternetOn()) {
                    viewModel.updateJourneyWithReceipt(it)
                } else showNoInternetMessage(requireView())
            }

        val dialog = serviceGuideDialogManager?.getDialog()
        dialog?.show()
    }

    private fun setObservers() {
        viewModel.journeyListResult.observe(viewLifecycleOwner, ::showJourneyListResult)
        viewModel.acceptJourneyResult.observe(viewLifecycleOwner, ::showAcceptJourneyResult)
        viewModel.rejectJourneyResult.observe(viewLifecycleOwner, ::showRejectJourneyResult)
        viewModel.startJourneyResult.observe(viewLifecycleOwner, ::showStartJourneyResult)
        viewModel.resumeJourneyResult.observe(viewLifecycleOwner, ::showResumeJourneyResult)
        viewModel.finishJourneyResult.observe(viewLifecycleOwner, ::showFinishJourneyResult)
        viewModel.driverInformationResult.observe(viewLifecycleOwner, ::onDriverInformationResult)
        viewModel.onRemissionDone.observe(viewLifecycleOwner, ::onRemissionDoneResult)
        viewModel.onReceiptDone.observe(viewLifecycleOwner, ::onReceiptDoneResult)
    }

    private fun showJourneyListResult(result: List<Journey>) {
        if (result.isNotEmpty()) {
            val journeysFromViewModel =
                result.filter { it.journeyStatus != "FINISHED" && it.journeyStatus != "TIMEOUT" && it.journeyStatus != "DENIED" }
            adapter.updateJourneyList(journeysFromViewModel)

            currentSelectedJourney?.let {
                val journeyInProgress =
                    result.first() { it.journeyId == it.journeyId }
                serviceGuideDialogManager?.updateJourneyPdfGuide(
                    journeyInProgress.hasVisitedOrigin,
                    journeyInProgress.hasVisitedDestination
                )
            }
        }
    }

    private fun showAcceptJourneyResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                //updateJourneyStatus(JourneyStatus.ACCEPTED.serverMessage)
                showMessage("El viaje fue aceptado exitosamente", requireView())
            }
            doIfError { showMessage(it.message.toString(), requireView()) }
        }
    }

    private fun showRejectJourneyResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                //updateJourneyStatus(JourneyStatus.DENIED.serverMessage)
                showMessage("El viaje fue rechazado exitosamente", requireView())
            }
            doIfError { showMessage(it.message.toString(), requireView()) }
        }
    }

    private fun showFinishJourneyResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                showMessage("El viaje fue finalizado exitosamente", requireView())
            }
            doIfError { showMessage(it.message.toString(), requireView()) }
        }
    }

    private fun showStartJourneyResult(result: Result<Unit>) {
        hideLoader()
        with(result) {
            doIfSuccess {
                navigateJourney()
            }
            doIfError { showMessage(it.message.toString(), requireView()) }
        }
    }

    private fun showResumeJourneyResult(result: Result<Unit>) {
        hideLoader()
        with(result) {
            doIfSuccess {
                navigateJourney()
            }
            doIfError { showMessage(it.message.toString(), requireView()) }
        }
    }

    private fun onRemissionDoneResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                serviceGuideDialogManager?.run {
                    remissionIsDone()
                    showMessage("Remisión guardado con exito", binding.root)
                }
            }
            doIfError {
                serviceGuideDialogManager?.run {
                    showMessage(it.message.toString(), binding.root)
                }
            }
        }
    }

    private fun onReceiptDoneResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                serviceGuideDialogManager?.run {
                    receiptIsDone()
                    showMessage("Recibo guardado con exito", binding.root)
                }
            }
            doIfError {
                serviceGuideDialogManager?.run {
                    showMessage(it.message.toString(), binding.root)
                }
            }
        }
    }

    private fun navigateJourney() {
        currentSelectedJourney?.run {

            val journeyCanBeFinished =
                this.hasVisitedOrigin && this.hasVisitedDestination

            viewModel.sendPendingLocations()

            if (!journeyCanBeFinished) {
                val journeyDestinationDetail = JourneyDestination(
                    Destination(
                        origin.address,
                        origin.businessName,
                        origin.latitude,
                        origin.longitude
                    ),
                    Destination(
                        destination.address,
                        destination.businessName,
                        destination.latitude,
                        destination.longitude
                    )
                )

                startGpsService(
                    journeyId.toString(),
                    journeyDestinationDetail.origin,// this will be used in the service to calculate distance to origin
                    journeyDestinationDetail.destination,// this will be used in the service to calculate distance to destination
                    hasVisitedOrigin,
                    hasVisitedDestination,
                    R.id.action_showServicesFragment_to_nav_home
                )
            } else showJourneyReadyConfirmationDialog()
        }
    }

    private fun showGuideNotCompletedDialog(journey: Journey) {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
            dialogInterface.dismiss()
            showPdfOrderJourney(journey)
        }
        context?.showRegistrationDialogConfirmation(
            RegistrationDialogConfirmation(
                title = "Guia de viaje",
                message = "Los datos en la guia no están completamente diligenciados. Te redireccionaremos a la guia para completar el proceso.",
                positiveButton = "Aceptar",
                positiveCallback = onConfirm
            )
        )
    }

    private fun showJourneyReadyConfirmationDialog() {
        if (currentSelectedJourney?.remissionNumber.isNullOrEmpty()
                .not() && currentSelectedJourney?.receiptNumber.isNullOrEmpty()
                .not()
        ) {
            //no need to show the alert because it was shown before from the else in this same function
        } else {
            val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            context?.showRegistrationDialogConfirmation(
                RegistrationDialogConfirmation(
                    title = "Ahora puedes finalizar este viaje",
                    message = "Asegurate de que los datos en la guia sean correctos y estén completamente diligenciados antes.",
                    positiveButton = "Aceptar",
                    positiveCallback = onConfirm
                )
            )
        }
    }

    private fun onDriverInformationResult(result: Result<Driver>) {
        hideLoader()
        with(result) {
            doIfSuccess { driver ->
                driver?.let { driverName = it.name }
            }
            doIfError {
            }
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        @JvmStatic
        fun newInstance(journeys: String) = ActiveServicesFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_JOURNEYS, journeys)
            }
        }

        private const val LOADER_DELAY = 500L

        var onUpdateMarkersOnMap: ((JourneyDestination) -> Unit)? = null
    }
}