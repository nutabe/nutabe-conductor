package com.jortegat.presentation.ui.base

import androidx.activity.viewModels
import com.jortegat.base.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class BaseDriverActivity : BaseActivity() {

    private val viewModel: BaseDriverActivityViewModel by viewModels()

}