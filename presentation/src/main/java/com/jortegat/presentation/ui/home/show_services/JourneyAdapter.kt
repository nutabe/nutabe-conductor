package com.jortegat.presentation.ui.home.show_services

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.jortegat.base.enums.JourneyStatus
import com.jortegat.base.helpers.convertUTCDateToLocal
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.ShowDriverServiceItemBinding
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*

class JourneyAdapter(private var journeys: List<Journey>) :
    RecyclerView.Adapter<JourneyAdapter.JourneyHolder>() {

    var onAcceptClick: ((Journey) -> Unit)? = null
    var onRejectClick: ((Journey) -> Unit)? = null
    var onStartClick: ((Journey) -> Unit)? = null
    var onResumeClick: ((Journey) -> Unit)? = null
    var onFinishClick: ((Journey) -> Unit)? = null
    var onPdfClick: ((Journey) -> Unit)? = null
    var onJourneyClick: ((Journey) -> Unit)? = null

    private lateinit var binding: ShowDriverServiceItemBinding

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): JourneyHolder {
        binding =
            ShowDriverServiceItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return JourneyHolder(binding)
    }

    override fun onBindViewHolder(holder: JourneyHolder, position: Int) {
        val item = journeys[position]
        holder.bindJourney(item)
    }

    override fun getItemCount(): Int {
        return journeys.size
    }

    fun updateJourneyList(journeysUpdated: List<Journey>) {
        journeys = journeysUpdated
        notifyDataSetChanged()
    }

    inner class JourneyHolder(private val v: ShowDriverServiceItemBinding) :
        RecyclerView.ViewHolder(v.root), View.OnClickListener {

        private var journey: Journey? = null

        fun bindJourney(journeyItem: Journey) {
            this.journey = journeyItem
            journey?.let { journeyElement ->
                val journeyDateParsed = journeyElement.date
                val serviceDateParsed = journeyElement.serviceCreationDate.subSequence(0, 10)

                v.apply {

                    cardViewDriverServiceParent.startAnimation(
                        AnimationUtils.loadAnimation(
                            v.root.context,
                            R.anim.anim_recycler
                        )
                    )


                    pdfButton.setOnClickListener { onPdfClick?.invoke(journeyElement) }
                    rejectJourneyButton.setOnClickListener { onRejectClick?.invoke(journeyElement) }
                    startJourneyButton.setOnClickListener {
                        if (journeyElement.journeyStatus == JourneyStatus.PAUSED.serverMessage) {
                            onResumeClick?.invoke(journeyElement)
                        } else {
                            onStartClick?.invoke(journeyElement)
                        }
                    }
                    finishJourneyButton.setOnClickListener { onFinishClick?.invoke(journeyElement) }
                    acceptJourneyButton.setOnClickListener { onAcceptClick?.invoke(journeyElement) }
                    cardViewDriverServiceParent.setOnClickListener {
                        onJourneyClick?.invoke(
                            journeyElement
                        )
                    }

                    journeyWorkdayTextView.text = journeyElement.workdayName
                    journeyDate.text = journeyDateParsed
                    serviceOrderTextView.text = journeyElement.serviceOrder
                    originAddressTextView.text = journeyElement.origin.businessName
                    destinationAddressTextView.text = journeyElement.destination.businessName
                    serviceDateTextView.text = serviceDateParsed

                    journeyDayTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        val formatter: DateTimeFormatter =
                            DateTimeFormatter.ofPattern("yyyy-MM-dd")
                        val localDate: LocalDate = LocalDate.parse(journeyDateParsed, formatter)
                        localDate.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault())
                    } else {
                        ""
                    }

                    when (journeyElement.journeyStatus) {
                        JourneyStatus.PENDING.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.PENDING.appMessage,
                                R.color.colorYellow
                            )

                            pdfButton.isEnabled = false
                            pdfButtonTextView.isEnabled = false
                            acceptJourneyButton.isEnabled = true
                            acceptJourneyTexView.isEnabled = true
                            rejectJourneyButton.isEnabled = true
                            rejectButtonTextView.isEnabled = true
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = false
                            finishButtonTextView.isEnabled = false

                            acceptJourneyButton.isVisible = true
                            acceptJourneyTexView.isVisible = true
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = false
                            finishButtonTextView.isVisible = false
                        }
                        JourneyStatus.ACCEPTED.serverMessage -> {
                            if (journeyElement.driverStatus == "PENDING") {
                                updateState(
                                    journeyStateName,
                                    JourneyStatus.PENDING.appMessage,
                                    R.color.colorYellow
                                )

                                pdfButton.isEnabled = false
                                pdfButtonTextView.isEnabled = false
                                acceptJourneyButton.isEnabled = true
                                acceptJourneyTexView.isEnabled = true
                                rejectJourneyButton.isEnabled = true
                                rejectButtonTextView.isEnabled = true
                                startJourneyButton.isEnabled = false
                                startButtonTextView.isEnabled = false
                                finishJourneyButton.isEnabled = false
                                finishButtonTextView.isEnabled = false

                                acceptJourneyButton.isVisible = true
                                acceptJourneyTexView.isVisible = true
                                startJourneyButton.isVisible = false
                                startButtonTextView.isVisible = false
                                finishJourneyButton.isVisible = false
                                finishButtonTextView.isVisible = false
                            } else {

                                updateState(
                                    journeyStateName,
                                    JourneyStatus.ACCEPTED.appMessage,
                                    R.color.colorAccentDark
                                )

                                acceptJourneyButton.isVisible = false
                                acceptJourneyTexView.isVisible = false
                                startJourneyButton.isVisible = true
                                startButtonTextView.isVisible = true
                                finishJourneyButton.isVisible = false
                                finishButtonTextView.isVisible = false

                                pdfButton.isEnabled = false
                                pdfButtonTextView.isEnabled = false
                                acceptJourneyButton.isEnabled = false
                                acceptJourneyTexView.isEnabled = false
                                rejectJourneyButton.isEnabled = true
                                rejectButtonTextView.isEnabled = true
                                startButtonTextView.isEnabled = true
                                startButtonTextView.text = "Iniciar"
                                startJourneyButton.isEnabled = true
                                finishJourneyButton.isEnabled = false
                                finishButtonTextView.isEnabled = false
                            }
                        }
                        JourneyStatus.DENIED.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.DENIED.appMessage,
                                R.color.colorRed
                            )

                            pdfButton.isEnabled = false
                            pdfButtonTextView.isEnabled = false
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = false
                            finishButtonTextView.isEnabled = false

                            acceptJourneyButton.isVisible = true
                            acceptJourneyTexView.isVisible = true
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = false
                            finishButtonTextView.isVisible = false
                        }
                        JourneyStatus.STARTED.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.STARTED.appMessage,
                                R.color.colorAccentDark
                            )

                            pdfButton.isEnabled = false
                            pdfButtonTextView.isEnabled = false
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = true
                            finishButtonTextView.isEnabled = true

                            acceptJourneyButton.isVisible = false
                            acceptJourneyTexView.isVisible = false
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = true
                            finishButtonTextView.isVisible = true
                        }
                        JourneyStatus.PROCESS.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.PROCESS.appMessage,
                                R.color.colorAccentDark
                            )

                            pdfButton.isEnabled = true
                            pdfButtonTextView.isEnabled = true
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = true
                            finishButtonTextView.isEnabled = true

                            acceptJourneyButton.isVisible = false
                            acceptJourneyTexView.isVisible = false
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = true
                            finishButtonTextView.isVisible = true
                        }
                        JourneyStatus.PAUSED.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.PAUSED.appMessage,
                                R.color.colorAccentDark
                            )

                            pdfButton.isEnabled = true
                            pdfButtonTextView.isEnabled = true
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = true
                            startButtonTextView.isEnabled = true
                            finishJourneyButton.isEnabled = false
                            finishButtonTextView.isEnabled = false

                            acceptJourneyButton.isVisible = false
                            acceptJourneyTexView.isVisible = false
                            startJourneyButton.isVisible = true
                            startButtonTextView.isVisible = true
                            startButtonTextView.text = "Reanudar"
                            finishJourneyButton.isVisible = false
                            finishButtonTextView.isVisible = false
                        }
                        JourneyStatus.TIMEOUT.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.TIMEOUT.appMessage,
                                R.color.colorRed
                            )

                            pdfButton.isEnabled = false
                            pdfButtonTextView.isEnabled = false
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = false
                            finishButtonTextView.isEnabled = false

                            acceptJourneyButton.isVisible = true
                            acceptJourneyTexView.isVisible = true
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = false
                            finishButtonTextView.isVisible = false
                        }
                        JourneyStatus.FINISHED.serverMessage -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.FINISHED.appMessage,
                                R.color.colorRed
                            )

                            pdfButton.isEnabled = true
                            pdfButtonTextView.isEnabled = true
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = false
                            finishButtonTextView.isEnabled = false

                            acceptJourneyButton.isVisible = false
                            acceptJourneyTexView.isVisible = false
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = true
                            finishButtonTextView.isVisible = true
                        }
                        else -> {
                            updateState(
                                journeyStateName,
                                JourneyStatus.getAppMessage(journeyElement.journeyStatus),
                                R.color.colorRed
                            )

                            pdfButton.isEnabled = false
                            acceptJourneyButton.isEnabled = false
                            acceptJourneyTexView.isEnabled = false
                            rejectJourneyButton.isEnabled = false
                            rejectButtonTextView.isEnabled = false
                            startJourneyButton.isEnabled = false
                            startButtonTextView.isEnabled = false
                            finishJourneyButton.isEnabled = false
                            finishButtonTextView.isEnabled = false

                            acceptJourneyButton.isVisible = false
                            acceptJourneyTexView.isVisible = false
                            startJourneyButton.isVisible = false
                            startButtonTextView.isVisible = false
                            finishJourneyButton.isVisible = true
                            finishButtonTextView.isVisible = true
                        }
                    }
                }
            }
        }

        private fun updateState(textview: TextView, state: String, color: Int) {
            textview.text = "ESTADO: $state"
            textview.setTextColor(ContextCompat.getColor(textview.context, color))
        }

        override fun onClick(v: View) {}
    }
}
