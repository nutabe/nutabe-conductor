package com.jortegat.presentation.ui.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.domain.helper.PreferencesManager
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.launch

class SplashActivityViewModel @ViewModelInject constructor(
    private val loginUseCase: LoginUseCase,
    private val preferencesManager: PreferencesManager
) : ViewModel() {

    fun isUserLogged() = loginUseCase.isUserLogged()

    fun isFirstUse(): Boolean {
        return preferencesManager.isFirstUse()
    }

    fun setNotFirstUse() {
        preferencesManager.setNotFirstUse()
    }

    fun requestToken() {
        viewModelScope.launch {
            loginUseCase.requestToken()
        }
    }
}