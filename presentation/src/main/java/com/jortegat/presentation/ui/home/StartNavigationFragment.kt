package com.jortegat.presentation.ui.home

import android.content.Intent
import androidx.annotation.IdRes
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.jortegat.base.models.domain.Destination
import com.jortegat.domain.helper.*
import com.jortegat.presentation.R
import com.jortegat.presentation.service.GpsForegroundService
import com.jortegat.presentation.service.GpsForegroundService.Companion.STOP_SERVICE_ACTION
import com.jortegat.presentation.ui.base.BaseDriverFragment

open class StartNavigationFragment : BaseDriverFragment() {

    fun startGpsService(
        journeyId: String,
        origin: Destination?,
        destination: Destination?,
        hasVisitedOrigin: Boolean = false,
        hasVisitedDestination: Boolean = false,
        @IdRes navigationAction: Int? = null //can be null when it is call from the mapFragment already
    ) {
        val intent = Intent(requireContext(), GpsForegroundService::class.java).apply {
            putExtra(JOURNEY_ID, journeyId)
        }
        origin?.run {
            val originLatitude = latitude
            val originLongitude = longitude
            intent.putExtra(JOURNEY_ORIGIN_LATITUDE, originLatitude)
            intent.putExtra(JOURNEY_ORIGIN_LONGITUDE, originLongitude)
        }
        destination?.run {
            val destinationLatitude = latitude
            val destinationLongitude = longitude
            intent.putExtra(JOURNEY_DESTINATION_LATITUDE, destinationLatitude)
            intent.putExtra(JOURNEY_DESTINATION_LONGITUDE, destinationLongitude)
            intent.putExtra(JOURNEY_HAS_VISITED_ORIGIN, hasVisitedOrigin)
            intent.putExtra(JOURNEY_HAS_VISITED_DESTINATION, hasVisitedDestination)
        }

        ContextCompat.startForegroundService(requireContext(), intent)

        navigationAction?.let {
            requireActivity().findNavController(R.id.nav_host_fragment).navigate(it)
        }
    }

    fun stopService() {
        val intent = Intent(requireContext(), GpsForegroundService::class.java).apply {
            action = STOP_SERVICE_ACTION
        }
        requireActivity().startService(intent)
    }
}