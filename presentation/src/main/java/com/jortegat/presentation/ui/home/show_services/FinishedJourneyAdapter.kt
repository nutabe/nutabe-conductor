package com.jortegat.presentation.ui.home.show_services

import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.ShowDriverServiceItemBinding
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*

class FinishedJourneyAdapter(private var journeys: List<Journey>) :
    RecyclerView.Adapter<FinishedJourneyAdapter.JourneyHolder>() {

    private val binding: ShowDriverServiceItemBinding get() = _binding!!
    private var _binding: ShowDriverServiceItemBinding? = null

    var onOpenDocument: ((String) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): JourneyHolder {
        _binding =
            ShowDriverServiceItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return JourneyHolder(binding)
    }

    override fun onBindViewHolder(holder: JourneyHolder, position: Int) {
        val item = journeys[position]
        holder.bindJourney(item)
    }

    override fun getItemCount(): Int {
        return journeys.size
    }

    fun updateJourneyList(journeysUpdated: List<Journey>) {
        journeys = journeysUpdated
        notifyDataSetChanged()
    }

    inner class JourneyHolder(private val v: ShowDriverServiceItemBinding) :
        RecyclerView.ViewHolder(v.root), View.OnClickListener {

        private var journey: Journey? = null

        private val blueColor = ContextCompat.getColor(binding.root.context, R.color.colorPrimary)
        private val greyColor = ContextCompat.getColor(binding.root.context, R.color.colorGrey)

        init {
            v.root.setOnClickListener(this)
        }

        fun bindJourney(journeyItem: Journey) {
            this.journey = journeyItem
            journey?.let {
                val journeyDateParsed = it.date.subSequence(0, 10)
                val serviceDateParsed = it.serviceCreationDate.subSequence(0, 10)
                v.apply {

                    cardViewDriverServiceParent.startAnimation(
                        AnimationUtils.loadAnimation(
                            v.root.context,
                            R.anim.anim_recycler
                        )
                    )

                    journeyWorkdayTextView.text = it.workdayName
                    journeyDate.text = journeyDateParsed
                    serviceOrderTextView.text = it.serviceOrder
                    originAddressTextView.text = it.origin.businessName
                    destinationAddressTextView.text = it.destination.businessName
                    serviceDateTextView.text = serviceDateParsed

                    journeyDayTextView.text =
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            val formatter: DateTimeFormatter =
                                DateTimeFormatter.ofPattern("yyyy-MM-dd")
                            val localDate: LocalDate = LocalDate.parse(journeyDateParsed, formatter)
                            localDate.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault())
                        } else {
                            ""
                        }

                    if (it.orderGuide != null) {
                        if (it.orderGuide!!.images.isNotEmpty()) {
                            pdfButton.isEnabled = true
                            pdfButtonTextView.setTextColor(blueColor)
                        } else {
                            pdfButton.isEnabled = false
                            pdfButtonTextView.setTextColor(greyColor)
                        }
                    } else {
                        pdfButton.isEnabled = false
                        pdfButtonTextView.setTextColor(greyColor)
                    }

                    pdfButton.setOnClickListener { view ->
                        it.orderGuide?.let { doc ->
                            onOpenDocument?.invoke(
                                doc.images.first()
                            )
                        }
                    }

                    updateState(binding.journeyStateName, "FINALIZADO", R.color.colorRed)

                    pdfButton.isEnabled = true
                    pdfButtonTextView.isEnabled = true
                    acceptJourneyButton.isEnabled = false
                    acceptJourneyTexView.isEnabled = false
                    rejectJourneyButton.isEnabled = false
                    rejectButtonTextView.isEnabled = false
                    startJourneyButton.isEnabled = false
                    startButtonTextView.isEnabled = false
                    finishJourneyButton.isEnabled = false
                    finishButtonTextView.isEnabled = false

                    acceptJourneyButton.isVisible = false
                    acceptJourneyTexView.isVisible = false
                    startJourneyButton.isVisible = false
                    startButtonTextView.isVisible = false
                    finishJourneyButton.isVisible = true
                    finishButtonTextView.isVisible = true
                }
            }
        }

        private fun updateState(textview: TextView, state: String, color: Int) {
            textview.text = "ESTADO: $state"
            textview.setTextColor(ContextCompat.getColor(textview.context, color))
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
        }
    }

    companion object {
        const val FINISHED = "FINISHED"
    }
}