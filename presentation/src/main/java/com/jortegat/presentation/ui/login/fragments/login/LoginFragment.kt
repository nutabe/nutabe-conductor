package com.jortegat.presentation.ui.login.fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.showMessage
import com.jortegat.base.helpers.showNoInternetMessage
import com.jortegat.base.ui.alert_dialog.SupportDialogManager
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.LoginFragmentBinding
import com.jortegat.presentation.ui.base.BaseDriverFragment
import com.onesignal.OneSignal
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONException
import org.json.JSONObject

@AndroidEntryPoint
class LoginFragment : BaseDriverFragment() {

    private val viewModel: LoginFragmentViewModel by viewModels()

    private var _binding: LoginFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {}

        binding.listeners()
        observers()
    }

    private fun observers() {
        viewModel.login.observe(viewLifecycleOwner, Observer(::validateRole))

        viewModel.roleValidation.observe(viewLifecycleOwner, Observer(::openHomeActivity))
    }

    private fun validateRole(result: Result<Unit>) {
        when (result) {
            is Result.Success -> {
                viewModel.validateRole()
            }
            is Result.Error -> {
                showMessage(result.exception.message.toString(), requireView())
                hideLoader()
            }
        }
    }

    private fun openHomeActivity(result: Result<Unit>) {
        when (result) {
            is Result.Success -> {
                viewModel.setUserLogged()
                hideLoader()
                viewModel.requestToken()
                findNavController().navigate(R.id.action_loginFragment_to_homeActivity)
            }
            is Result.Error -> {
                showMessage(result.exception.message.toString(), requireView())
                hideLoader()
            }
        }
    }

    private fun setDriverTag() {
        OneSignal.sendTag("user_type", "driver")
    }

    private fun setUserIdToOneSignal(user: String) {
        OneSignal.setExternalUserId(
            user,
            object : OneSignal.OSExternalUserIdUpdateCompletionHandler {
                override fun onSuccess(results: JSONObject) {
                    try {
                        if (results.has("push") && results.getJSONObject("push").has("success")) {
                            val isPushSuccess = results.getJSONObject("push").getBoolean("success")
                            OneSignal.onesignalLog(
                                OneSignal.LOG_LEVEL.VERBOSE,
                                "Set external user id for push status: $isPushSuccess"
                            )
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    try {
                        if (results.has("email") && results.getJSONObject("email").has("success")) {
                            val isEmailSuccess =
                                results.getJSONObject("email").getBoolean("success")
                            OneSignal.onesignalLog(
                                OneSignal.LOG_LEVEL.VERBOSE,
                                "Set external user id for email status: $isEmailSuccess"
                            )
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    try {
                        if (results.has("sms") && results.getJSONObject("sms").has("success")) {
                            val isSmsSuccess = results.getJSONObject("sms").getBoolean("success")
                            OneSignal.onesignalLog(
                                OneSignal.LOG_LEVEL.VERBOSE,
                                "Set external user id for sms status: $isSmsSuccess"
                            )
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(error: OneSignal.ExternalIdError) {
                    // The results will contain channel failure statuses
                    // Use this to detect if external_user_id was not set and retry when a better network connection is made
                    OneSignal.onesignalLog(
                        OneSignal.LOG_LEVEL.VERBOSE,
                        "Set external user id done with error: $error"
                    )
                }
            })

    }

    private fun LoginFragmentBinding.listeners() {
        buttonGoToRegistration.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }

        textViewResetPassword.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToReestablishFragment())
        }

        buttonLogin.setOnClickListener(::login)

        buttonSupport
            .setOnClickListener(::showSupportDialog)
    }

    private fun login(view: View) {
        val user = binding.editTextUser.text.toString()
        val pass = binding.editTextPass.text.toString()
        if (validateFields(user, pass)) {
            if (internetManager.isInternetOn()) {
                showLoader()
                setUserIdToOneSignal(user)
                setUserIdToFirebase(user)
                setDriverTag()
                viewModel.login(user, pass)
            } else {
                showNoInternetMessage(requireView())
            }
        } else {
            showMessage(getString(R.string.all_fields_are_require_error), requireView())
        }
    }

    private fun setUserIdToFirebase(user: String) {
        FirebaseCrashlytics.getInstance().setUserId(user)
    }

    private fun showSupportDialog(view: View) {
        val dialogManager = SupportDialogManager(requireContext())
        val supportDialog = dialogManager.getSupportDialog()
        dialogManager.setOnClickListeners { supportDialog.dismiss() }
        supportDialog.show()
    }

    private fun validateFields(user: String, pass: String) = user.isNotEmpty() && pass.isNotEmpty()
}