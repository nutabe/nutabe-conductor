package com.jortegat.presentation.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.jortegat.base.R
import com.jortegat.presentation.ui.home.HomeActivity
import com.jortegat.presentation.ui.login.fragments.login.LoginActivity
import com.jortegat.presentation.ui.welcomecards.WelcomeCardsActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private val viewModel: SplashActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val nextScreen = if (isFirstUse()) {
            setNotFirstUse()
            WelcomeCardsActivity::class.java
        } else {
            if (isUserLogged()) {
                HomeActivity::class.java
            } else {
                LoginActivity::class.java
            }
        }
        CoroutineScope(Dispatchers.Main).launch {
            if(nextScreen == HomeActivity::class.java){
                viewModel.requestToken()
            }
            delay(600)
            startActivity(Intent(applicationContext, nextScreen))
        }
    }

    private fun isUserLogged(): Boolean {
        return viewModel.isUserLogged()
    }

    private fun isFirstUse(): Boolean {
        return viewModel.isFirstUse()
    }

    private fun setNotFirstUse() {
        viewModel.setNotFirstUse()
    }
}