package com.jortegat.presentation.ui.base

import android.Manifest
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import com.jortegat.base.helpers.InternetManager
import com.jortegat.base.helpers.ManagePermissions
import com.jortegat.base.helpers.showConfirmationDialog
import com.jortegat.base.helpers.showMessage
import com.jortegat.base.models.ui.ConfirmationDialogInformation
import com.jortegat.base.ui.base.BaseFragment
import com.jortegat.presentation.R
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
open class BaseDriverFragment : BaseFragment() {

    private lateinit var managePermissions: ManagePermissions

    @Inject
    lateinit var internetManager: InternetManager

    private val permissionList =
        listOf(
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            managePermissions =
                ManagePermissions(it, permissionList)
        }

        requestPermission()
    }

    private fun requestPermission() {
        if (managePermissions.requestPermissionsNeeded()) {
            showPermissionMessage()
        }
    }

    private fun showPermissionMessage() {
        val locationPermissionDialog = buildLocationConfirmationDialog()
        requireContext().showConfirmationDialog(locationPermissionDialog)
    }

    private fun buildLocationConfirmationDialog(): ConfirmationDialogInformation {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            // Check location permission
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                permissionsRequestCode
            )
        }

        val onBack: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            showMessage(
                "Sin acceso a tú ubicación no podrás dar inicio a nuevos viajes.",
                requireView()
            )
        }

        return ConfirmationDialogInformation(
            title = "Nutabe Conductor necesita acceder a tu ubicación",
            message = "Nutabe Conductor recopila datos de ubicación para habilitar el seguimiento del estado de los viajes que realizas incluso cuando la aplicación está cerrada o no está en uso.",
            negativeButton = requireContext().getString(R.string.no_go_back),
            positiveButton = requireContext().getString(R.string.yes_confirm),
            icon = R.drawable.ic_exclamation_circle,
            positiveCallback = onConfirm,
            negativeCallback = onBack
        )
    }

    companion object {
        private const val permissionsRequestCode = 123

        val LOCATION_PERMISSION_REQUEST_CODE_START_JOURNEY = 1
        val LOCATION_PERMISSION_REQUEST_CODE_RESUME_JOURNEY = 2
    }
}