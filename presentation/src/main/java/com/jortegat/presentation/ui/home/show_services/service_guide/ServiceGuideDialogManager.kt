package com.jortegat.presentation.ui.home.show_services.service_guide

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.jortegat.base.enums.ServiceUnitType
import com.jortegat.base.helpers.showConfirmationDialog
import com.jortegat.base.helpers.showMessage
import com.jortegat.base.models.ui.ConfirmationDialogInformation
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.LayoutServiceGuideDialogBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ServiceGuideDialogManager(
    private val context: Context,
    private val journey: Journey,
    driverName: String?
) {

    private var _binding: LayoutServiceGuideDialogBinding? = null
    val binding: LayoutServiceGuideDialogBinding get() = _binding!!

    var onRemissionDone: ((Journey) -> Unit)? = null
    var onReceiptDone: ((Journey) -> Unit)? = null

    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    private var current = LocalDateTime.now()

    init {
        _binding = LayoutServiceGuideDialogBinding.inflate(LayoutInflater.from(context))
        binding.run {
            driverName?.let {
                etOrderOwnerDriver.text = it
                etRemissionDriver.text = it
                etReceivedDriver.text = it
            }

            journey.run {
                etJourneyNumber.text = serial.toString()
                etOrderMaterial.text = material.name
                etOrderAddressOrigin.text = origin.businessName
                etRemissionOrigin.text = origin.businessName
                etOrderOwnerPlate.text = vehicleId
                etRemissionPlate.text = vehicleId
                etReceivedPlate.text = vehicleId
                etOrderNumber.text = serviceOrder
                etOrderAddressDestination.text = destination.businessName
                etReceivedDestination.text = destination.businessName

                cbOrderDispatched.isChecked = !remissionDispatcher.isNullOrEmpty()
                etDispatcherName.setText(remissionDispatcher ?: "")
                etRemissionNumber.setText(remissionNumber ?: "")
                remissionLoad?.let { etDispatcherLoad.setText(it.toString()) }
                rbLoadTypeTon.isChecked = remissionLoadType == ServiceUnitType.TON.serverValue
                etRemissionTime.text = remissionDate
                if (cbOrderDispatched.isChecked) lockRemissionFields()

                cbOrderReceived.isChecked = !receiptName.isNullOrEmpty()
                etReceiverName.setText(receiptName ?: "")
                etReceiveNumber.setText(receiptNumber ?: "")
                etReceiveTime.text = receiptDate
                if (cbOrderReceived.isChecked) lockReceiverFields()
            }
            cbOrderAccepted.isChecked = true
            cbOrderAccepted.isClickable = false

            cbOrderDispatched.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    if (journey.hasVisitedOrigin) {
                        if (validateRemissionFields()) {
                            val information = buildRemissionConfirmationDialog()
                            context.showConfirmationDialog(information)

                        } else {
                            cbOrderDispatched.isChecked = false
                            context.showMessage(
                                "Todos los campos deben ser diligenciados.",
                                binding.root
                            )
                        }
                    } else {
                        cbOrderDispatched.isChecked = false
                        context.showMessage(
                            "Este vehiculo no ha visitado el punto de control de inicio.",
                            binding.root
                        )
                    }
                }
            }

            cbOrderReceived.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    if (journey.hasVisitedOrigin) {
                        if (journey.hasVisitedDestination) {
                            if (!cbOrderDispatched.isChecked) {
                                context.showMessage(
                                    "Debes confirmar que la orden fue despachada primero.",
                                    binding.root
                                )
                                cbOrderReceived.isChecked = false
                            } else {
                                if (validateReceivedFields()) {
                                    val information = buildReceiptConfirmationDialog()
                                    context.showConfirmationDialog(information)
                                } else {
                                    cbOrderReceived.isChecked = false
                                    context.showMessage(
                                        "Ingresa el nombre de quien recibe y el # de la remisión.",
                                        binding.root
                                    )
                                }
                            }
                        } else {
                            cbOrderReceived.isChecked = false
                            context.showMessage(
                                "Este vehiculo no ha visitado el punto de control de destino.",
                                binding.root
                            )
                        }
                    } else {
                        cbOrderReceived.isChecked = false
                        context.showMessage(
                            "Este vehiculo no ha visitado el punto de control de inicio.",
                            binding.root
                        )
                    }
                }
            }
        }
    }

    private fun buildReceiptConfirmationDialog(): ConfirmationDialogInformation {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->

            current = LocalDateTime.now()
            binding.etReceiveTime.text = current.format(formatter)

            journey.apply {
                receiptName = binding.etReceiverName.text.toString()
                receiptNumber = binding.etReceiveNumber.text.toString()
                receiptDate = binding.etReceiveTime.text.toString()
                onReceiptDone?.invoke(this)
            }
            dialog.dismiss()
        }

        val onBack: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            binding.cbOrderReceived.isChecked = false
        }

        return ConfirmationDialogInformation(
            title = context.getString(R.string.important_received_order),
            message = context.getString(R.string.received_order_confirmation_message),
            negativeButton = context.getString(R.string.no_go_back),
            positiveButton = context.getString(R.string.yes_confirm),
            icon = R.drawable.ic_exclamation_circle,
            positiveCallback = onConfirm,
            negativeCallback = onBack
        )
    }

    private fun buildRemissionConfirmationDialog(): ConfirmationDialogInformation {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->

            current = LocalDateTime.now()
            binding.etRemissionTime.text = current.format(formatter)

            journey.apply {
                remissionDispatcher = binding.etDispatcherName.text.toString()
                remissionNumber = binding.etRemissionNumber.text.toString()
                remissionDate = binding.etRemissionTime.text.toString()
                remissionLoad = binding.etDispatcherLoad.text.toString().toLong()
                remissionLoadType =
                    if (binding.rbLoadTypeM3.isChecked) ServiceUnitType.M3.serverValue else ServiceUnitType.TON.serverValue
                onRemissionDone?.invoke(this)
            }
            dialog.dismiss()
        }

        val onBack: (DialogInterface, Int) -> Unit = { dialog, _ ->
            dialog.dismiss()
            binding.cbOrderDispatched.isChecked = false
        }

        return ConfirmationDialogInformation(
            title = context.getString(R.string.important_dispatch_order),
            message = context.getString(R.string.dispatch_order_confirmation_message),
            negativeButton = context.getString(R.string.no_go_back),
            positiveButton = context.getString(R.string.yes_confirm),
            icon = R.drawable.ic_exclamation_circle,
            positiveCallback = onConfirm,
            negativeCallback = onBack
        )
    }

    private fun LayoutServiceGuideDialogBinding.lockRemissionFields() {
        cbOrderDispatched.isClickable = false
        tilDispatcherName.isEnabled = false
        tilRemissionNumber.isEnabled = false
        etDispatcherName.isEnabled = false
        etRemissionNumber.isEnabled = false
        etDispatcherLoad.isEnabled = false
        tilDispatcherLoad.isEnabled = false
        rbLoadTypeGroup.isEnabled = false
        rbLoadTypeM3.isEnabled = false
        rbLoadTypeM3.isClickable = false
        rbLoadTypeTon.isEnabled = false
        rbLoadTypeTon.isClickable = false
    }

    private fun LayoutServiceGuideDialogBinding.lockReceiverFields() {
        cbOrderReceived.isClickable = false
        tilReceiverName.isEnabled = false
        tilReceiveNumber.isEnabled = false
        etReceiverName.isEnabled = false
        etReceiveNumber.isEnabled = false
    }

    private fun LayoutServiceGuideDialogBinding.validateRemissionFields(): Boolean {
        return !etDispatcherName.text.isNullOrEmpty() && !etRemissionNumber.text.isNullOrEmpty() && !etDispatcherLoad.text.isNullOrEmpty()
    }

    private fun LayoutServiceGuideDialogBinding.validateReceivedFields(): Boolean {
        return !etReceiverName.text.isNullOrEmpty() && !etReceiveNumber.text.isNullOrEmpty()
    }

    fun updateJourneyPdfGuide(originVisited: Boolean, destinationVisited: Boolean) {
        journey.hasVisitedOrigin = originVisited
        journey.hasVisitedDestination = destinationVisited
    }

    fun remissionIsDone() {
        binding.cbOrderDispatched.isChecked = true
        binding.lockRemissionFields()
    }

    fun receiptIsDone() {
        binding.cbOrderReceived.isChecked = true
        binding.lockReceiverFields()
    }

    fun getDialog() = AlertDialog.Builder(context).setView(binding.root).create()
}