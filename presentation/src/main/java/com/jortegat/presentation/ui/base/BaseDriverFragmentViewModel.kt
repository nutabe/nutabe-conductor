package com.jortegat.presentation.ui.base

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.jortegat.domain.driver.usecase.DriverUseCase

open class BaseDriverFragmentViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase
) : ViewModel() {

}