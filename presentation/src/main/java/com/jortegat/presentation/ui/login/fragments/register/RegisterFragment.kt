package com.jortegat.presentation.ui.login.fragments.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jortegat.base.helpers.*
import com.jortegat.base.models.UserRegistrationForm
import com.jortegat.base.ui.base.BaseFragment
import com.jortegat.base.ui.widgets.NutabeEditText
import com.jortegat.presentation.R
import com.jortegat.presentation.ui.base.BaseDriverFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : BaseDriverFragment() {

    private val viewModel: RegisterViewModel by viewModels()
    private lateinit var userRegistrationForm: UserRegistrationForm

    //view
    private lateinit var registerButton: FloatingActionButton
    private lateinit var editTextEmail: NutabeEditText
    private lateinit var editTextFirstName: EditText
    private lateinit var editTextLastName: EditText
    private lateinit var editTextIdNumber: EditText
    private lateinit var editTextPhone: EditText
    private lateinit var editTextPhoneConfirmation: EditText
    private lateinit var editTextPassword: NutabeEditText
    private lateinit var editTextConfirmPassword: NutabeEditText
    private lateinit var textViewPassword: TextView
    private lateinit var textViewConfirmPassword: TextView
    private lateinit var textViewPhone: TextView
    private lateinit var textViewPhoneConfirmation: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        listeners()
        observers()
    }

    private fun initView() {
        requireActivity().run {
            registerButton = findViewById(R.id.registerButton)
            editTextEmail = findViewById(R.id.editTextEmail)
            editTextFirstName = findViewById(R.id.editTextFirstName)
            editTextLastName = findViewById(R.id.editTextLastName)
            editTextIdNumber = findViewById(R.id.editTextIdNumber)
            editTextPhone = findViewById(R.id.editTextPhone)
            editTextPhoneConfirmation = findViewById(R.id.editTextPhoneConfirmation)
            editTextPassword = findViewById(R.id.editTextPassword)
            editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword)
            textViewPassword = findViewById(R.id.textViewPassword)
            textViewConfirmPassword = findViewById(R.id.textViewConfirmPassword)
            textViewPhone = findViewById(R.id.textViewPhone)
            textViewPhoneConfirmation = findViewById(R.id.textViewPhoneConfirmation)
        }
    }

    private fun listeners() {
        requireActivity().hideKeyboard()
        registerButton.setOnClickListener {
            val email = editTextEmail.text.toString()

            userRegistrationForm = UserRegistrationForm(
                firstName = editTextFirstName.text.toString(),
                lastName = editTextLastName.text.toString(),
                identification = editTextIdNumber.text.toString(),
                phone = COLOMBIA_CODE + editTextPhone.text.toString(),
                email = if (email.isBlank() || email.isEmpty()) null else email,
                password = editTextPassword.text.toString()
            )

            if (validateFields(userRegistrationForm)) {
                if (validatePassword(userRegistrationForm.password)) {
                    if (validatePhone(userRegistrationForm.identification)) {
                        activity?.hideKeyboard()
                        registerUser(userRegistrationForm)
                    } else {
                        val shakeAnimation = AnimationUtils.loadAnimation(context, R.anim.shake)
                        textViewPhone.startAnimation(shakeAnimation)
                        textViewPhoneConfirmation.startAnimation(shakeAnimation)
                        showMessage(getString(R.string.not_matching_phone__message), requireView())
                    }
                } else {
                    val shakeAnimation = AnimationUtils.loadAnimation(context, R.anim.shake)
                    textViewPassword.startAnimation(shakeAnimation)
                    textViewConfirmPassword.startAnimation(shakeAnimation)
                    showMessage(getString(R.string.not_matching_password__message), requireView())
                }
            } else {
                showMessage(getString(R.string.empty_fields_validation_message), requireView())
            }
        }
    }

    private fun validateFields(userRegistration: UserRegistrationForm): Boolean {
        userRegistration.run {
            return firstName.isNotEmpty() &&
                    lastName.isNotEmpty() &&
                    identification.isNotEmpty() &&
                    phone.isNotEmpty() &&
                    password.isNotEmpty()
        }
    }

    private fun validatePassword(password: String): Boolean {
        return password.isNotEmpty() && (editTextPassword.text.toString() == editTextConfirmPassword.text.toString())
    }

    private fun validatePhone(cc: String): Boolean {
        return cc.isNotEmpty() && (editTextPhone.text.toString() == editTextPhoneConfirmation.text.toString())
    }

    private fun registerUser(userRegistrationForm: UserRegistrationForm) {
        if (internetManager.isInternetOn()) {
            showLoader()
            viewModel.register(userRegistrationForm)
        } else {
            showNoInternetMessage(requireView())
        }
    }

    private fun observers() {
        viewModel.registerLiveData.observe(viewLifecycleOwner, Observer(::validateRegistration))
    }

    private fun validateRegistration(result: Result<Unit>) {
        hideLoader()
        when (result) {
            is Result.Success -> {
                findNavController().navigate(
                    R.id.action_registerFragment_to_confirmationFragment,
                    Bundle().apply {
                        putString("user", editTextIdNumber.text.toString())
                    }
                )
            }
            is Result.Error -> showMessage(result.exception.message.toString(), requireView())
        }
    }

    companion object {
        const val COLOMBIA_CODE = "+57"
    }
}