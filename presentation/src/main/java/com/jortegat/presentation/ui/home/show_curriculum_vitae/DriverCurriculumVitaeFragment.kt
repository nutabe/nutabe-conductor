package com.jortegat.presentation.ui.home.show_curriculum_vitae

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.jortegat.base.enums.DriverDocumentTypes
import com.jortegat.base.extensions.setRightDrawableBoolean
import com.jortegat.base.helpers.*
import com.jortegat.base.models.domain.Driver
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentDriverCurriculumVitaeBinding
import com.jortegat.presentation.extensions.onBackPressed
import com.jortegat.presentation.ui.base.BaseDriverFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DriverCurriculumVitaeFragment : BaseDriverFragment() {

    private val viewModel: DriverCurriculumVitaeFragmentViewModel by viewModels()

    private val binding: FragmentDriverCurriculumVitaeBinding get() = _binding!!
    private var _binding: FragmentDriverCurriculumVitaeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDriverCurriculumVitaeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setObservers()
        listeners()

        requestToken()
    }

    private fun listeners() {
        requireActivity().onBackPressed {
            findNavController().navigate(R.id.action_driverCurriculumVitaeFragment_to_nav_home)
        }
    }

    private fun requestToken() {
        if (internetManager.isInternetOn()) {
            viewModel.requestToken()
        } else {
            showNoInternetMessage(requireView())
            showNoInternetLayout()
        }
    }

    private fun validateDocumentsAreFilled() {
        showLoader("Estamos validando tu perfil.")
        viewModel.validateDocumentsAreFilled()
    }

    private fun setObservers() {
        viewModel.documentsFilled.observe(viewLifecycleOwner, ::requestToFillDocuments)
        viewModel.driverInformationResult.observe(viewLifecycleOwner, ::manageDriverInformation)
        viewModel.tokenResult.observe(viewLifecycleOwner, ::onTokenResponse)
        internetManager.observe(viewLifecycleOwner, ::onInternetChange)
    }

    private fun onInternetChange(hasInternet: Boolean?) {
        if (hasInternet == true) {
            if (binding.noInternetLayout.isVisible) {
                requestToken()
            }
        }
    }

    private fun onTokenResponse(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                if (internetManager.isInternetOn()) {
                    validateDocumentsAreFilled()
                } else {
                    showNoInternetMessage(requireView())
                    showNoInternetLayout()
                }
            }
            doIfError {
                showErrorContainer()
            }
        }
    }

    private fun requestToFillDocuments(documentsAreFilled: Result<Boolean>) {
        with(documentsAreFilled) {
            doIfSuccess {
                getDriverInformation()
            }
            doIfError {
                hideLoader()
                binding.textViewNoCurriculum.text =
                    getString(R.string.curriculum_truck_driver_error)
                findNavController().navigate(R.id.action_driverCurriculumVitaeFragment_to_driverDialogFragment)
            }
        }
    }

    private fun manageDriverInformation(result: Result<Driver>) {
        hideLoader()
        with(result) {
            doIfSuccess {
                if (it != null && !it.name.isNullOrEmpty()) showDriverInformation(it)
                else binding.curriculumVisibilityDetails(false)
            }
            doIfError {
                binding.textViewNoCurriculum.text =
                    getString(R.string.curriculum_truck_driver_error)
            }
        }
    }

    private fun FragmentDriverCurriculumVitaeBinding.curriculumVisibilityDetails(isVisible: Boolean) {
        noCurriculumLayout.isVisible = !isVisible
        curriculumLayout.isVisible = isVisible
    }

    private fun showDriverInformation(driver: Driver) {
        with(binding) {
            showDriverCurriculumContainer()
            curriculumVisibilityDetails(true)
            setMainInformation(driver)
            setLicenseInformation(driver)
        }
        hideLoader()
    }

    private fun FragmentDriverCurriculumVitaeBinding.setMainInformation(driver: Driver) {
        driverNameTextView.setText(driver.name ?: "No registrado")
        idTextView.setText(driver.id ?: driver.identification ?: "No registrada")
        assignedVehicle.setText(driver.vehicle?.id ?: "Sin vehículo")
        category.setText(driver.category ?: "No registrada")
        isActive.setRightDrawableBoolean(driver.isActive ?: false)
        isVerified.setRightDrawableBoolean(driver.isVerified ?: false)
        isRuntVerified.setRightDrawableBoolean(driver.runtVerified ?: false)
        courtRecord.setRightDrawableBoolean(driver.courtRecords ?: false)
    }

    private fun FragmentDriverCurriculumVitaeBinding.setLicenseInformation(driver: Driver) {
        propertyCardTitle.text = driver.license?.type?.let { DriverDocumentTypes.getAppValue(it) }
        licenseEndDate.apply {
            driver.licenseEndDate?.let {
                setText(it ?: "desconocida")
            } ?: run { isVisible = false }
        }
        loadPropertyCardImages(driver)
    }

    private fun FragmentDriverCurriculumVitaeBinding.loadPropertyCardImages(driver: Driver) {
        if (detailLayout.isVisible && !driver.license?.images.isNullOrEmpty()) {
            propertyCardLayout.isVisible = true
            val imageFront = driver.license?.images?.get(0)
            val imageBack = driver.license?.images?.get(1)
            manageDocumentType(imageFront, imageViewFront, pdfViewFront)
            manageDocumentType(imageBack, imageViewBack, pdfViewBack)
        } else propertyCardLayout.isVisible = false
    }

    private fun manageDocumentType(fileName: String?, imageView: ImageView, pdfView: WebView) {
        fileName?.let {
            if (it.contains(".pdf")) {
                imageView.isVisible = false
                pdfView.loadFile(fileName)
            } else {
                pdfView.isVisible = false
                imageView.loadImage(fileName)
            }
        } ?: run {
            val noDocumentDrawable =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_no_photos, null)
            imageView.setImageDrawable(noDocumentDrawable)
            pdfView.isVisible = false
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun WebView.loadFile(fileName: String) {
        isVisible = true
        settings.javaScriptEnabled = true
        loadUrl(PDF_VIEWER_URL + BASE_URL + fileName)
    }

    private fun ImageView.loadImage(fileName: String) {
        isVisible = true
        Glide.with(binding.root.context)
            .load(BASE_URL + fileName)
            .into(this)
    }

    private fun getDriverInformation() {
        viewModel.getDriverInformation()
    }

    private fun showErrorContainer() {
        binding.containerDriverCurriculum.isVisible = false
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = true
    }

    private fun showNoInternetLayout() {
        binding.containerDriverCurriculum.isVisible = false
        binding.layoutError.isVisible = false
        binding.noInternetLayout.isVisible = true
    }

    private fun showDriverCurriculumContainer() {
        binding.containerDriverCurriculum.isVisible = true
        binding.layoutError.isVisible = false
        binding.noInternetLayout.isVisible = false
    }
}
