package com.jortegat.presentation.ui.home.show_notifications

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.NotificationItemBinding
import com.jortegat.presentation.databinding.PendingInvitationItemBinding

class PendingInvitationAdapter(private var pendingInvitations: List<PendingInvitation>) :
    RecyclerView.Adapter<PendingInvitationAdapter.NotificationsHolder>() {

    private val binding: PendingInvitationItemBinding get() = _binding!!
    private var _binding: PendingInvitationItemBinding? = null

    var onAcceptClick: ((String) -> Unit)? = null
    var onRejectClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationsHolder {
        _binding =
            PendingInvitationItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return NotificationsHolder(binding)
    }

    override fun onBindViewHolder(holder: NotificationsHolder, position: Int) {
        val item = pendingInvitations[position]
        holder.bindNotification(item)
    }

    override fun getItemCount(): Int {
        return pendingInvitations.size
    }

    inner class NotificationsHolder(private val v: PendingInvitationItemBinding) :
        RecyclerView.ViewHolder(v.root) {

        private var invitation: PendingInvitation? = null

        fun bindNotification(invitation: PendingInvitation) {
            this.invitation = invitation

            v.apply {

                parentCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        v.root.context,
                        R.anim.anim_recycler
                    )
                )

                invitation.run {
                    val dateAndTime = createdAt.split("T")
                    val dateOnly = dateAndTime[0].replace("-", "/")
                    val timeOnly = dateAndTime[1].subSequence(0, 5)
                    tvNotificationTime.text = "$dateOnly - $timeOnly"
                    tvNotificationBody.text = "Has sido invitado a ser el conductor del vehículo $vehicleId"

                    buttonConfirmInvitation.setOnClickListener {
                        onAcceptClick?.invoke(
                            id.toString()
                        )
                    }
                    buttonRejectInvitation.setOnClickListener {
                        onRejectClick?.invoke(
                            id.toString()
                        )
                    }

                }
            }
        }

    }

}
