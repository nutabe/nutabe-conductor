package com.jortegat.presentation.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import com.jortegat.presentation.ui.base.BaseDriverActivityViewModel
import kotlinx.coroutines.launch
import com.jortegat.base.helpers.Result
import com.jortegat.domain.driver.model.notifications.Notification
import kotlinx.coroutines.GlobalScope

class HomeActivityViewModel @ViewModelInject constructor(
    private val loginUseCase: LoginUseCase,
    private val driverUseCase: DriverUseCase
) : BaseDriverActivityViewModel(driverUseCase, loginUseCase) {

    fun saveFrontalPhotoPath(key: String, path: String?) {
        driverUseCase.saveFrontalPhotoPath(key, path)
    }

    fun saveBackPhotoPath(key: String, path: String?) {
        driverUseCase.saveBackPhotoPath(key, path)
    }

    fun saveOnDemandOriginFilePath(key: String, path: String?) {
        driverUseCase.saveOnDemandOriginFilePath(key, path)
    }

    fun saveOnDemandDestinationFilePath(key: String, path: String?) {
        driverUseCase.saveOnDemandDestinationFilePath(key, path)
    }

    fun saveOnDemandSupportFilePath(key: String, path: String?) {
        driverUseCase.saveOnDemandSupportFilePath(key, path)
    }

}