package com.jortegat.presentation.ui.home.create_on_demand_services

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.doIfSuccess
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.Driver
import com.jortegat.base.models.on_demand_material.OnDemandMaterial
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverBuildingSite
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.helper.*
import com.jortegat.presentation.ui.base.BaseDriverFragmentViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.IOException

class CreateOnDemandServiceFragmentViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val dispatcher: CoroutineDispatcher
) : BaseDriverFragmentViewModel(driverUseCase) {

    private val _buildingSites = MediatorLiveData<Result<List<DriverBuildingSite>>>()
    val buildingSites: LiveData<Result<List<DriverBuildingSite>>> get() = _buildingSites

    private val _quarryAndDump = MediatorLiveData<Result<List<Place>>>()
    val quarryAndDump: LiveData<Result<List<Place>>> get() = _quarryAndDump

    private val _materials = MediatorLiveData<Result<List<OnDemandMaterial>>>()
    val materials: LiveData<Result<List<OnDemandMaterial>>> get() = _materials

    private val _driver = MediatorLiveData<Result<Driver>>()
    val driver: LiveData<Result<Driver>> get() = _driver

    private val _onCreateOnDemandService = MutableLiveData<Result<Int>>()
    val onCreateOnDemandService: LiveData<Result<Int>> get() = _onCreateOnDemandService

    private val _onUploadDemandServiceDocuments = MutableLiveData<Boolean>()
    val onUploadDemandServiceDocuments: LiveData<Boolean> get() = _onUploadDemandServiceDocuments


    fun getBuildingSites() {
        viewModelScope.launch(dispatcher) {
            _buildingSites.postValue(driverUseCase.getBuildingSites())
        }
    }

    fun getDriverInformation() {
        viewModelScope.launch(dispatcher) {
            _driver.postValue(driverUseCase.getDriverInformation())
        }
    }

    fun getQuarryAndDump() {
        val places = mutableListOf<Place>()

        var quarryError = false
        var dumpError = false

        viewModelScope.launch(dispatcher) {

            val quarryResponse = async { driverUseCase.getPlacesByType(QUARRY_TYPE) }
            val dumpResponse = async { driverUseCase.getPlacesByType(DUMP_TYPE) }

            when (val result = quarryResponse.await()) {
                is Result.Success -> places.addAll(result.data.map { it.copy(type = QUARRY_TYPE) })
                is Result.Error -> quarryError = true
            }

            when (val result = dumpResponse.await()) {
                is Result.Success -> places.addAll(result.data.map { it.copy(type = DUMP_TYPE) })
                is Result.Error -> dumpError = true
            }

            if (quarryError || dumpError)
                _quarryAndDump.postValue(Result.Error(IOException()))
            else
                _quarryAndDump.postValue(Result.Success(places))
        }
    }

    fun getMaterials() {
        viewModelScope.launch(dispatcher) {
            _materials.postValue(driverUseCase.getMaterials())
        }
    }

    fun createOnDemandService(service: CreateOnDemandService) {
        viewModelScope.launch(dispatcher) {
            _onCreateOnDemandService.postValue(driverUseCase.createOnDemandService(service))
        }
    }

    fun uploadDemandServiceDocuments(serviceId: Int) {
        viewModelScope.launch(dispatcher) {

            var originSucceed = false
            var destinationSucceed = false
            var supportSucceed = false

            val originDocument = async { driverUseCase.uploadOriginDocument(serviceId) }
            val destinationDocument = async { driverUseCase.uploadDestinationDocument(serviceId) }
            val supportDocument = async { driverUseCase.uploadSupportDocument(serviceId) }

            originDocument.await().doIfSuccess { originSucceed = true }
            destinationDocument.await().doIfSuccess { destinationSucceed = true }
            supportDocument.await().doIfSuccess { supportSucceed = true }
            _onUploadDemandServiceDocuments.postValue(originSucceed && destinationSucceed && supportSucceed)
        }
    }

    fun documentsAreAttached(): Boolean {
        return driverUseCase.getOnDemandOriginFilePath()
            .isNotEmpty() && driverUseCase.getOnDemandDestinationFilePath().isNotEmpty()

    }

    fun resetThePathSavedForPdfs() {
        driverUseCase.saveOnDemandOriginFilePath(ON_DEMAND_ORIGIN_FILE_PATH, "")
        driverUseCase.saveOnDemandDestinationFilePath(ON_DEMAND_DESTINATION_FILE_PATH, "")
        driverUseCase.saveOnDemandSupportFilePath(ON_DEMAND_SUPPORT_FILE_PATH, "")
    }

}