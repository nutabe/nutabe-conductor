package com.jortegat.presentation.ui.home.show_services

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.jortegat.base.helpers.*
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.databinding.FragmentFinishedServicesBinding
import com.jortegat.presentation.ui.base.BaseDriverFragment
import com.jortegat.presentation.ui.home.view_documents.ViewDocumentsActivity
import kotlinx.android.synthetic.main.sort_date.*

private const val ARG_VEHICLE_ID = "vehicleId"

class FinishedServicesFragment : BaseDriverFragment() {

    private lateinit var adapter: FinishedJourneyAdapter
    private var journeys: List<Journey> = mutableListOf()
    private var vehicle = ""

    private val viewModel: ShowFinishedServicesViewModel by viewModels()

    private val binding: FragmentFinishedServicesBinding get() = _binding!!
    private var _binding: FragmentFinishedServicesBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFinishedServicesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val id = it.getString(ARG_VEHICLE_ID)
            vehicle = id ?: ""
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.startShimmerAnimation()
        listeners()
        observers()
        getFinishedJourneys(vehicle)
    }

    private fun observers() {
        viewModel.finishedJourneyListResult.observe(viewLifecycleOwner, ::showFinishedJourneyResult)
    }

    private fun showFinishedJourneyResult(result: Result<List<Journey>>) {
        binding.stopShimmerAnimation()
        with(result) {
            doIfSuccess {
                if (it != null && it.isNotEmpty()) {
                    journeys = it
                    binding.initView()
                    adapter = FinishedJourneyAdapter(journeys)
                    adapter.onOpenDocument = ::openDocument
                    binding.recyclerFinishedServiceView.adapter = adapter
                } else {
                    binding.showNoFinishedJourneysContainer()
                }
            }
            doIfError {
                showMessage(it.message.toString(), requireView())
            }
        }
    }

    private fun getFinishedJourneys(vehicle: String) {
        if (internetManager.isInternetOn()) {
            viewModel.getListOfFinishedJourneys(vehicle)
        } else {
            showNoInternetMessage(requireView())
            binding.stopShimmerAnimation()
            binding.showNoInternetContainer()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun listeners() {
        rbAscendent.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    binding.svFilter.setQuery("", true)
                    rbAscendent.isSelected = true
                    rbDescendent.isSelected = false

                    if (journeys.isEmpty()) return@setOnTouchListener true

                    adapter =
                        FinishedJourneyAdapter(journeys.sortedByDescending { journey -> journey.date }
                            .toMutableList())
                    adapter.onOpenDocument = ::openDocument
                    binding.recyclerFinishedServiceView.adapter = adapter

                }
            }
            return@setOnTouchListener true
        }

        rbDescendent.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    binding.svFilter.setQuery("", true)
                    rbAscendent.isSelected = false
                    rbDescendent.isSelected = true

                    if (journeys.isEmpty()) return@setOnTouchListener true

                    adapter = FinishedJourneyAdapter(journeys.sortedBy { journey -> journey.date }
                        .toMutableList())
                    adapter.onOpenDocument = ::openDocument
                    binding.recyclerFinishedServiceView.adapter = adapter
                }
            }
            return@setOnTouchListener true
        }
    }

    private fun openDocument(path: String) {
        val intent = Intent(requireContext(), ViewDocumentsActivity::class.java)
        intent.putExtra(ViewDocumentsActivity.RESOURCE, path)
        startActivity(intent)
    }

    private fun FragmentFinishedServicesBinding.initView() {
        hideNoFinishedJourneysContainer()
        hideNoInternetContainer()
        showSearchBarAndOrderToggle()

        svFilter.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { keyword ->
                    adapter =
                        FinishedJourneyAdapter(journeys.filter { it.date.contains(keyword) })
                    adapter.onOpenDocument = ::openDocument
                    recyclerFinishedServiceView.adapter = adapter
                }
                return true
            }

        })
    }

    private fun FragmentFinishedServicesBinding.startShimmerAnimation() {
        shimmerFinishedLayout.startShimmer()
        shimmerFinishedLayout.visibility = View.VISIBLE
        hideSearchBarAndOrderToggle()
        hideNoFinishedJourneysContainer()
    }

    private fun FragmentFinishedServicesBinding.hideNoFinishedJourneysContainer() {
        viewNoPendingJourneys.isVisible = false
        textViewNoPendingJourneys.isVisible = false
    }

    private fun FragmentFinishedServicesBinding.showNoFinishedJourneysContainer() {
        stopShimmerAnimation()
        hideSearchBarAndOrderToggle()
        hideNoInternetContainer()
        viewNoPendingJourneys.isVisible = true
        textViewNoPendingJourneys.isVisible = true
        recyclerFinishedServiceView.isVisible = false
    }

    private fun FragmentFinishedServicesBinding.hideSearchBarAndOrderToggle() {
        tvSortByTitle.isVisible = false
        sortCardViewButton.isVisible = false
        svFilter.isVisible = false
    }

    private fun FragmentFinishedServicesBinding.showSearchBarAndOrderToggle() {
        hideNoInternetContainer()
        tvSortByTitle.isVisible = true
        sortCardViewButton.isVisible = true
        svFilter.isVisible = true
        svFilter.clearFocus()
        rbDescendent.isSelected = true
    }

    private fun FragmentFinishedServicesBinding.showNoInternetContainer() {
        hideNoFinishedJourneysContainer()
        hideSearchBarAndOrderToggle()
        recyclerFinishedServiceView.isVisible = false
        noInternetLayout.isVisible = true
    }

    private fun FragmentFinishedServicesBinding.hideNoInternetContainer() {
        noInternetLayout.isVisible = false
    }

    private fun FragmentFinishedServicesBinding.stopShimmerAnimation() {
        shimmerFinishedLayout.stopShimmer()
        shimmerFinishedLayout.visibility = View.GONE
    }

    companion object {
        @JvmStatic
        fun newInstance(vehicleId: String) = FinishedServicesFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_VEHICLE_ID, vehicleId)
            }
        }
    }
}