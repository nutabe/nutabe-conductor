package com.jortegat.presentation.ui.login.fragments.confirmation

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.RegistrationCode
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.launch
import java.io.IOException

class ConfirmationViewModel @ViewModelInject constructor(private val loginUseCase: LoginUseCase) :
    ViewModel() {

    private val _registrationCode = MediatorLiveData<Result<Unit>>()
    val registrationCodeLiveData: LiveData<Result<Unit>> get() = _registrationCode

    private val _reestablishPassLiveData = MediatorLiveData<Result<Unit>>()
    val reestablishPassLiveData: LiveData<Result<Unit>> get() = _reestablishPassLiveData

    fun confirm(confirmationCode: RegistrationCode) {
        viewModelScope.launch {
            when (val result = loginUseCase.confirm(confirmationCode)) {
                is Result.Success -> {
                    _registrationCode.postValue(Result.Success(Unit))
                }
                is Result.Error -> _registrationCode.postValue(Result.Error(result.exception))
            }
        }
    }

    fun requestCodeAgain(identification: String) {
        viewModelScope.launch {
            loginUseCase.requestCodeAgain(identification)
        }
    }

    /**
     * Before going to the HomeActivity, save in preference that the user had a successful logged in
     * */
    fun setUserLogged() {
        loginUseCase.setUserLogged()
    }

    fun reestablishPassword(user: String, pass: String, code: String) {
        viewModelScope.launch {
            when (
                loginUseCase.reestablishPassword(user, pass, code)) {
                is Result.Success -> {
                    _reestablishPassLiveData.postValue(Result.Success(Unit))
                }
                is Result.Error -> _reestablishPassLiveData.postValue(Result.Error(IOException()))
            }
        }
    }

}