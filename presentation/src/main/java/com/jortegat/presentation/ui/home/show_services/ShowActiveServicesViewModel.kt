package com.jortegat.presentation.ui.home.show_services

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.domain.CustomJourney
import com.jortegat.base.models.domain.Driver
import com.jortegat.base.models.domain.JourneyDestination
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.launch

class ShowActiveServicesViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val loginUseCase: LoginUseCase
) : ShowServicesViewModel(driverUseCase, loginUseCase) {

    private val _startedJourneyResult = MediatorLiveData<Result<CustomJourney>>()
    val startedJourneyResult: LiveData<Result<CustomJourney>> get() = _startedJourneyResult

    private val _journeyDestinationResult = MediatorLiveData<Result<JourneyDestination>>()
    val journeyDestinationResult: LiveData<Result<JourneyDestination>> get() = _journeyDestinationResult

    private val _finishJourney = MediatorLiveData<Result<Unit>>()
    val finishJourneyResult: LiveData<Result<Unit>> get() = _finishJourney

    private val _acceptJourneyResult = MediatorLiveData<Result<Unit>>()
    val acceptJourneyResult: LiveData<Result<Unit>> get() = _acceptJourneyResult

    private val _rejectJourneyResult = MediatorLiveData<Result<Unit>>()
    val rejectJourneyResult: LiveData<Result<Unit>> get() = _rejectJourneyResult

    private val _startJourneyResult = MediatorLiveData<Result<Unit>>()
    val startJourneyResult: LiveData<Result<Unit>> get() = _startJourneyResult

    private val _resumeJourneyResult = MediatorLiveData<Result<Unit>>()
    val resumeJourneyResult: LiveData<Result<Unit>> get() = _resumeJourneyResult

    val driverInformationResult: LiveData<Result<Driver>> get() = _driverInformationResult
    private val _driverInformationResult = MediatorLiveData<Result<Driver>>()

    val onRemissionDone: LiveData<Result<Unit>> get() = _onRemissionDone
    private val _onRemissionDone = MediatorLiveData<Result<Unit>>()

    val onReceiptDone: LiveData<Result<Unit>> get() = _onReceiptDone
    private val _onReceiptDone = MediatorLiveData<Result<Unit>>()

    fun getDriverInformation() {
        viewModelScope.launch {
            _driverInformationResult.postValue(driverUseCase.getDriverInformation())
        }
    }

    fun acceptJourney(id: String) {
        viewModelScope.launch {
            _acceptJourneyResult.postValue(driverUseCase.acceptJourney(id))
        }
    }

    fun rejectJourney(id: String) {
        viewModelScope.launch {
            _rejectJourneyResult.postValue(driverUseCase.rejectJourney(id))
        }
    }

    fun startJourney(id: String) {
        viewModelScope.launch {
            _startJourneyResult.postValue(driverUseCase.startJourney(id))
        }
    }

    fun resumeJourney(id: String) {
        viewModelScope.launch {
            _resumeJourneyResult.postValue(driverUseCase.resumeJourney(id))
        }
    }

    fun finishJourney(id: String) {
        viewModelScope.launch {
            _finishJourney.postValue(driverUseCase.finishJourney(id))
        }
    }

    fun updateJourneyInDb(journey: Journey) {
        viewModelScope.launch { driverUseCase.updateJourneyInDb(journey) }
    }

    fun updateJourneyWithRemission(journey: Journey) {
        viewModelScope.launch {
            _onRemissionDone.postValue(driverUseCase.updateJourneyWithRemission(journey))
        }
    }

    fun updateJourneyWithReceipt(journey: Journey) {
        viewModelScope.launch {
            _onReceiptDone.postValue(driverUseCase.updateJourneyWithReceipt(journey))
        }
    }

    fun sendPendingLocations() {
        viewModelScope.launch {
            driverUseCase.sendPendingLocations()
        }
    }
}