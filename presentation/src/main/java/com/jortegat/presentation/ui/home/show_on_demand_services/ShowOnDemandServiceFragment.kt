package com.jortegat.presentation.ui.home.show_on_demand_services

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.doIfError
import com.jortegat.base.helpers.doIfSuccess
import com.jortegat.base.helpers.showNoInternetMessage
import com.jortegat.base.models.domain.Driver
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandService
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentShowOnDemandServicesBinding
import com.jortegat.presentation.ui.base.BaseDriverFragment
import com.jortegat.presentation.ui.home.view_documents.ViewDocumentsActivity
import com.jortegat.presentation.ui.home.view_documents.ViewDocumentsActivity.Companion.RESOURCE
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.sort_date.*

@AndroidEntryPoint
class ShowOnDemandServiceFragment : BaseDriverFragment() {

    private val viewModel: ShowOnDemandServiceFragmentViewModel by viewModels()

    private val binding: FragmentShowOnDemandServicesBinding get() = _binding!!
    private var _binding: FragmentShowOnDemandServicesBinding? = null

    private lateinit var adapter: OnDemandServiceAdapter

    private var services: List<OnDemandService> = mutableListOf()

    //this flag is right now used because when this fragment is loaded coming from the CreateOnDemandServiceFragment
    //it makes the binding.svFilter.setOnQueryTextListener() triggered.
    private var shouldRecreateAdapterOnTextChanged = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.run {
            shouldRecreateAdapterOnTextChanged = getBoolean("shouldRecreateAdapterOnTextChanged", true)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShowOnDemandServicesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setObservers()
        listeners()

        binding.startShimmerAnimation()
        rbDescendent.isSelected = true

        requestToken()
    }

    private fun requestToken() {
        if (internetManager.isInternetOn()) {
            viewModel.requestToken()
        } else {
            showNoInternetMessage(requireView())
            showNoInternetLayout()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun listeners() {
        binding.fabCreateOnDemandService.setOnClickListener {
            findNavController().navigate(R.id.action_showOnDemandServiceFragment_to_createOnDemandServiceFragment)
        }

        binding.svFilter.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { keyword ->
                    if (shouldRecreateAdapterOnTextChanged) {
                        adapter =
                            OnDemandServiceAdapter(services.filter { it.journeyDate.contains(keyword) })
                        binding.recyclerOnDemandServiceView.adapter = adapter
                        adapter.onOpenDocument = ::openDocument
                    }
                }
                return true
            }

        })

        rbAscendent.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    binding.svFilter.setQuery("", true)
                    rbAscendent.isSelected = true
                    rbDescendent.isSelected = false
                    adapter =
                        OnDemandServiceAdapter(services.sortedByDescending { service -> service.journeyDate }
                            .toMutableList())
                    binding.recyclerOnDemandServiceView.adapter = adapter
                    adapter.onOpenDocument = ::openDocument

                }
            }
            return@setOnTouchListener true
        }

        rbDescendent.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    binding.svFilter.setQuery("", true)
                    rbAscendent.isSelected = false
                    rbDescendent.isSelected = true
                    adapter =
                        OnDemandServiceAdapter(services.sortedBy { service -> service.journeyDate }
                            .toMutableList())
                    binding.recyclerOnDemandServiceView.adapter = adapter
                    adapter.onOpenDocument = ::openDocument
                }
            }
            return@setOnTouchListener true
        }
    }

    private fun openDocument(path: String) {
        val intent = Intent(requireContext(), ViewDocumentsActivity::class.java)
        intent.putExtra(RESOURCE, path)
        startActivity(intent)
    }

    private fun validateDocumentsAreFilled() {
        showLoader("Estamos validando tu perfil.")
        viewModel.validateDocumentsAreFilled()
    }

    private fun setObservers() {
        viewModel.documentsFilled.observe(viewLifecycleOwner, ::requestToFillDocuments)
        viewModel.driverInformationResult.observe(viewLifecycleOwner, ::onDriverInformationResult)
        viewModel.onDemandServiceResult.observe(viewLifecycleOwner, ::onDemandServicesResult)
        viewModel.tokenResult.observe(viewLifecycleOwner, ::onTokenResponse)
        internetManager.observe(viewLifecycleOwner, ::onInternetChange)
    }

    private fun onInternetChange(hasInternet: Boolean?) {
        if (hasInternet == true) {
            if (binding.noInternetLayout.isVisible) {
                binding.startShimmerAnimation()
                showOnDemandServiceContainer()
                requestToken()
            }
        }
    }

    private fun onTokenResponse(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                if (internetManager.isInternetOn()) {
                    validateDocumentsAreFilled()
                } else {
                    showNoInternetMessage(requireView())
                    showNoInternetLayout()
                }
            }
            doIfError {
                binding.stopShimmerAnimation()
                showErrorContainer()
            }
        }
    }

    private fun requestToFillDocuments(documentsAreFilled: Result<Boolean>) {
        with(documentsAreFilled) {
            doIfSuccess { getDriverInformation() }
            doIfError {
                hideLoader()
                binding.stopShimmerAnimation()
                binding.textViewNoCurriculum.text =
                    getString(R.string.curriculum_truck_driver_error)
                findNavController().navigate(R.id.action_showOnDemandServiceFragment_to_driverDialogFragment)
            }
        }
    }

    private fun onDriverInformationResult(result: Result<Driver>) {
        hideLoader()
        with(result) {
            doIfSuccess {
                getOnDemandServices()
            }
            doIfError {
                binding.stopShimmerAnimation()
                binding.textViewNoCurriculum.text =
                    getString(R.string.curriculum_truck_driver_error)
            }
        }
    }

    private fun getOnDemandServices() {
        viewModel.getOnDemandServices()
    }

    private fun onDemandServicesResult(result: Result<List<OnDemandService>>) {
        hideLoader()
        with(result) {
            doIfSuccess {
                binding.stopShimmerAnimation()
                if (it != null && it.isNotEmpty()) showOnDemandServices(it)
                else binding.showErrorServicesMessageView(getString(R.string.on_demand_service_no_services_error))
            }
            doIfError {
                binding.stopShimmerAnimation()
                showErrorContainer()
            }
        }
    }

    private fun FragmentShowOnDemandServicesBinding.shouldShowOnDemandServices(isVisible: Boolean) {
        showOnDemandServiceContainer()
        noCurriculumLayout.isVisible = !isVisible
        errorOnDemandServicesLayout.isVisible = !isVisible
        onDemandServicesLayout.isVisible = isVisible
        svFilter.clearFocus()
    }

    private fun showOnDemandServices(onDemandServices: List<OnDemandService>) {
        with(binding) {
            shouldShowOnDemandServices(true)
            services = if (onDemandServices.size > 29) onDemandServices.subList(
                0,
                29 //show just the oldest 30 services
            ) else onDemandServices
            adapter = OnDemandServiceAdapter(services)
            recyclerOnDemandServiceView.adapter = adapter
            adapter.onOpenDocument = ::openDocument
        }
        hideLoader()
    }

    private fun getDriverInformation() {
        viewModel.getDriverInformation()
    }

    private fun FragmentShowOnDemandServicesBinding.startShimmerAnimation() {
        shimmerOnDemandServiceLayout.startShimmer()
        shimmerOnDemandServiceLayout.visibility = View.VISIBLE
    }

    private fun FragmentShowOnDemandServicesBinding.stopShimmerAnimation() {
        shimmerOnDemandServiceLayout.stopShimmer()
        shimmerOnDemandServiceLayout.isVisible = false
    }

    private fun FragmentShowOnDemandServicesBinding.showErrorServicesMessageView(message: String) {
        showOnDemandServiceContainer()
        noCurriculumLayout.isVisible = false
        onDemandServicesLayout.isVisible = false
        errorOnDemandServicesLayout.isVisible = true
        tvErrorOnDemandServicesMessage.text = message
    }

    private fun showErrorContainer() {
        binding.fabCreateOnDemandService.isVisible = false
        binding.layoutOnDemandServices.isVisible = false
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = true
    }

    private fun showNoInternetLayout() {
        binding.fabCreateOnDemandService.isVisible = false
        binding.layoutOnDemandServices.isVisible = false
        binding.layoutError.isVisible = false
        binding.noInternetLayout.isVisible = true
    }

    private fun showOnDemandServiceContainer() {
        binding.fabCreateOnDemandService.isVisible = true
        binding.layoutOnDemandServices.isVisible = true
        binding.layoutError.isVisible = false
        binding.noInternetLayout.isVisible = false
    }
}
