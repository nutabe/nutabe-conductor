package com.jortegat.presentation.ui.home.view_documents

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.jortegat.base.helpers.BASE_URL
import com.jortegat.base.helpers.PDF_VIEWER_URL
import com.jortegat.presentation.databinding.ActivityViewDocumentsBinding
import java.io.File

class ViewDocumentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityViewDocumentsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewDocumentsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.extras?.let {
            val fileName = it.getString(RESOURCE) ?: return

            if (fileName.contains(".pdf")) {
                binding.ivPlaceholder.isVisible = false
                binding.pdfPlaceholder.isVisible = true
                binding.pdfPlaceholder.loadFile(fileName)
            } else {
                binding.pdfPlaceholder.isVisible = false
                binding.ivPlaceholder.isVisible = true
                binding.ivPlaceholder.loadImage(fileName)
            }
            binding.fabDownloadFile.setOnClickListener {
                val mimeType = if (fileName.contains(".pdf")) "application/pdf" else "image/*"
                downloadFile(fileName, mimeType)
            }


        }
    }

    private fun downloadFile(fileName: String, mimeType: String): Long {
        val downloadManager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        try {
            val request = DownloadManager.Request(Uri.parse(BASE_URL + fileName))
            request.setTitle(fileName)
                .setDescription("Nutabe conductor")
                .setMimeType(mimeType)
                .setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    File.separator + fileName
                )
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            return downloadManager.enqueue(request)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(
                this,
                "No se puede descargar este archivo.",
                Toast.LENGTH_SHORT
            ).show()
        }
        return -1
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun WebView.loadFile(fileName: String) {
        isVisible = true
        webViewClient = WebViewClient()
        settings.setSupportZoom(true)
        settings.javaScriptEnabled = true
        loadUrl(PDF_VIEWER_URL + BASE_URL + fileName)
    }

    private fun ImageView.loadImage(fileName: String) {
        isVisible = true
        Glide.with(binding.ivPlaceholder)
            .load(BASE_URL + fileName)
            .into(this)
    }

    companion object {
        const val RESOURCE = "resource"
    }
}