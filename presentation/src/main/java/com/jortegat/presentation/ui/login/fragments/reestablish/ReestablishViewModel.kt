package com.jortegat.presentation.ui.login.fragments.reestablish

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.IOException

class ReestablishViewModel @ViewModelInject constructor(private val loginUseCase: LoginUseCase) :
    ViewModel() {

    private val _resetCodeLiveData = MediatorLiveData<Result<Unit>>()
    val resetCodeLiveData: LiveData<Result<Unit>> get() = _resetCodeLiveData

    fun requestResetCode(identification: String) {
        viewModelScope.launch {
            when (
                loginUseCase.requestResetCode(identification)) {
                is Result.Success -> {
                    _resetCodeLiveData.postValue(Result.Success(Unit))
                }
                is Result.Error -> _resetCodeLiveData.postValue(Result.Error(IOException()))
            }
        }
    }
}