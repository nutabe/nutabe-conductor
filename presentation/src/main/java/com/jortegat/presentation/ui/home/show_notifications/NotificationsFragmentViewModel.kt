package com.jortegat.presentation.ui.home.show_notifications

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import com.jortegat.presentation.ui.base.BaseDriverFragmentViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class NotificationsFragmentViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val loginUseCase: LoginUseCase,
    private val dispatcher: CoroutineDispatcher
) : BaseDriverFragmentViewModel(driverUseCase) {

    private val _vehicleInvitationResult = MediatorLiveData<Result<Unit>>()
    val vehicleInvitationResult: LiveData<Result<Unit>> get() = _vehicleInvitationResult

    private val _notificationsResult = MediatorLiveData<Result<List<Notification>>>()
    val notificationsResult: LiveData<Result<List<Notification>>> get() = _notificationsResult

    private val _tokenResult = MediatorLiveData<Result<Unit>>()
    val tokenResult: LiveData<Result<Unit>> get() = _tokenResult

    private val _pendingInvitations = MediatorLiveData<Result<List<PendingInvitation>>>()
    val pendingInvitations: LiveData<Result<List<PendingInvitation>>> get() = _pendingInvitations

    fun getNotifications() {
        viewModelScope.launch {
            _notificationsResult.postValue(driverUseCase.getNotifications())
        }
    }

    fun updateNotificationsToSeen(unreadNotificationsId: List<String>) {
        viewModelScope.launch {
            driverUseCase.updateNotificationsToSeen(unreadNotificationsId)
        }
    }

    fun handleVehicleInvitation(acceptInvitation: Boolean, vehicleInvitationId: String) {
        viewModelScope.launch {
            _vehicleInvitationResult.postValue(
                driverUseCase.handleVehicleInvitation(
                    acceptInvitation,
                    vehicleInvitationId
                )
            )
        }
    }

    fun requestToken() {
        viewModelScope.launch {
            _tokenResult.postValue(loginUseCase.requestToken())
        }
    }

    fun checkIfDriverHasPendingInvitations() {
        viewModelScope.launch {
            _pendingInvitations.postValue(driverUseCase.checkIfDriverHasPendingInvitations())
        }
    }

}