package com.jortegat.presentation.ui.home.show_services

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.gson.Gson
import com.jortegat.domain.driver.model.services.Journey

class ViewPagerAdapter(
    fragment: Fragment,
    private val journeys: MutableList<Journey>,
    private val vehicleId: String
) :
    FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {

        return when (position) {
            0 -> ActiveServicesFragment.newInstance(Gson().toJson(journeys.filter { it.journeyStatus != "FINISHED" && it.journeyStatus != "TIMEOUT" && it.journeyStatus != "DENIED" }))
            else -> FinishedServicesFragment.newInstance(vehicleId)
        }
    }
}