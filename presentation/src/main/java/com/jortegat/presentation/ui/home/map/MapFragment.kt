package com.jortegat.presentation.ui.home.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.maps.GeoApiContext
import com.jortegat.base.enums.NotificationStatus
import com.jortegat.base.extensions.dpToPx
import com.jortegat.base.helpers.*
import com.jortegat.base.models.domain.Destination
import com.jortegat.base.models.domain.Driver
import com.jortegat.base.models.domain.JourneyDestination
import com.jortegat.base.models.ui.ConfirmationDialogInformation
import com.jortegat.base.models.ui.RegistrationDialogConfirmation
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentMapBinding
import com.jortegat.presentation.extensions.bitmapDescriptorFromVector
import com.jortegat.presentation.ui.home.OnNotificationActionListener
import com.jortegat.presentation.ui.home.StartNavigationFragment
import com.jortegat.presentation.ui.home.show_services.JourneyAdapter
import com.jortegat.presentation.ui.home.show_services.service_guide.ServiceGuideDialogManager
import com.onesignal.OneSignal
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.sheet_map.*

@AndroidEntryPoint
class MapFragment : StartNavigationFragment() {

    private val viewModel: MapViewModel by viewModels()
    private var sheetBehavior: BottomSheetBehavior<ConstraintLayout>? = null

    private lateinit var map: GoogleMap

    private var mGeoApiContext: GeoApiContext? = null

    // Variables to get lastKnown location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var currentLocation: Location

    private val locationList = mutableListOf<LatLng>()

    private val onMapReady = OnMapReadyCallback { googleMap ->
        googleMap.setPadding(0, 0, 0, 70.dpToPx.toInt())
        map = googleMap
        zoomToMyPoint()
        enableMyLocation()
    }

    private val binding: FragmentMapBinding get() = _binding!!
    private var _binding: FragmentMapBinding? = null

    private lateinit var journeyAdapter: JourneyAdapter
    private var journeyInProgress: Journey? = null
    private var listener: OnNotificationActionListener? = null

    private var serviceGuideDialogManager: ServiceGuideDialogManager? = null
    private var driverName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Next lines guarantee that the app will go to background when user is on the map and press back
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            true
        ) {
            requireActivity().moveTaskToBack(true)
        }

        setUpBottomSheet()
        startShimmerAnimation()

        initMapAndLocation()

        observers()
        listeners()

        requestToken()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnNotificationActionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnNotificationActionListener")
        }
    }

    private fun requestToken() {
        if (internetManager.isInternetOn()) {
            viewModel.requestToken()
        } else {
            showNoInternetMessage(requireView())
            showNoInternetContainer()
        }
    }

    private fun setMarker(
        title: String,
        latitude: Double,
        longitude: Double,
        @DrawableRes drawable: Int
    ) {
        val markerOptions = MarkerOptions()
            .position(LatLng(latitude, longitude))
            .title(title)
            .icon(requireContext().bitmapDescriptorFromVector(drawable))
        locationList.add(markerOptions.position)
        map.addMarker(markerOptions)
    }

    private fun validateDocumentsAreFilled() {
        viewModel.validateDocumentsAreFilled()
    }

    private fun getNotifications() {
        viewModel.getNotifications()
    }


    private fun observers() {
        viewModel.journeyListResult.observe(viewLifecycleOwner, ::showJourneysInProgressResult)
        viewModel.driverInformationResult.observe(viewLifecycleOwner, ::manageDriverInformation)
        viewModel.finishJourneyResult.observe(viewLifecycleOwner, ::showFinishJourneyResult)
        viewModel.onRemissionDone.observe(viewLifecycleOwner, ::onRemissionDoneResult)
        viewModel.onReceiptDone.observe(viewLifecycleOwner, ::onReceiptDoneResult)
        viewModel.documentsFilled.observe(viewLifecycleOwner, ::requestToFillDocuments)
        viewModel.tokenResult.observe(viewLifecycleOwner, ::onTokenResponse)
        viewModel.notificationsResult.observe(viewLifecycleOwner, ::onNotificationsResult)
    }

    private fun manageDriverInformation(result: Result<Driver>) {
        with(result) {
            doIfSuccess {
                it?.let { driver ->
                    driverName = driver.name
                    val plate = driver.vehicle?.id

                    if (internetManager.isInternetOn()) {
                        if (plate != null) {
                            getListOfJourneysInProgress(plate)
                        } else {
                            stopShimmerAnimation(false)
                            showMessage(
                                "Aún no tienes un vehículo asignado. Comunicate con tu contratista.",
                                requireView()
                            )
                        }
                    } else {
                        showNoInternetMessage(requireView())
                        showNoInternetContainer()
                    }
                }
            }
            doIfError {
            }
        }
    }

    private fun onTokenResponse(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                if (internetManager.isInternetOn()) {
                    validateDocumentsAreFilled()
                    //the intention is to get the notifications to count the unseen. If there is at least one
                    //then update the notification icon
                    getNotifications()
                } else {
                    showNoInternetMessage(requireView())
                    showNoInternetContainer()
                }
            }
            doIfError {
                showMessage(it.message.toString(), requireView())
            }
        }
    }

    private fun showJourneysInProgressResult(journeyList: List<Journey>) {
        journeyList.let {
            if (it.isNotEmpty()) {
                stopShimmerAnimation(true)

                //It will always be only one journey in PROCESS
                journeyInProgress = it.first()

                journeyInProgress?.let { journey ->
                    serviceGuideDialogManager?.updateJourneyPdfGuide(
                        journey.hasVisitedOrigin,
                        journey.hasVisitedDestination
                    )
                }

                journeyAdapter = JourneyAdapter(it)
                journeyAdapter.onFinishClick = ::finishJourney
                journeyAdapter.onPdfClick = ::showPdfOrderJourney
                journeyAdapter.onJourneyClick = ::continueJourney
                binding.bottomSheetMap.recyclerViewProgressJourneys.run {
                    layoutManager = LinearLayoutManager(requireContext())
                    this.adapter = journeyAdapter
                }

                journeyInProgress?.run {
                    showJourneyDestination(this)
                }

            } else {
                stopShimmerAnimation(false)
            }
        }
    }

    private fun showFinishJourneyResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                map.clear()
                locationList.clear()

                showMessage("El viaje fue finalizado exitosamente", requireView())
            }
            doIfError { showMessage(it.message.toString(), requireView()) }
        }
    }

    private fun onRemissionDoneResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                serviceGuideDialogManager?.run {
                    remissionIsDone()
                    showMessage("Remisión guardado con exito", binding.root)
                }
            }
            doIfError {
                serviceGuideDialogManager?.run {
                    showMessage(it.message.toString(), binding.root)
                }
            }
        }
    }

    private fun onReceiptDoneResult(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                serviceGuideDialogManager?.run {
                    receiptIsDone()
                    showMessage("Recibo guardado con exito", binding.root)
                }
            }
            doIfError {
                serviceGuideDialogManager?.run {
                    showMessage(it.message.toString(), binding.root)
                }
            }
        }
    }

    private fun onNotificationsResult(result: Result<List<Notification>>) {
        with(result) {
            doIfSuccess { list ->
                list?.let { notifications ->
                    val unseenCount =
                        notifications.count { it.status == NotificationStatus.UNREAD.serverMessage }
                    if (unseenCount > 0) {
                        //update the bell icon to show the dot (has unseen notifications)
                        listener?.onNotificationAction(true)
                    } else {
                        listener?.onNotificationAction(false)
                    }
                }

            }
            doIfError {
            }
        }
    }

    private fun continueJourney(journey: Journey) {
        journey.run {
            val journeyCanBeFinished =
                journey.hasVisitedOrigin && journey.hasVisitedDestination
            viewModel.sendPendingLocations()
            if (!journeyCanBeFinished) {
                val journeyDestinationDetail = JourneyDestination(
                    Destination(
                        origin.address,
                        origin.businessName,
                        origin.latitude,
                        origin.longitude
                    ),
                    Destination(
                        destination.address,
                        destination.businessName,
                        destination.latitude,
                        destination.longitude
                    )
                )

                startGpsService(
                    journeyId.toString(),
                    journeyDestinationDetail.origin,// this will be used in the service to calculate distance to origin
                    journeyDestinationDetail.destination,// this will be used in the service to calculate distance to destination
                    journey.hasVisitedOrigin,
                    journey.hasVisitedDestination
                )
            } else showJourneyReadyConfirmationDialog(journey)
        }
    }

    private fun showGuideNotCompletedDialog(journey: Journey) {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
            dialogInterface.dismiss()
            showPdfOrderJourney(journey)
        }
        context?.showRegistrationDialogConfirmation(
            RegistrationDialogConfirmation(
                title = "Guia de viaje",
                message = "Los datos en la guia no están completamente diligenciados. Te redireccionaremos a la guia para completar el proceso.",
                positiveButton = "Aceptar",
                positiveCallback = onConfirm
            )
        )
    }

    private fun showJourneyReadyConfirmationDialog(journey: Journey) {
        if (journey.remissionNumber.isNullOrEmpty().not() && journey.receiptNumber.isNullOrEmpty()
                .not()
        ) {
            //no need to show the alert because it was shown before from the else in this same function
        } else {
            val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            context?.showRegistrationDialogConfirmation(
                RegistrationDialogConfirmation(
                    title = "Ahora puedes finalizar este viaje",
                    message = "Asegurate de que los datos en la guia sean correctos y estén completamente diligenciados antes.",
                    positiveButton = "Aceptar",
                    positiveCallback = onConfirm
                )
            )
        }
    }

    private fun requestToFillDocuments(documentsAreFilled: Result<Boolean>) {
        with(documentsAreFilled) {
            doIfSuccess { filled ->
                filled?.let {
                    if (internetManager.isInternetOn()) {
                        getDriverInformation()
                    } else {
                        showNoInternetMessage(requireView())
                        showNoInternetContainer()
                    }
                    OneSignal.promptForPushNotifications()
                }
            }
            doIfError {
                findNavController().navigate(R.id.action_nav_home_to_driverDialogFragment)
            }
        }
    }

    private fun getDriverInformation() {
        viewModel.getDriverInformation()
    }

    private fun getListOfJourneysInProgress(vehicleId: String) {
        viewModel.getListOfJourneysInProgress(vehicleId)
    }

    private fun showJourneyDestination(selectedJourney: Journey) {
        sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        binding.bottomSheetMap.topArrow.isActivated = true
        if (::map.isInitialized.not()) return
        map.clear()
        locationList.clear()
        selectedJourney.origin.apply {
            val originTitle = "Origen: $address"
            setMarker(
                originTitle,
                latitude,
                longitude,
                R.drawable.ic_location_marker
            )
            zoomToLocation(latitude, longitude)
        }
        selectedJourney.destination.apply {
            val destinationTitle = "Destino: $address"
            setMarker(
                destinationTitle,
                latitude,
                longitude,
                R.drawable.ic_location_marker
            )
        }
    }

    private fun listeners() {
        sheetBehavior?.isDraggable = false
        sheetBehavior?.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING, BottomSheetBehavior.STATE_SETTLING ->
                        sheetBehavior!!.setHideable(false)
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
        binding.bottomSheetMap.textViewJourneysInProgress.setOnClickListener {
            animateBottomSheetArrow()
        }
        binding.bottomSheetMap.topArrow.setOnClickListener {
            animateBottomSheetArrow()
        }
    }

    private fun finishJourney(journey: Journey) {
        if (journey.hasVisitedOrigin) {
            if (journey.hasVisitedDestination) {
                if (journey.remissionNumber.isNullOrEmpty()
                        .not() && journey.receiptNumber.isNullOrEmpty().not()
                ) {
                    if (internetManager.isInternetOn()) {
                        val onConfirm: (DialogInterface, Int) -> Unit = { dialog, _ ->
                            dialog.dismiss()
                            viewModel.finishJourney(journeyInProgress!!.journeyId.toString())
                            stopService()
                        }
                        val information = ConfirmationDialogInformation(
                            title = getString(R.string.important_finishing_journey),
                            message = getString(R.string.finish_journey_confirmation_message),
                            negativeButton = getString(R.string.no_go_back),
                            positiveButton = getString(R.string.yes_finish),
                            icon = R.drawable.ic_exclamation_circle,
                            positiveCallback = onConfirm
                        )
                        context?.showConfirmationDialog(information)
                    } else {
                        showNoInternetMessage(requireView())
                    }
                } else {
                    showGuideNotCompletedDialog(journey)
                }

            } else {
                showMessage(
                    "Este vehiculo no ha visitado el punto de control de destino.",
                    requireView()
                )
            }
        } else {
            showMessage(
                "Este vehiculo no ha visitado el punto de control de inicio ni destino.",
                requireView()
            )
        }

    }

    private fun showPdfOrderJourney(journey: Journey) {
        //make sure we send any pending information saved because the device had no internet before.
        //we need to do this before opening the guide in case the driver wasnt to send the remission or receipt and the server still
        // doesnt now the vehicle already passed the origin and destination
        viewModel.sendPendingLocations()

        serviceGuideDialogManager = ServiceGuideDialogManager(
            requireContext(),
            journeyInProgress!!,
            driverName
        )

        serviceGuideDialogManager?.onRemissionDone =
            {
                if (internetManager.isInternetOn()) {
                    viewModel.updateJourneyWithRemission(it)
                } else showNoInternetMessage(requireView())
            }
        serviceGuideDialogManager?.onReceiptDone =
            {
                if (internetManager.isInternetOn()) {
                    viewModel.updateJourneyWithReceipt(it)
                } else showNoInternetMessage(requireView())
            }

        val dialog = serviceGuideDialogManager?.getDialog()
        dialog?.show()
    }

    private fun animateBottomSheetArrow() {
        binding.bottomSheetMap.topArrow.isActivated = !binding.bottomSheetMap.topArrow.isActivated
        if (sheetBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED)
            sheetBehavior?.state = BottomSheetBehavior.STATE_HALF_EXPANDED
        else sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun setUpBottomSheet() {
        sheetBehavior =
            BottomSheetBehavior.from(binding.bottomSheetMap.bottomSheetMap)
        binding.bottomSheetMap.bottomSheetMap.visibility = View.VISIBLE
        binding.bottomSheetMap.topArrow.isActivated = true
    }

    // Get lastKnowPosition after location permissions are granted
    @SuppressLint("MissingPermission")
    private fun initMapAndLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    currentLocation = location
                    setMapFragment()
                }
            }
    }

    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    private fun setMapFragment() {
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(onMapReady)
        if (mGeoApiContext == null) {
            mGeoApiContext =
                GeoApiContext.Builder().apiKey(getString(R.string.google_maps_key)).build()
        }
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        map.isMyLocationEnabled = true
    }

    private fun zoomToMyPoint() {
        val zoomLevel = 15f
        val homeLatLng = LatLng(currentLocation.latitude, currentLocation.longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(homeLatLng, zoomLevel))
    }

    private fun zoomToLocation(latitude: Double, longitude: Double) {
        val zoomLevel = 10f

        val homeLatLng = LatLng(latitude, longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(homeLatLng, zoomLevel))
    }

    //this function is used when the points are set from a different fragment
    /* private fun setJourneyDestinationPoints() {
         journeyDestination?.apply {
             if (::map.isInitialized) {
                 map.clear()
                 val originTitle = "Origen: ${origin.address}"
                 setMarker(
                     originTitle,
                     origin.latitude,
                     origin.longitude,
                     R.drawable.ic_location_marker
                 )
                 val destinationTitle = "Destino: ${destination.address}"
                 setMarker(
                     destinationTitle,
                     destination.latitude,
                     destination.longitude,
                     R.drawable.ic_location_marker
                 )
                 zoomToLocation(origin.latitude, origin.longitude)
             }
         }
     }*/

    private fun startShimmerAnimation() {
        shimmerLayout.startShimmer()
        shimmerLayout.visibility = View.VISIBLE
    }

    private fun stopShimmerAnimation(hasJourneys: Boolean) {
        shimmerLayout.stopShimmer()
        shimmerLayout.visibility = View.GONE
        layoutJourneysInProgress.isVisible = hasJourneys
        layoutNoStartedJourneys.isVisible = !hasJourneys
        layoutNoInternet.isVisible = false
    }

    private fun showNoInternetContainer() {
        shimmerLayout.stopShimmer()
        shimmerLayout.visibility = View.GONE
        layoutJourneysInProgress.isVisible = false
        layoutNoStartedJourneys.isVisible = false
        layoutNoInternet.isVisible = true
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}