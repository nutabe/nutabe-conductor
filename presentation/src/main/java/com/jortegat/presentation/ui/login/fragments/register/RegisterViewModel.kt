package com.jortegat.presentation.ui.login.fragments.register

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.RegistrationCode
import com.jortegat.base.models.UserRegistrationForm
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class RegisterViewModel @ViewModelInject constructor(private val loginUseCase: LoginUseCase) :
    ViewModel() {

    private val _registerLiveData = MediatorLiveData<Result<Unit>>()
    val registerLiveData: LiveData<Result<Unit>> get() = _registerLiveData

    fun register(registration: UserRegistrationForm) {
        viewModelScope.launch {
            when (val result = loginUseCase.register(registration)) {
                is Result.Success -> {
                    _registerLiveData.postValue(Result.Success(Unit))
                }
                is Result.Error -> _registerLiveData.postValue(Result.Error(result.exception))
            }
        }
    }
}