package com.jortegat.presentation.ui.welcomecards

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.jortegat.base.ui.adapters.ViewPagerAdapter
import com.jortegat.base.ui.adapters.ViewPagerCallback
import com.jortegat.base.ui.welcome_cards.card_fragments.FirstWelcomeFragment
import com.jortegat.base.ui.welcome_cards.card_fragments.SecondWelcomeFragment
import com.jortegat.base.ui.welcome_cards.card_fragments.ThirdWelcomeFragment
import com.jortegat.presentation.databinding.ActivityWelcomeCardsBinding
import com.jortegat.presentation.ui.login.fragments.login.LoginActivity

class WelcomeCardsActivity : FragmentActivity() {

    private lateinit var binding: ActivityWelcomeCardsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeCardsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fragments =
            listOf(
                FirstWelcomeFragment.instance,
                SecondWelcomeFragment.instance,
                ThirdWelcomeFragment.instance
            )

        val adapter =
            ViewPagerAdapter(
                this,
                fragments
            )

        binding.welcomeFragmentsPager.adapter = adapter

        TabLayoutMediator(binding.tabLayout, binding.welcomeFragmentsPager) { _, _ ->
        }.attach()

        var currentFragment = 0
        binding.welcomeFragmentsPager.registerOnPageChangeCallback(ViewPagerCallback {
            currentFragment = it
        })

        binding.continueCardButton.setOnClickListener {
            if (currentFragment == 2) {
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                binding.welcomeFragmentsPager.currentItem += 1
            }
        }
    }
}