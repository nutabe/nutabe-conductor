package com.jortegat.presentation.ui.register_driver

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.presentation.ui.base.BaseDriverFragmentViewModel
import kotlinx.coroutines.launch

class DriverDialogFragmentViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase
) : BaseDriverFragmentViewModel(driverUseCase) {

    private val _uploadDriver = MediatorLiveData<Result<Unit>>()
    val uploadDriver: LiveData<Result<Unit>> get() = _uploadDriver

    fun getFrontPhotoPath() = driverUseCase.getFrontPhotoPath()

    fun getBackPhotoPath() = driverUseCase.getBackPhotoPath()

    fun uploadDriverData(
        licenseCategory: String,
        frontPhotoPath: String,
        backPhotoPath: String
    ) {
        viewModelScope.launch {
            _uploadDriver.postValue(
                driverUseCase.uploadDriverData(licenseCategory, frontPhotoPath, backPhotoPath)
            )
        }
    }
}