package com.jortegat.presentation.ui.home.create_on_demand_services

import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.jortegat.base.enums.ServiceUnitType
import com.jortegat.base.helpers.*
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.Driver
import com.jortegat.base.models.on_demand_material.OnDemandMaterial
import com.jortegat.base.models.ui.PlaceSpinner
import com.jortegat.base.models.ui.RegistrationDialogConfirmation
import com.jortegat.base.ui.adapters.PlaceSpinnerAdapter
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverBuildingSite
import com.jortegat.domain.helper.*
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentCreateOnDemandServicesBinding
import com.jortegat.presentation.extensions.onBackPressed
import com.jortegat.presentation.model.mappers.PlacesMappers
import com.jortegat.presentation.ui.base.BaseDriverFragment
import com.jortegat.presentation.ui.home.HomeActivity.Companion.event
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@AndroidEntryPoint
class CreateOnDemandServiceFragment : BaseDriverFragment() {

    private val viewModel: CreateOnDemandServiceFragmentViewModel by viewModels()

    private var _binding: FragmentCreateOnDemandServicesBinding? = null
    private val binding: FragmentCreateOnDemandServicesBinding get() = _binding!!

    private lateinit var buildingSiteSelected: String

    private var quarriesAndDumps: MutableList<Place>? = null
    private var materials: List<OnDemandMaterial>? = null

    private val origins = mutableListOf<PlaceSpinner>()
    private val destinations = mutableListOf<PlaceSpinner>()

    private var listener: OnPdfUploadFragmentListener? = null

    private var buildingSpinnerSelection: DriverBuildingSite? = null
    private var originSpinnerSelection: PlaceSpinner? = null

    private var driver: Driver? = null

    private val eventReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateIcons(intent)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreateOnDemandServicesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setObservers()

        binding.initView()

        fetchData()

        binding.listeners()

        registerEventReceiver()
    }

    private fun fetchData() {
        if (internetManager.isInternetOn()) {
            showCreateOnDemandServiceContainer()
            getQuarryAndDumps()
            getBuildingSites()
            getMaterials()
            getDriverInformation()
        } else {
            showNoInternetLayout()
        }
    }

    private fun registerEventReceiver() {
        val eventFilter = IntentFilter()
        eventFilter.addAction(event)
        requireActivity().registerReceiver(eventReceiver, eventFilter)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnPdfUploadFragmentListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnPdfUploadFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun FragmentCreateOnDemandServicesBinding.initView() {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        tvDate.text = current.format(formatter)
        initSpinners()
    }

    private fun getBuildingSites() {
        showLoader()
        viewModel.getBuildingSites()
    }

    private fun getQuarryAndDumps() {
        viewModel.getQuarryAndDump()
    }

    private fun getMaterials() {
        viewModel.getMaterials()
    }

    private fun getDriverInformation() {
        viewModel.getDriverInformation()
    }

    private fun initSpinners() {
        binding.run {
            initSpinner(BUILDING_SITE_DEFAULT_NAME, spBuildingSite)
            initSpinner(ORIGIN, spOrigin)
            initSpinner(DESTINATION, spDestination)
            initSpinner(MATERIAL, spMaterial)
        }
    }

    private fun FragmentCreateOnDemandServicesBinding.listeners() {
        etInitialTime.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                context?.showTimePickerDialog(binding.etInitialTime)
                activity?.hideKeyboard()
            }
        }
        etInitialTime.setOnClickListener {
            context?.showTimePickerDialog(binding.etInitialTime)
            activity?.hideKeyboard()
        }

        etFinalTime.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                context?.showTimePickerDialog(binding.etFinalTime)
                activity?.hideKeyboard()
            }
        }
        etFinalTime.setOnClickListener {
            context?.showTimePickerDialog(binding.etFinalTime)
            activity?.hideKeyboard()
        }

        btPdfOriginRemission.setOnClickListener {
            listener?.onUploadOriginFile()
        }

        btPdfDestinationRemission.setOnClickListener {
            listener?.onUploadDestinationFile()
        }

        btPdfOtherSupportDocument.setOnClickListener {
            listener?.onUploadSupportFile()
        }

        fabSaveOnDemandService.setOnClickListener {
            if (binding.fieldsAreFilled()) {
                binding.sendOnDemandService()
            } else showMessage(getString(R.string.field_mandatory), requireView())
        }

        requireActivity().onBackPressed {
            findNavController().navigate(
                R.id.action_createOnDemandServiceFragment_to_showOnDemandServiceFragment,
                Bundle().apply {
                    putBoolean("shouldRecreateAdapterOnTextChanged", false)
                }
            )
        }
    }

    private fun FragmentCreateOnDemandServicesBinding.fieldsAreFilled(): Boolean {
        return buildingSpinnerSelection != null &&
                originSpinnerSelection != null &&
                spDestination.selectedItemPosition != 0 &&
                spMaterial.selectedItemPosition != 0 &&
                etVolume.text.toString().isNotEmpty() &&
                etInitialKilometers.text.toString().isNotEmpty() &&
                etFinalKilometers.text.toString().isNotEmpty() &&
                etInitialTime.text.toString().isNotEmpty() &&
                etFinalTime.text.toString().isNotEmpty() &&
                documentsAreAttached()
    }

    private fun documentsAreAttached(): Boolean {
        return viewModel.documentsAreAttached()
    }

    private fun FragmentCreateOnDemandServicesBinding.sendOnDemandService() {
        if (driver == null) {
            showMessage(getString(R.string.on_demand_service_error_driver_null), requireView())
            return
        }
        if (driver!!.vehicle == null) {
            showMessage(getString(R.string.on_demand_service_error_vehicle_null), requireView())
            return
        }
        if (materials == null) {
            showMessage(getString(R.string.on_demand_service_error_material_null), requireView())
            return
        }

        val destination = destinations[spDestination.selectedItemPosition]
        val material = materials!![spMaterial.selectedItemPosition]
        val finalHorometer =
            if (etFinalHorometer.text.toString().isNotEmpty()) etFinalHorometer.text.toString()
                .toInt() else null
        val startHorometer =
            if (etInitialHorometer.text.toString().isNotEmpty()) etInitialHorometer.text.toString()
                .toInt() else null
        val finalPk = etFinalPk.text.toString().ifEmpty { null }
        val startPk = etInitialPk.text.toString().ifEmpty { null }
        val createOnDemandService = CreateOnDemandService(
            buildingSpinnerSelection!!.id,
            tvDate.text.toString(),
            destination.id.toLong(),
            destination.type,
            driver!!.identification,
            finalHorometer,
            etInitialTime.text.toString(),
            etFinalKilometers.text.toString().toInt(),
            finalPk,
            etVolume.text.toString().toDouble(),
            material.id,
            originSpinnerSelection!!.id.toLong(),
            originSpinnerSelection!!.type,
            ServiceUnitType.M3.serverValue,
            startHorometer,
            etFinalTime.text.toString(),
            etInitialKilometers.text.toString().toInt(),
            startPk,
            driver!!.vehicle!!.id
        )

        createService(createOnDemandService)
    }

    private fun createService(createOnDemandService: CreateOnDemandService) {
        showLoader()
        viewModel.createOnDemandService(createOnDemandService)
    }


    private fun setObservers() {
        viewModel.buildingSites.observe(viewLifecycleOwner, ::onBuildingSitesResponse)
        viewModel.quarryAndDump.observe(viewLifecycleOwner, ::onQuarryAndDumpResponse)
        viewModel.materials.observe(viewLifecycleOwner, ::onMaterialResponse)
        viewModel.driver.observe(viewLifecycleOwner, ::onDriverInformationResponse)
        viewModel.onCreateOnDemandService.observe(viewLifecycleOwner, ::onCreateOnDemandService)
        viewModel.onUploadDemandServiceDocuments.observe(viewLifecycleOwner, ::onDocumentsUploaded)
        internetManager.observe(viewLifecycleOwner, ::onInternetChange)

    }

    private fun onInternetChange(hasInternet: Boolean?) {
        if (hasInternet == true) {
            if (binding.noInternetLayout.isVisible) {
                showCreateOnDemandServiceContainer()
                getQuarryAndDumps()
                getBuildingSites()
                getMaterials()
                getDriverInformation()
            }
        }
    }

    private fun onCreateOnDemandService(result: Result<Int>) {
        when (result) {
            is Result.Success -> {
                uploadDemandServiceDocuments(result.data)
            }
            is Result.Error -> {
                hideLoader()
                showMessage(result.exception.message.toString(), requireView())
            }
        }
    }

    private fun onDocumentsUploaded(succeed: Boolean) {
        hideLoader()
        if (succeed) {
            resetThePathSavedForPdfs()
            showSuccessConfirmationDialog()
        } else showMessage(
            "Tuvimos un fallo guardando los documentos, por favor comunicate con tu contratista.",
            requireView()
        )
    }

    private fun resetThePathSavedForPdfs() {
        viewModel.resetThePathSavedForPdfs()
    }

    private fun showSuccessConfirmationDialog() {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
            dialogInterface.dismiss()
            //this flag shouldRecreateAdapterOnTextChanged, is right now used because when going back to the CreateOnDemandServiceFragment
            //it makes the binding.svFilter.setOnQueryTextListener() triggered.
            findNavController().navigate(
                R.id.action_createOnDemandServiceFragment_to_showOnDemandServiceFragment,
                Bundle().apply {
                    putBoolean("shouldRecreateAdapterOnTextChanged", false)
                }
            )
        }
        context?.showRegistrationDialogConfirmation(
            RegistrationDialogConfirmation(
                title = "Viaje creado con exito",
                message = "Serás redireccionado a la lista de viajes.",
                positiveCallback = onConfirm
            )
        )
    }

    private fun FragmentCreateOnDemandServicesBinding.showValidatingContainer() {
        containerCreatingService.isVisible = false
        fabSaveOnDemandService.isVisible = false
        containerValidatingData.isVisible = true
        validatinDataAnimation.playAnimation()
    }

    private fun uploadDemandServiceDocuments(serviceId: Int) {
        viewModel.uploadDemandServiceDocuments(serviceId)
    }

    private fun onDriverInformationResponse(result: Result<Driver>) {
        with(result) {
            doIfSuccess {
                it?.let {
                    if (it.vehicle == null) {
                        showErrorContainer()
                        return
                    }
                    driver = it
                }
            }
            doIfError {
                showErrorContainer()
            }
        }
    }

    private fun onMaterialResponse(result: Result<List<OnDemandMaterial>>) {
        with(result) {
            doIfSuccess {
                it?.let { fetchMaterialSpinner(it) }
            }
            doIfError {
                showMessage(
                    it.message.toString(),
                    requireView()
                )
            }
        }
    }

    private fun onBuildingSitesResponse(buildingSites: Result<List<DriverBuildingSite>>) {
        hideLoader()
        with(buildingSites) {
            doIfSuccess {
                fetchBuildingSpinner(it)
            }
            doIfError {
                showMessage(
                    it.message.toString(),
                    requireView()
                )
            }
        }
    }

    private fun onQuarryAndDumpResponse(result: Result<List<Place>>) {
        with(result) {
            doIfSuccess { quarryDumpList ->
                if (quarryDumpList.isNullOrEmpty()) return
                quarriesAndDumps = quarryDumpList.toMutableList()

                val places = quarriesAndDumps!!
                if (!buildingSpinnerSelection?.driverFrontLoads.isNullOrEmpty()) {
                    val frontLoadsToPlaces =
                        buildingSpinnerSelection?.driverFrontLoads!!.map {
                            PlacesMappers.mapFrontLoadToPlace(
                                it
                            )
                        }
                    places.addAll(frontLoadsToPlaces)
                }
                initPlaces(places)
            }
            doIfError {
                showMessage(
                    it.message.toString(),
                    requireView()
                )
            }
        }
    }

    private fun initPlaces(places: MutableList<Place>) {
        if (places.isEmpty()) return

        places.groupBy(Place::type).run {

            val dumpList = mutableListOf(PlaceSpinner("0", "Botaderos", "", true))
            val quarryList = mutableListOf(PlaceSpinner("0", "Canteras", "", true))
            val frontLoadList = mutableListOf(PlaceSpinner("0", "Frentes de carga", "", true))

            if (!quarriesAndDumps.isNullOrEmpty()) {
                dumpList.addAll(getValue(DUMP_TYPE).map { convertToPlaceSpinner(it, DUMP_TYPE) })

                quarryList.addAll(getValue(QUARRY_TYPE).map {
                    convertToPlaceSpinner(
                        it,
                        QUARRY_TYPE
                    )
                })
            }
            if (!buildingSpinnerSelection?.driverFrontLoads.isNullOrEmpty()) {
                frontLoadList.apply {
                    addAll(getValue(FRONT_LOAD_TYPE).map {
                        convertToPlaceSpinner(
                            it,
                            FRONT_LOAD_TYPE
                        )
                    })
                }
            }
            initPlacesSpinner(frontLoadList, quarryList, dumpList)
        }
    }

    private fun convertToPlaceSpinner(place: Place, type: String): PlaceSpinner {
        return place.run { PlaceSpinner(identification, address, type, false) }
    }

    private fun initPlacesSpinner(
        frontLoads: List<PlaceSpinner>,
        quarries: List<PlaceSpinner>,
        dumps: List<PlaceSpinner>
    ) {
        val unselectedOrigin = PlaceSpinner(
            id = "0", name = ORIGIN, type = "", isHeader = false
        )
        origins.add(unselectedOrigin)

        origins.addAll(frontLoads.plus(quarries))
        binding.spOrigin.adapter = getAdapter(origins)


        binding.spOrigin.onItemSelectedListener =
            originSelectedListener(origins, frontLoads, dumps)

        destinations.add(getUnselectedDestination())
        binding.spDestination.adapter = getAdapter(destinations)
    }

    private fun originSelectedListener(
        origins: List<PlaceSpinner>,
        frontLoads: List<PlaceSpinner>,
        dumps: List<PlaceSpinner>
    ): AdapterView.OnItemSelectedListener {

        return object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                destinations.clear()
                destinations.add(getUnselectedDestination())
                originSpinnerSelection = origins[position]

                if (position == 0) {
                    binding.spDestination.adapter = getAdapter(destinations)
                    originSpinnerSelection = null
                    return
                }

                val currentPlace = origins[position]
                val frontLoadsCopy = frontLoads.map { it.copy() }.toMutableList()

                if (currentPlace.type == FRONT_LOAD_TYPE) {
                    frontLoadsCopy.remove(currentPlace)
                }

                destinations.addAll(frontLoadsCopy.plus(dumps))
                binding.spDestination.adapter = getAdapter(destinations)
                return

            }
        }
    }

    private fun getUnselectedDestination(): PlaceSpinner {
        return PlaceSpinner(
            id = "0",
            name = DESTINATION,
            type = "",
            isHeader = false
        )
    }

    private fun getAdapter(places: List<PlaceSpinner>): PlaceSpinnerAdapter {
        return PlaceSpinnerAdapter(requireContext(), places)
    }

    private fun initSpinner(defaultSelection: String, spinner: Spinner) {
        spinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            listOf(defaultSelection)
        )
    }

    private fun fetchMaterialSpinner(materials: List<OnDemandMaterial>) {
        this.materials = materials
        val materialList = mutableListOf<String>()
        materialList.add(MATERIAL)
        materialList.addAll(materials.map { it.name })
        binding.spMaterial.adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                materialList
            )
    }

    private fun fetchBuildingSpinner(buildingSites: List<DriverBuildingSite>?) {
        val buildingList = mutableListOf<String>()
        buildingList.add(BUILDING_SITE_DEFAULT_NAME)
        buildingSites?.forEach { buildingList.add(it.name) }
        binding.spBuildingSite.adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                buildingList
            )

        binding.spBuildingSite.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    buildingSiteSelected = parent!!.getItemAtPosition(position).toString()
                    //when a new building is selected then reset the state of the other dependants spinners
                    originSpinnerSelection = null
                    buildingSpinnerSelection = null
                    if (buildingSiteSelected != BUILDING_SITE_DEFAULT_NAME) {
                        buildingSpinnerSelection =
                            buildingSites?.first { it.name == buildingSiteSelected }

                        if (buildingSpinnerSelection?.driverFrontLoads.isNullOrEmpty()) return
                        val frontLoadsToPlaces =
                            buildingSpinnerSelection?.driverFrontLoads!!.map {
                                PlacesMappers.mapFrontLoadToPlace(
                                    it
                                )
                            }

                        val places: MutableList<Place> = frontLoadsToPlaces.toMutableList()
                        if (!quarriesAndDumps.isNullOrEmpty()) {
                            places.addAll(quarriesAndDumps!!)
                        }

                        clearSelectedOriginAndDestination()
                        initPlaces(places)
                    }
                }
            }
    }

    private fun updateIcons(intent: Intent?) {
        intent?.run {
            val fileType = getStringExtra(ON_DEMAND_FILE_TYPE)
            val isLoaded = getBooleanExtra(IS_LOADED, false)

            fileType?.let { type ->
                when (type) {
                    ORIGIN_FILE -> binding.btAttachOriginRemission.isVisible = isLoaded
                    DESTINATION_FILE -> binding.btAttachDestinationRemission.isVisible = isLoaded
                    SUPPORT_FILE -> binding.btAttachSupportDocument.isVisible = isLoaded
                }
            }
        }
    }

    private fun clearSelectedOriginAndDestination() {
        origins.clear()
        destinations.clear()
    }

    private fun showErrorContainer() {
        binding.containerValidatingData.isVisible = false
        binding.containerCreatingService.isVisible = false
        binding.fabSaveOnDemandService.isVisible = false
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = true
    }

    private fun showNoInternetLayout() {
        binding.containerValidatingData.isVisible = false
        binding.containerCreatingService.isVisible = false
        binding.fabSaveOnDemandService.isVisible = false
        binding.layoutError.isVisible = false
        binding.noInternetLayout.isVisible = true
    }

    private fun showCreateOnDemandServiceContainer() {
        binding.containerValidatingData.isVisible = false
        binding.fabSaveOnDemandService.isVisible = true
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = false
        binding.containerCreatingService.isVisible = true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().unregisterReceiver(eventReceiver)
        _binding = null
    }

    companion object {
        private const val BUILDING_SITE_DEFAULT_NAME = "*Obra"
        private const val ORIGIN = "*Origen"
        private const val DESTINATION = "*Destino"
        private const val MATERIAL = "*Material"
    }
}

interface OnPdfUploadFragmentListener {
    fun onUploadOriginFile()
    fun onUploadDestinationFile()
    fun onUploadSupportFile()
}
