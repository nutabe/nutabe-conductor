package com.jortegat.presentation.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.extensions.runWithDelay
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.domain.CustomJourney
import com.jortegat.base.models.domain.JourneyDestination
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.usecase.DriverUseCase
import kotlinx.coroutines.launch

open class ValidationViewModel(private val driverUseCase: DriverUseCase) : ViewModel() {

    private var retryCounter = 0

    private val _journeyDetailResult = MediatorLiveData<Result<CustomJourney?>>()
    val journeyDetailResult: LiveData<Result<CustomJourney?>> get() = _journeyDetailResult

    fun getJourneyDetail(id: String) {
        viewModelScope.launch {
            _journeyDetailResult.postValue(driverUseCase.getJourneyDetail(id))
        }
    }

    fun isValidationTimedOut(id: String): Boolean {
        return if (retryCounter < RETRY_ATTEMPTS) {
            viewModelScope.runWithDelay(RETRY_DELAY) {
                getJourneyDetail(id)
                retryCounter++
            }
            false
        } else {
            retryCounter = 0
            true
        }
    }

    companion object {
        private const val RETRY_DELAY = 10000L
        private const val RETRY_ATTEMPTS = 5
    }
}