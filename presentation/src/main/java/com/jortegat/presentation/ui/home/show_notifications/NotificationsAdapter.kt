package com.jortegat.presentation.ui.home.show_notifications

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.NotificationItemBinding

class NotificationsAdapter(private var notifications: List<Notification>) :
    RecyclerView.Adapter<NotificationsAdapter.NotificationsHolder>() {

    private val binding: NotificationItemBinding get() = _binding!!
    private var _binding: NotificationItemBinding? = null

    var onAcceptClick: ((String) -> Unit)? = null
    var onRejectClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationsHolder {
        _binding =
            NotificationItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return NotificationsHolder(binding)
    }

    override fun onBindViewHolder(holder: NotificationsHolder, position: Int) {
        val item = notifications[position]
        holder.bindNotification(item)
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    inner class NotificationsHolder(private val v: NotificationItemBinding) :
        RecyclerView.ViewHolder(v.root) {

        private var notification: Notification? = null

        fun bindNotification(notification: Notification) {
            this.notification = notification

            /*    val journeyDateParsed = element.date.subSequence(0, 10)*/

            v.apply {

                parentCardView.startAnimation(
                    AnimationUtils.loadAnimation(
                        v.root.context,
                        R.anim.anim_recycler
                    )
                )

                notification.run {
                    val dateAndTime = createdAt.split("T")
                    val dateOnly = dateAndTime[0].replace("-", "/")
                    val timeOnly = dateAndTime[1].subSequence(0, 5)
                    tvNotificationTime.text = "$dateOnly - $timeOnly"
                    tvNotificationBody.text = body

                }
            }
        }

    }

}
