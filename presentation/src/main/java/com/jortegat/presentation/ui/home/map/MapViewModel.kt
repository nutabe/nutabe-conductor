package com.jortegat.presentation.ui.home.map

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.domain.Driver
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MapViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val loginUseCase: LoginUseCase
) : ViewModel() {

    private val _journeyListResult = MediatorLiveData<List<Journey>>()
    val journeyListResult: LiveData<List<Journey>> get() = _journeyListResult

    val driverInformationResult: LiveData<Result<Driver>> get() = _driverInformationResult
    private val _driverInformationResult = MediatorLiveData<Result<Driver>>()

    private val _finishJourney = MediatorLiveData<Result<Unit>>()
    val finishJourneyResult: LiveData<Result<Unit>> get() = _finishJourney

    val onRemissionDone: LiveData<Result<Unit>> get() = _onRemissionDone
    private val _onRemissionDone = MediatorLiveData<Result<Unit>>()

    val onReceiptDone: LiveData<Result<Unit>> get() = _onReceiptDone
    private val _onReceiptDone = MediatorLiveData<Result<Unit>>()

    private val _documentsFilled = MediatorLiveData<Result<Boolean>>()
    val documentsFilled: LiveData<Result<Boolean>> get() = _documentsFilled

    private val _tokenResult = MediatorLiveData<Result<Unit>>()
    val tokenResult: LiveData<Result<Unit>> get() = _tokenResult

    private val _notificationsResult = MediatorLiveData<Result<List<Notification>>>()
    val notificationsResult: LiveData<Result<List<Notification>>> get() = _notificationsResult

    fun getDriverInformation() {
        viewModelScope.launch {
            _driverInformationResult.postValue(driverUseCase.getDriverInformation())
        }
    }

    fun validateDocumentsAreFilled() {
        viewModelScope.launch {
            _documentsFilled.postValue(driverUseCase.validateDocumentsAreFilled())
        }
    }

    fun getListOfJourneysInProgress(vehicleId: String) {
        viewModelScope.launch(Dispatchers.Main) {
            _journeyListResult.addSource(driverUseCase.getListOfJourneys(vehicleId)) {
                _journeyListResult.postValue(it.filter { journey -> journey.journeyStatus == "PROCESS" })
            }
        }
    }

    fun finishJourney(id: String) {
        viewModelScope.launch {
            _finishJourney.postValue(driverUseCase.finishJourney(id))
        }
    }

    fun updateJourneyInDb(journey: Journey) {
        viewModelScope.launch { driverUseCase.updateJourneyInDb(journey) }
    }

    fun updateJourneyWithRemission(journey: Journey) {
        viewModelScope.launch {
            _onRemissionDone.postValue(driverUseCase.updateJourneyWithRemission(journey))
        }
    }

    fun updateJourneyWithReceipt(journey: Journey) {
        viewModelScope.launch {
            _onReceiptDone.postValue(driverUseCase.updateJourneyWithReceipt(journey))
        }
    }

    fun sendPendingLocations() {
        viewModelScope.launch {
            driverUseCase.sendPendingLocations()
        }
    }

    fun requestToken() {
        viewModelScope.launch {
            _tokenResult.postValue(loginUseCase.requestToken())
        }
    }

    fun getNotifications() {
        viewModelScope.launch {
            _notificationsResult.postValue(driverUseCase.getNotifications())
        }
    }

}