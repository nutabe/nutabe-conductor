package com.jortegat.presentation.ui.home.show_curriculum_vitae

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.domain.Driver
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import com.jortegat.presentation.ui.base.BaseDriverFragmentViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class DriverCurriculumVitaeFragmentViewModel @ViewModelInject constructor(
    private val loginUseCase: LoginUseCase,
    private val driverUseCase: DriverUseCase,
    private val dispatcher: CoroutineDispatcher
) : BaseDriverFragmentViewModel(driverUseCase) {

    val driverInformationResult: LiveData<Result<Driver>> get() = _driverInformationResult
    private val _driverInformationResult = MediatorLiveData<Result<Driver>>()

    private val _documentsFilled = MediatorLiveData<Result<Boolean>>()
    val documentsFilled: LiveData<Result<Boolean>> get() = _documentsFilled

    private val _tokenResult = MediatorLiveData<Result<Unit>>()
    val tokenResult: LiveData<Result<Unit>> get() = _tokenResult

    fun getDriverInformation() {
        viewModelScope.launch(dispatcher) {
            _driverInformationResult.postValue(driverUseCase.getDriverInformation())
        }
    }

    fun validateDocumentsAreFilled() {
        viewModelScope.launch {
            _documentsFilled.postValue(driverUseCase.validateDocumentsAreFilled())
        }
    }

    fun requestToken() {
        viewModelScope.launch {
            _tokenResult.postValue(loginUseCase.requestToken())
        }
    }
}