package com.jortegat.presentation.ui.register_driver

import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jortegat.base.helpers.*
import com.jortegat.base.models.ui.RegistrationDialogConfirmation
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentRegisterDriverBinding
import com.jortegat.presentation.extensions.onBackPressed
import com.jortegat.presentation.ui.base.BaseDriverFragment
import com.skydoves.powerspinner.OnSpinnerItemSelectedListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DriverDialogFragment : BaseDriverFragment() {

    private val viewModel: DriverDialogFragmentViewModel by viewModels()

    private var listener: OnDialogFragmentListener? = null
    private var licenseCategory: String = ""
    private lateinit var frontPhotoPath: String
    private lateinit var backPhotoPath: String

    private val eventReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            binding.updateIcons(intent)
        }
    }

    private val binding: FragmentRegisterDriverBinding get() = _binding!!
    private var _binding: FragmentRegisterDriverBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterDriverBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDialogFragmentListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnDialogFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerEventReceiver()
        listeners()
        observers()
    }

    private fun observers() {
        viewModel.uploadDriver.observe(viewLifecycleOwner, Observer(::showSuccessScreen))
    }

    private fun showSuccessScreen(result: Result<Unit>?) {
        hideLoader()
        result?.let {
            when (it) {
                is Result.Success -> {
                    showMessage(
                        getString(R.string.complete_driver_registration_success),
                        requireView()
                    )
                    showSuccessConfirmationDialog()
                }
                is Result.Error -> showMessage(
                    getString(R.string.complete_driver_registration_error), requireView()
                )
            }
        }
    }

    private fun showSuccessConfirmationDialog() {
        val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
            dialogInterface.dismiss()
            findNavController().navigate(R.id.action_driverDialogFragment_to_driverCurriculumVitaeFragment)
        }
        context?.showRegistrationDialogConfirmation(
            RegistrationDialogConfirmation(
                message = "Tus datos serán validados por el personal administrativo de Nutabe.",
                positiveCallback = onConfirm
            )
        )
    }

    private fun listeners() {
        requireActivity().onBackPressed {
            findNavController().navigate(R.id.action_driverDialogFragment_to_nav_home)
        }

        binding.buttonUploadLicenseFrontalPhoto.setOnClickListener {
            listener?.onUploadFrontalFile()
        }

        binding.buttonUploadLicenseBackPhoto.setOnClickListener {
            listener?.onUploadBackFile()
        }

        binding.buttonUploadDriverData.setOnClickListener {
            if (validateFields()) {
                showLoader()
                viewModel.uploadDriverData(
                    licenseCategory,
                    frontPhotoPath,
                    backPhotoPath
                )
            } else showMessage(getString(R.string.all_fields_are_require_error), requireView())
        }

        binding.spinnerLicenseCategory.setOnSpinnerItemSelectedListener(
            OnSpinnerItemSelectedListener<String?> { _, _, _, selection ->
                selection?.let { licenseCategory = it }
            })

    }

    private fun validateFields(): Boolean {
        frontPhotoPath = viewModel.getFrontPhotoPath()
        backPhotoPath = viewModel.getBackPhotoPath()
        return licenseCategory.isNotEmpty() && frontPhotoPath.isNotEmpty() && backPhotoPath.isNotEmpty()
    }

    private fun registerEventReceiver() {
        val eventFilter = IntentFilter()
        eventFilter.addAction(event)
        requireActivity().registerReceiver(eventReceiver, eventFilter)
    }

    private fun FragmentRegisterDriverBinding.updateIcons(intent: Intent?) {
        intent?.run {
            val isFront = getBooleanExtra(IS_FRONT, false)
            val isLoaded = getBooleanExtra(IS_LOADED, false)

            if (isFront) {
                buttonUploadLicenseFrontPhotoCheck.isVisible = isLoaded
            } else {
                buttonUploadLicenseBackPhotoCheck.isVisible = isLoaded
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().unregisterReceiver(eventReceiver)
        _binding = null
    }

    companion object {
        const val event = "com.jortegat.presentation.ui.home.FILE_LOADED"
    }
}

interface OnDialogFragmentListener {
    fun onUploadFrontalFile()
    fun onUploadBackFile()
}