package com.jortegat.presentation.ui.home.show_services

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.base.models.domain.Vehicle
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import com.jortegat.presentation.ui.home.ValidationViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class ShowServicesViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val loginUseCase: LoginUseCase,
) : ValidationViewModel(driverUseCase) {

    private val _journeyListResult = MediatorLiveData<List<Journey>>()
    val journeyListResult: LiveData<List<Journey>> get() = _journeyListResult

    private val _vehicle = MediatorLiveData<Vehicle?>()
    val vehicle: LiveData<Vehicle?> get() = _vehicle

    private val _tokenRequestResult = MediatorLiveData<Result<Unit>>()
    val tokenRequestResult: LiveData<Result<Unit>> get() = _tokenRequestResult

    fun getListOfJourneys(vehicleId: String) {
        viewModelScope.launch(Dispatchers.Main) {
            val journeyList = driverUseCase.getListOfJourneys(vehicleId)
            _journeyListResult.addSource(journeyList) {
                _journeyListResult.postValue(it)
            }
        }
    }

    fun getVehicle() {
        viewModelScope.launch() {
            _vehicle.postValue(driverUseCase.getVehicle())
        }
    }

    fun requestToken() {
        viewModelScope.launch {
            _tokenRequestResult.postValue(loginUseCase.requestToken())
        }
    }

}