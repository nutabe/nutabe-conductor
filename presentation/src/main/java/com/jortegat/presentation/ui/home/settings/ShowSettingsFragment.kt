package com.jortegat.presentation.ui.home.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jortegat.base.helpers.showNoInternetMessage
import com.jortegat.presentation.databinding.FragmentShowSettingsBinding
import com.jortegat.presentation.ui.base.BaseDriverFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShowSettingsFragment : BaseDriverFragment() {

    private val binding: FragmentShowSettingsBinding get() = _binding!!
    private var _binding: FragmentShowSettingsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShowSettingsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            btTerms.setOnClickListener {
                if (internetManager.isInternetOn()) {
                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse("https://static.nutabe.com/static/terms-driver.html")
                    startActivity(openURL)
                } else {
                    showNoInternetMessage(requireView())
                }

            }

            btPrivacy.setOnClickListener {
                if (internetManager.isInternetOn()) {
                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse("https://nutabe.nyc3.digitaloceanspaces.com/static/privacy_policies.html")
                    startActivity(openURL)
                } else {
                    showNoInternetMessage(requireView())
                }

            }
        }

    }

}
