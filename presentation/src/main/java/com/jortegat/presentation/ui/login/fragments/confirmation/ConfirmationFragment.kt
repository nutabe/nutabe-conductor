package com.jortegat.presentation.ui.login.fragments.confirmation

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.showMessage
import com.jortegat.base.helpers.showRegistrationDialogConfirmation
import com.jortegat.base.models.RegistrationCode
import com.jortegat.base.models.ui.RegistrationDialogConfirmation
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.FragmentConfirmationDialogBinding
import com.jortegat.presentation.ui.base.BaseDriverFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConfirmationFragment : BaseDriverFragment() {

    private val viewModel: ConfirmationViewModel by viewModels()

    private var user: String? = null
    private var password: String? = null
    private var isReestablishFlow = false

    private val binding: FragmentConfirmationDialogBinding get() = _binding!!
    private var _binding: FragmentConfirmationDialogBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            user = it.getString("user")
            password = it.getString("password")
            isReestablishFlow = it.getBoolean("isReestablishFlow", false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConfirmationDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
        observers()
    }

    private fun listeners() {
        binding.confirmCodeButton.setOnClickListener {
            val code = binding.codeEditText.text.toString()
            if (code.isNotEmpty()) {
                user?.let {
                    if (isReestablishFlow) {
                        password?.let { pass ->
                            viewModel.reestablishPassword(it, pass, code)
                        }
                    } else {
                        viewModel.confirm(RegistrationCode(it, code))
                    }
                }
            } else showMessage(getString(R.string.confirmation_code_is_required), requireView())
        }

        binding.sendCodeButton.setOnClickListener {
            user?.let {
                viewModel.requestCodeAgain(it)
            }
        }

        binding.fabGoBack.setOnClickListener {
            findNavController().navigate(R.id.action_confirmationFragment_to_loginFragment)
        }
    }

    private fun observers() {
        viewModel.registrationCodeLiveData.observe(
            viewLifecycleOwner,
            Observer(::openLoginScreen)
        )

        viewModel.reestablishPassLiveData.observe(
            viewLifecycleOwner,
            Observer(::openLoginScreen)
        )
    }

    private fun openLoginScreen(result: Result<Unit>) {
        when (result) {
            is Result.Success -> {
                viewModel.setUserLogged()

                val onConfirm: (DialogInterface, Int) -> Unit = { dialogInterface, _ ->
                    dialogInterface.dismiss()
                    findNavController().navigate(R.id.action_confirmationFragment_to_loginFragment)
                }
                context?.showRegistrationDialogConfirmation(
                    RegistrationDialogConfirmation(
                        positiveCallback = onConfirm
                    )
                )
            }
            is Result.Error -> showMessage(result.exception.message.toString(), requireView())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}