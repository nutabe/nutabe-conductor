package com.jortegat.presentation.ui.login.fragments.reestablish

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jortegat.base.helpers.*
import com.jortegat.base.ui.base.BaseFragment
import com.jortegat.base.ui.widgets.NutabeEditText
import com.jortegat.presentation.R
import com.jortegat.presentation.ui.base.BaseDriverFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ReestablishFragment : BaseDriverFragment() {

    private val viewModel: ReestablishViewModel by viewModels()

    //view
    private lateinit var editTextNewPass: NutabeEditText
    private lateinit var editTextResetUser: AppCompatEditText
    private lateinit var reestablishButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.reestablish_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        listeners()
        observers()
    }

    private fun initView() {
        editTextResetUser = requireActivity().findViewById(R.id.editTextResetUser)
        editTextNewPass = requireActivity().findViewById(R.id.editTextNewPass)
        reestablishButton = requireActivity().findViewById(R.id.buttonResetPassword)
    }

    private fun listeners() {
        reestablishButton.setOnClickListener(::requestRegistrationCode)
    }

    private fun observers() {
        viewModel.resetCodeLiveData.observe(viewLifecycleOwner, Observer(::validateResult))
    }

    private fun validateResult(result: Result<Unit>) {
        hideLoader()
        when (result) {
            is Result.Success -> {
                findNavController().navigate(
                    R.id.action_reestablishFragment_to_confirmationFragment,
                    Bundle().apply {
                        putString("user", editTextResetUser.text.toString())
                        putString("password", editTextNewPass.text.toString())
                        putBoolean("isReestablishFlow", true)
                    }
                )
            }
            is Result.Error -> {
                requireActivity().hideKeyboard()
                showMessage(result.exception.message.toString(), requireView())
            }
        }
    }

    private fun requestRegistrationCode(view: View) {
        if (validateFields()) {
            if (internetManager.isInternetOn()) {
                showLoader()
                viewModel.requestResetCode(editTextResetUser.text.toString())
            } else {
                showNoInternetMessage(requireView())
            }
        } else {
            showMessage(getString(R.string.all_fields_are_require_error), requireView())
        }
    }

    private fun validateFields(): Boolean {
        return editTextResetUser.text.toString().isNotEmpty() && editTextNewPass.text.toString()
            .isNotEmpty()
    }
}