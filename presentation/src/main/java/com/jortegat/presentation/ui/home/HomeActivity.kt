package com.jortegat.presentation.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.jortegat.base.helpers.FilePathManager
import com.jortegat.base.helpers.IS_FRONT
import com.jortegat.base.helpers.IS_LOADED
import com.jortegat.base.helpers.showMessage
import com.jortegat.base.ui.alert_dialog.SupportDialogManager
import com.jortegat.domain.helper.*
import com.jortegat.presentation.R
import com.jortegat.presentation.databinding.ActivityHomeBinding
import com.jortegat.presentation.ui.base.BaseDriverActivity
import com.jortegat.presentation.ui.home.create_on_demand_services.OnPdfUploadFragmentListener
import com.jortegat.presentation.ui.register_driver.OnDialogFragmentListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.app_bar_main.*

@AndroidEntryPoint
class HomeActivity : BaseDriverActivity(), OnDialogFragmentListener, OnPdfUploadFragmentListener,
    OnNotificationActionListener {

    private val viewModel: HomeActivityViewModel by viewModels()

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    private var isFront = false
    private var isLoaded = false

    private lateinit var resultOriginLauncher: ActivityResultLauncher<Intent>
    private lateinit var resultDestinationLauncher: ActivityResultLauncher<Intent>
    private lateinit var resultSupportLauncher: ActivityResultLauncher<Intent>
    private lateinit var resultBackFileLauncher: ActivityResultLauncher<Intent>
    private lateinit var resultFrontFileLauncher: ActivityResultLauncher<Intent>

    private lateinit var binding: ActivityHomeBinding

    private lateinit var menu: Menu

    private val pickerFileIntent =
        Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
                .setType("image/*|application/pdf")
                .putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*", "application/pdf"))

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(toolbar)

        navController = findNavController(R.id.nav_host_fragment)

        binding.setupNavController(
            R.menu.activity_main_driver_drawer,
            R.layout.driver_nav_header_main,
        )

        initVariables()
        handlePushNotifications(intent)

    }

    private fun handlePushNotifications(intent: Intent?) {
        val bundle = Bundle()
        val extras = intent?.extras
        val title = extras?.getString("title")
        val body = extras?.getString("body")
        val notificationId = extras?.getString("notificationId")
        bundle.putString("body", body)
        bundle.putString("title", title)
        bundle.putString("notificationId", notificationId)

        val isVehicleInvitation = extras?.getBoolean("vehicleInvitation", false)
        isVehicleInvitation?.let {
            bundle.putBoolean("vehicleInvitation", it)
            if (it) {
                val vehicleInvitationId = extras.getString("vehicleInvitationId")
                bundle.putString("vehicleInvitationId", vehicleInvitationId)
            }
        }
        title?.let {
            findNavController(R.id.nav_host_fragment)
            navController.navigate(R.id.action_nav_home_to_notificationsFragment, bundle)
        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        handlePushNotifications(intent)
    }

    private fun initVariables() {
        resultOriginLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            it.data?.let { info ->
                val uri = info.data
                val filePath = FilePathManager.getRealPathFromURI(this, uri!!)

                isLoaded = if (fileFormatIsValid(filePath)) {
                    viewModel.saveOnDemandOriginFilePath(ON_DEMAND_ORIGIN_FILE_PATH, filePath)
                    showMessage(
                        getString(R.string.on_demand_origin_file_saved_success),
                        window.decorView.rootView
                    )
                    true
                } else {
                    showMessage(
                        getString(R.string.format_not_supported),
                        window.decorView.rootView
                    )
                    false
                }
                Intent(event).apply {
                    putExtra(ON_DEMAND_FILE_TYPE, ORIGIN_FILE)
                    putExtra(IS_LOADED, isLoaded)
                    sendBroadcast(this)
                }
            }
        }

        resultDestinationLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            it.data?.let { info ->
                val uri = info.data
                val filePath = FilePathManager.getRealPathFromURI(this, uri!!)
                isLoaded = if (fileFormatIsValid(filePath)) {
                    viewModel.saveOnDemandDestinationFilePath(
                        ON_DEMAND_DESTINATION_FILE_PATH,
                        filePath
                    )
                    showMessage(
                        getString(R.string.on_demand_destination_file_saved_success),
                        window.decorView.rootView
                    )
                    true
                } else {
                    showMessage(
                        getString(R.string.format_not_supported),
                        window.decorView.rootView
                    )
                    false
                }
                Intent(event).apply {
                    putExtra(ON_DEMAND_FILE_TYPE, DESTINATION_FILE)
                    putExtra(IS_LOADED, isLoaded)
                    sendBroadcast(this)
                }
            }
        }

        resultSupportLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            it.data?.let { info ->
                val uri = info.data
                val filePath = FilePathManager.getRealPathFromURI(this, uri!!)

                isLoaded = if (fileFormatIsValid(filePath)) {
                    viewModel.saveOnDemandSupportFilePath(ON_DEMAND_SUPPORT_FILE_PATH, filePath)
                    showMessage(
                        getString(R.string.on_demand_support_file_saved_success),
                        window.decorView.rootView
                    )
                    true
                } else {
                    showMessage(
                        getString(R.string.format_not_supported),
                        window.decorView.rootView
                    )
                    false
                }
                Intent(event).apply {
                    putExtra(ON_DEMAND_FILE_TYPE, SUPPORT_FILE)
                    putExtra(IS_LOADED, isLoaded)
                    sendBroadcast(this)
                }
            }
        }

        resultFrontFileLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            it.data?.let { info ->
                val uri = info.data
                val filePath = FilePathManager.getRealPathFromURI(this, uri!!)
                isLoaded = if (fileFormatIsValid(filePath)) {
                    viewModel.saveFrontalPhotoPath(FRONTAL_LICENSE_PHOTO_PATH, filePath)
                    showMessage(
                        getString(R.string.frontal_image_saved_success),
                        window.decorView.rootView
                    )
                    true
                } else {
                    showMessage(
                        getString(R.string.format_not_supported),
                        window.decorView.rootView
                    )
                    false
                }
                Intent(event).apply {
                    putExtra(IS_FRONT, isFront)
                    putExtra(IS_LOADED, isLoaded)
                    sendBroadcast(this)
                }
            }
        }

        resultBackFileLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            it.data?.let { info ->
                val uri = info.data
                val filePath = FilePathManager.getRealPathFromURI(this, uri!!)

                isLoaded = if (fileFormatIsValid(filePath)) {
                    viewModel.saveBackPhotoPath(BACK_LICENSE_PHOTO_PATH, filePath)
                    showMessage(
                        getString(R.string.back_image_saved_success),
                        window.decorView.rootView
                    )
                    true
                } else {
                    showMessage(
                        getString(R.string.format_not_supported),
                        window.decorView.rootView
                    )
                    false
                }
                Intent(event).apply {
                    putExtra(IS_FRONT, isFront)
                    putExtra(IS_LOADED, isLoaded)
                    sendBroadcast(this)
                }
            }
        }
    }

    private fun ActivityHomeBinding.setupNavController(@MenuRes menu: Int, @LayoutRes header: Int) {
        appBarConfiguration = AppBarConfiguration(DRIVER_OPTIONS, drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.inflateMenu(menu)
        navView.inflateHeaderView(header)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.toolbar_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_support -> {
                val dialogManager = SupportDialogManager(this)
                val supportDialog = dialogManager.getSupportDialog()
                dialogManager.setOnClickListeners { supportDialog.dismiss() }
                supportDialog.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onUploadFrontalFile() {
        isFront = true
        resultFrontFileLauncher.launch(pickerFileIntent)
    }

    override fun onUploadBackFile() {
        isFront = false
        resultBackFileLauncher.launch(pickerFileIntent)
    }

    override fun onUploadOriginFile() {
        resultOriginLauncher.launch(pickerFileIntent)
    }

    override fun onUploadDestinationFile() {
        resultDestinationLauncher.launch(pickerFileIntent)
    }

    override fun onUploadSupportFile() {
        resultSupportLauncher.launch(pickerFileIntent)
    }

    override fun onNotificationAction(hasPendingNotification: Boolean) {
        if (::menu.isInitialized) {
            val notificationItem = menu.findItem(R.id.action_notifications)
            notificationItem.setActionView(if (hasPendingNotification) R.layout.ic_notification_with_dot else R.layout.ic_notification_without_dot)
            notificationItem.actionView?.setOnClickListener {
                if (navController.currentDestination?.id != R.id.notificationsFragment) {
                    navController.navigate(R.id.notificationsFragment)
                }
            }
        }
    }

    companion object {
        const val event = "com.jortegat.presentation.ui.home.FILE_LOADED"
        private val DRIVER_OPTIONS = setOf(
            R.id.nav_home,
            R.id.driverCurriculumVitaeFragment,
            R.id.showServicesFragment,
            R.id.showOnDemandServiceFragment,
            R.id.notificationsFragment
        )
    }
}

interface OnNotificationActionListener {
    fun onNotificationAction(hasPendingNotification: Boolean)
}