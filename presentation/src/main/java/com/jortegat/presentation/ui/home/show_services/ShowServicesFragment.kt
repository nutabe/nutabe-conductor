package com.jortegat.presentation.ui.home.show_services

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.doIfError
import com.jortegat.base.helpers.doIfSuccess
import com.jortegat.base.helpers.showMessage
import com.jortegat.base.models.domain.Vehicle
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.presentation.databinding.FragmentShowServiceTabBinding
import com.jortegat.presentation.ui.home.StartNavigationFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShowServicesFragment : StartNavigationFragment() {

    private var vehicleId: String = ""
    private val viewModel: ShowServicesViewModel by viewModels()
    private lateinit var adapter: ViewPagerAdapter

    private var _binding: FragmentShowServiceTabBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShowServiceTabBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observers()
        requestToken()
    }

    private fun observers() {
        viewModel.vehicle.observe(viewLifecycleOwner, ::onVehicleResult)
        viewModel.journeyListResult.observe(viewLifecycleOwner, ::showJourneyListResult)
        viewModel.tokenRequestResult.observe(viewLifecycleOwner, ::onTokenResponse)
        internetManager.observe(viewLifecycleOwner, ::onInternetChange)
    }

    private fun requestToken() {
        if (internetManager.isInternetOn()) {
            showLoader("Estamos cargando tus viajes")
            binding.startShimmerAnimation()
            viewModel.requestToken()
        } else {
            showNoInternetContainer()
        }
    }

    private fun showNoInternetContainer() {
        binding.stopShimmerAnimation()
        binding.noInternetLayout.isVisible = true
        binding.viewPager.isVisible = false
        hideOrShowNoServicesContainer(false)
    }

    private fun hideNoInternetContainer(){
        binding.noInternetLayout.isVisible = false
    }

    private fun onInternetChange(hasInternet: Boolean?) {
        if (hasInternet == true) {
            /*try to get the notifications only if they are not visible already*/
            if (binding.noInternetLayout.isVisible) {
                showLoader("Estamos cargando tus viajes")
                hideNoInternetContainer()
                binding.startShimmerAnimation()
                viewModel.requestToken()
            }
        }
    }

    private fun onVehicleResult(vehicle: Vehicle?) {
        if (vehicle != null) {
            vehicleId = vehicle.id
            viewModel.getListOfJourneys(vehicle.id)
        } else {
            hideLoader()
            binding.stopShimmerAnimation()
            showErrorContainer()
        }
    }

    private fun onTokenResponse(result: Result<Unit>) {
        with(result) {
            doIfSuccess {
                viewModel.getVehicle()

            }
            doIfError {
                //even if it fails to get the token, we can try to get the local journeys in DB
                viewModel.getVehicle()
            }
        }
    }

    private fun FragmentShowServiceTabBinding.startShimmerAnimation() {
        shimmerLayout.startShimmer()
        shimmerLayout.visibility = View.VISIBLE
    }

    private fun FragmentShowServiceTabBinding.stopShimmerAnimation() {
        shimmerLayout.stopShimmer()
        shimmerLayout.visibility = View.GONE
        viewPager.visibility = View.VISIBLE

    }

    private fun showJourneyListResult(result: List<Journey>) {
        binding.stopShimmerAnimation()
        hideLoader()
        hideNoInternetContainer()
        if (result.isNotEmpty()) {
            hideOrShowNoServicesContainer(false)
            initView(result.toMutableList())
        } else {
            hideOrShowNoServicesContainer(true)
        }
    }

    private fun initView(journeys: MutableList<Journey>) {
        binding.viewPager.isVisible = true
        adapter = ViewPagerAdapter(this, journeys, vehicleId)
        binding.viewPager.adapter = adapter

        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = ACTIVE_SERVICE_TAB
                else -> tab.text = FINISHED_SERVICE_TAB
            }
        }.attach()
    }

    private fun hideOrShowNoServicesContainer(show: Boolean) {
        binding.noServicesAssignedContainer.isVisible = show
    }

    private fun showErrorContainer() {
        binding.stopShimmerAnimation()
        binding.noServicesAssignedContainer.isVisible = false
        binding.viewPager.isVisible = false
        binding.noInternetLayout.isVisible = false
        binding.layoutError.isVisible = true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val ACTIVE_SERVICE_TAB = "Activos"
        const val FINISHED_SERVICE_TAB = "Finalizados"
    }
}