package com.jortegat.presentation.ui.home.show_services

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.jortegat.base.helpers.Result
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.usecase.DriverUseCase
import com.jortegat.domain.login.usecase.LoginUseCase
import kotlinx.coroutines.launch

class ShowFinishedServicesViewModel @ViewModelInject constructor(
    private val driverUseCase: DriverUseCase,
    private val loginUseCase: LoginUseCase
) : ShowServicesViewModel(driverUseCase, loginUseCase) {

    private val _finishedJourneyListResult = MediatorLiveData<Result<List<Journey>>>()
    val finishedJourneyListResult: LiveData<Result<List<Journey>>> get() = _finishedJourneyListResult

    fun getListOfFinishedJourneys(vehicleId: String) {
        viewModelScope.launch {
            _finishedJourneyListResult.postValue(driverUseCase.getListOfFinishedJourneys(vehicleId))
        }
    }


}