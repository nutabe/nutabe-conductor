package com.jortegat.data.db.dao

import androidx.room.*
import com.jortegat.data.db.entity.on_demand_services.DbDriverBuildingSite
import com.jortegat.data.db.entity.on_demand_services.DbDriverFrontLoad
import com.jortegat.data.db.entity.on_demand_services.relations.BuildingSiteWithFrontLoads

@Dao
interface BuildingSiteDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBuildingSite(buildingSite: DbDriverBuildingSite)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFrontLoad(frontLoad: DbDriverFrontLoad)

    @Transaction
    @Query("SELECT * FROM buildingSite")
    suspend fun getAllBuildingSites(): List<BuildingSiteWithFrontLoads>

    @Transaction
    @Query("SELECT * FROM buildingSite WHERE buildingSiteId = :id")
    suspend fun getBuildingSiteById(id: Int): BuildingSiteWithFrontLoads

    @Query("SELECT name FROM buildingSite WHERE buildingSiteId = :id")
    suspend fun getBuildingSiteNameById(id: Int): String
}