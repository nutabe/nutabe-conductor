package com.jortegat.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class DbUser(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var name: String,
    var lastName: String,
    var identification: Long,
    var phone: Long,
    var role: String?
)