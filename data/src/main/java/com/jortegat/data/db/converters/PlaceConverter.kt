package com.jortegat.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jortegat.data.db.entity.journey.DbPlace

class PlaceConverter {
    @TypeConverter
    fun fromStringToDbPlace(data: String?): DbPlace? {
        if (data == null || data.isEmpty()) {
            return null
        }
        val type = object : TypeToken<DbPlace>() {}.type
        return Gson().fromJson(data, type)
    }

    @TypeConverter
    fun fromDbPlaceToString(myObjects: DbPlace): String {
        return Gson().toJson(myObjects)
    }
}
