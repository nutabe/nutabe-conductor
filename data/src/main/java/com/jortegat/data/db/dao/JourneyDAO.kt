package com.jortegat.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jortegat.data.db.entity.journey.DbJourney

@Dao
interface JourneyDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertJourney(journey: DbJourney)

    @Query("UPDATE journey SET journeyStatus = :status WHERE journeyId = :id")
    suspend fun updateJourneyStatus(id: String, status : String)

    @Query("UPDATE journey SET driverStatus = :status WHERE journeyId = :id")
    suspend fun updateJourneyDriverStatus(id: String, status : String)

    @Query("DELETE FROM journey WHERE journeyId = :id")
    suspend fun deleteJourney(id: String)

    @Transaction
    @Query("SELECT * FROM journey")
    fun getAllJourneys(): LiveData<List<DbJourney>?>

    @Transaction
    @Query("SELECT * FROM journey WHERE journeyId = :id")
    suspend fun getJourney(id: String): DbJourney?

    @Query("DELETE FROM journey")
    suspend fun clearAll()
}