package com.jortegat.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jortegat.data.db.entity.material.DbMaterial

@Dao
interface MaterialDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMaterial(material: DbMaterial)

    @Query("SELECT * FROM material")
    suspend fun getAllMaterials(): List<DbMaterial>

    @Query("SELECT name FROM material WHERE id = :id")
    suspend fun getMaterialNameById(id: Int): String
}