package com.jortegat.data.db.entity.vehicle

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "locations")
data class DbVehicleLocation (
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var vehicleId: String,
    val latitude: String,
    val longitude: String,
    val journeyId: String
)