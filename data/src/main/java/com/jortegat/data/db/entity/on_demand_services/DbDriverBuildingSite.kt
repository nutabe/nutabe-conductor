package com.jortegat.data.db.entity.on_demand_services

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "buildingSite")
data class DbDriverBuildingSite(
    @PrimaryKey(autoGenerate = false)
    var buildingSiteId: Int,
    val name: String
)