package com.jortegat.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jortegat.data.db.entity.journey.DbJourney
import com.jortegat.data.db.entity.vehicle.DbVehicleLocation

@Dao
interface LocationsDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(location: DbVehicleLocation): Long

    @Query("DELETE FROM locations WHERE id = :id")
    suspend fun deleteLocation(id: Int): Int

    @Transaction
    @Query("SELECT * FROM locations")
    suspend fun getPendingLocations(): List<DbVehicleLocation>

    @Query("DELETE FROM locations")
    suspend fun clearAll()
}