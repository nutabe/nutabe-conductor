package com.jortegat.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jortegat.data.db.entity.DbUser

@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: DbUser): Long

    @Query("SELECT * FROM user WHERE id = :userId")
    suspend fun getLoggedUser(userId: Int): List<DbUser>

    @Query("SELECT * FROM user")
    suspend fun findAll(): List<DbUser>
}