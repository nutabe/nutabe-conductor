package com.jortegat.data.db.dao

import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jortegat.base.models.domain.Vehicle
import com.jortegat.data.db.entity.vehicle.DbVehicle

@Dao
interface VehicleDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveVehicle(vehicle: DbVehicle)

    @Query("SELECT * FROM vehicle")
    suspend fun getVehicle(): List<DbVehicle>
}