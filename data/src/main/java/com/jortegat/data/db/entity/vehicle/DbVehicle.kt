package com.jortegat.data.db.entity.vehicle

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vehicle")
data class DbVehicle(
    @PrimaryKey(autoGenerate = false)
    var id: String,
    var type: String,
    var volumen: String? = null,
    var soatEndDate: String? = null,
    var technomechanicalEndDate: String? = null,
    var policyEndDate: String? = null,
    var civilLiabilityEndDate: String? = null,
    var isVerified: Boolean,
    var isActive: Boolean,
    var createdAt: String
)