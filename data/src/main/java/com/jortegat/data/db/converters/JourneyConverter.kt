package com.jortegat.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.jortegat.data.db.entity.journey.DbJourney

class JourneyConverter {
    @TypeConverter
    fun fromStringToDbJourney(data: String?): DbJourney? {
        if (data == null || data.isEmpty()) {
            return null
        }
        return Gson().fromJson(data, DbJourney::class.java)
    }

    @TypeConverter
    fun fromDbJourneyToString(myObject: DbJourney): String {
        return Gson().toJson(myObject)
    }

    @TypeConverter
    fun fromDbJourneyListToString(value: List<DbJourney>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun fromStringToDbJourneyList(value: String): List<DbJourney> {
        return Gson().fromJson(value, Array<DbJourney>::class.java).toList()
    }
}
