package com.jortegat.data.db.entity.on_demand_services

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "frontLoad")
data class DbDriverFrontLoad(
    @PrimaryKey(autoGenerate = false)
    var frontLoadId: Int,
    val buildingSiteId: Int,
    val assistant: String,
    val createdAt: String,
    val isActive: Boolean,
    val isDump: Boolean,
    val latitude: Double,
    val location: String,
    val longitude: Double
)