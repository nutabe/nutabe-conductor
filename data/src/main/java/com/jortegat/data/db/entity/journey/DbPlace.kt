package com.jortegat.data.db.entity.journey

data class DbPlace(
    val identification: String,
    var address: String,
    var businessName: String,
    var latitude: Double,
    var longitude: Double,
    var isQuarry: Boolean,
    var isDump: Boolean,
    var isActive: Boolean,
    var type: String,
    var createdAt: String
)