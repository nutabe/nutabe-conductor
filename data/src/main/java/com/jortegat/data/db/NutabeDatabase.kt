package com.jortegat.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jortegat.data.db.converters.MaterialConverter
import com.jortegat.data.db.converters.PlaceConverter
import com.jortegat.data.db.dao.*
import com.jortegat.data.db.entity.DbUser
import com.jortegat.data.db.entity.journey.DbJourney
import com.jortegat.data.db.entity.material.DbMaterial
import com.jortegat.data.db.entity.on_demand_services.DbDriverBuildingSite
import com.jortegat.data.db.entity.on_demand_services.DbDriverFrontLoad
import com.jortegat.data.db.entity.vehicle.DbVehicle
import com.jortegat.data.db.entity.vehicle.DbVehicleLocation

@Database(
    version = 1, exportSchema = false, entities = [
        DbUser::class,
        DbDriverFrontLoad::class,
        DbDriverBuildingSite::class,
        DbMaterial::class,
        DbVehicle::class,
        DbJourney::class,
        DbVehicleLocation::class
    ]
)

@TypeConverters(PlaceConverter::class, MaterialConverter::class)
abstract class NutabeDatabase : RoomDatabase() {

    abstract fun userDAO(): UserDAO
    abstract fun buildingSiteDAO(): BuildingSiteDAO
    abstract fun materialDAO(): MaterialDAO
    abstract fun vehicleDAO(): VehicleDAO
    abstract fun journeyDAO(): JourneyDAO
    abstract fun locationsDAO(): LocationsDAO

    companion object {
        @Volatile
        private var INSTANCE: NutabeDatabase? = null

        fun getInstance(appContext: Context): NutabeDatabase {
            synchronized(this) {
                return INSTANCE ?: Room.databaseBuilder(
                    appContext,
                    NutabeDatabase::class.java,
                    "nutabe_db"
                ).build().also { INSTANCE = it }
            }
        }
    }
}