package com.jortegat.data.db.entity.journey

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jortegat.base.models.domain.OrderGuide
import com.jortegat.data.db.entity.material.DbMaterial

@Entity(tableName = "journey")
data class DbJourney(
    @PrimaryKey(autoGenerate = false)
    val journeyId: Int,
    val serviceCreationDate: String,
    val serviceOrder: String,
    val vehicleId: String,
    val vehicleType: String,
    val serviceType: String,
    val company: String,
    var destination: DbPlace,
    var origin: DbPlace,
    var material: DbMaterial,
    val workdayId: Int,
    var workdayName: String,
    val ownerId: String,
    var ownerName: String,
    var journeyStatus: String,
    var driverStatus: String,
    val date: String,
    var receiptName: String?,
    var receiptNumber: String?,
    var receiptDate: String?,
    var remissionDispatcher: String?,
    var remissionNumber: String?,
    var remissionDate: String?,
    var remissionLoad: Long?,
    var remissionLoadType: String,
    var hasVisitedOrigin: Boolean = false,
    var hasVisitedDestination: Boolean = false,
    val serial: Int
)