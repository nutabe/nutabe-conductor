package com.jortegat.data.db.entity.material

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "material")
data class DbMaterial(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val name: String,
    val type: String
)