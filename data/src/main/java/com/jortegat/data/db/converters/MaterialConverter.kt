package com.jortegat.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jortegat.data.db.entity.material.DbMaterial

class MaterialConverter {
    @TypeConverter
    fun fromStringToDbMaterial(data: String?): DbMaterial? {
        if (data == null || data.isEmpty()) {
            return null
        }
        val type = object : TypeToken<DbMaterial>() {}.type
        return Gson().fromJson(data, type)
    }

    @TypeConverter
    fun fromDbMaterialToString(myObjects: DbMaterial): String {
        return Gson().toJson(myObjects)
    }
}
