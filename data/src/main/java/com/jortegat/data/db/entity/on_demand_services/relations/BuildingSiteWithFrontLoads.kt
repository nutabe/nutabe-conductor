package com.jortegat.data.db.entity.on_demand_services.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.jortegat.data.db.entity.on_demand_services.DbDriverBuildingSite
import com.jortegat.data.db.entity.on_demand_services.DbDriverFrontLoad

data class BuildingSiteWithFrontLoads(
    @Embedded val buildingSite: DbDriverBuildingSite,
    @Relation(
        parentColumn = "buildingSiteId",
        entityColumn = "buildingSiteId"
    )
    val frontLoadList: List<DbDriverFrontLoad>
)