package com.jortegat.data.login.datasource.remote

import com.jortegat.base.models.*
import com.jortegat.data.login.datasource.remote.model.ApiIdentification
import retrofit2.Response
import retrofit2.http.*

interface LoginApi {

    @POST
    suspend fun login(
        @Url url: String,
        @Body user: UserPass
    ): Response<ApiLoginResponse>

    @POST
    suspend fun register(
        @Url url: String,
        @Body registration: UserRegistrationForm
    ): Response<RegistrationApiResponse>

    @POST
    suspend fun confirm(
        @Url url: String,
        @Body registrationCode: RegistrationCode
    ): Response<RegistrationApiResponse>

    @POST
    suspend fun requestResetCode(
        @Url url: String,
        @Query("identification") identification: String
    ): Response<RegistrationApiResponse>

    @POST
    suspend fun requestCodeAgain(
        @Url url: String,
        @Body identification: ApiIdentification
    ): Response<RegistrationApiResponse>

    @PUT
    suspend fun reestablishPass(
        @Url url: String,
        @Query("token") token: String,
        @Body user: UserPass
    ): Response<RegistrationApiResponse>

    @GET
    suspend fun getRole(
        @Url url: String
    ): Response<ApiGetRoleResponse>

    @POST
    suspend fun sendRoles(
        @Url url: String,
        @Body types: ApiRoleTypes
    ): Response<RegistrationApiResponse>

    @POST
    suspend fun requestNewToken(
        @Url url: String
    ): Response<ApiLoginResponse>
}