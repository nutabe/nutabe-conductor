package com.jortegat.data.login.datasource.local

import com.jortegat.base.helpers.Result
import com.jortegat.data.db.dao.UserDAO
import com.jortegat.data.db.entity.DbUser
import com.jortegat.domain.helper.PreferencesManager

class LoginLocalDataSourceImpl(
    private val preferencesManager: PreferencesManager,
    private val userDAO: UserDAO,
) : LoginLocalDataSource {


    override suspend fun save(dbUser: DbUser): Result<Unit> {
        val user = userDAO.insertUser(dbUser)
        return if (user != 0L) {
            Result.Success(Unit)
        } else {
            Result.Error(Exception("Error saving the logged user"))
        }
    }

    override suspend fun getLoggedUser(): Result<DbUser> {
        val user = 1 //as we only have one register per device
        val dbUser = userDAO.getLoggedUser(user)
        return Result.Success(dbUser.first())
    }

    override fun setUserLogged() {
        preferencesManager.setUserLogged()
    }

    override fun isUserLogged(): Boolean {
        return preferencesManager.isUserLogged()
    }
}