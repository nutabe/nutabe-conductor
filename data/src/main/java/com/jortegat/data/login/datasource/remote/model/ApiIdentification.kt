package com.jortegat.data.login.datasource.remote.model

data class ApiIdentification(val identification: String)