package com.jortegat.data.login.datasource.remote

import android.os.Bundle
import android.util.Log
import com.auth0.android.jwt.JWT
import com.jortegat.base.helpers.ACCESS_TOKEN
import com.jortegat.base.helpers.REFRESH_TOKEN
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.RoleTypes
import com.jortegat.base.models.*
import com.jortegat.data.login.datasource.remote.model.ApiIdentification
import com.jortegat.domain.helper.PreferencesManager
import java.io.IOException

class LoginRemoteDataSourceImpl(
    private val loginApi: LoginApi,
    private val preferencesManager: PreferencesManager
) : LoginRemoteDataSource {

    override suspend fun login(user: String, pass: String): Result<MutableList<String>> {
        var userProperties: MutableList<String> = mutableListOf()
        return try {
            val url = "https://api.nutabe.com/auth/account/login"
            val response = loginApi.login(url, UserPass(user, pass))
            if (response.isSuccessful) {
                val apiLoginResponse = response.body()
                apiLoginResponse?.let {
                    userProperties = saveCredentials(it)
                }

                Result.Success(userProperties)
            } else {
                when (response.code()) {
                    400 -> Result.Error(Exception("Usuario o contraseña incorrecta"))
                    403 -> Result.Error(Exception("Por favor verifica tu cuenta"))
                    404 -> Result.Error(Exception("Usuario o contraseña incorrecta"))
                    else -> Result.Error(Exception("Intenta de nuevo por favor."))
                }
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    private fun saveCredentials(apiLoginResponse: ApiLoginResponse): MutableList<String> {
        //https://github.com/auth0/JWTDecode.Android
        val jwt = JWT(apiLoginResponse.data.accessToken)
        val firstName = jwt.claims["firstName"]?.asString()
        val lastName = jwt.claims["lastName"]?.asString()
        val identification = jwt.claims["sub"]?.asString()
        val phone = jwt.claims["phone"]?.asString()
        val bundle = Bundle()

        bundle.putString(ACCESS_TOKEN, apiLoginResponse.data.accessToken)
        bundle.putString(REFRESH_TOKEN, apiLoginResponse.data.refreshToken)

        preferencesManager.saveCredentials(bundle)

        return mutableListOf(firstName!!, lastName!!, identification!!, phone!!)
    }

    override suspend fun register(registration: UserRegistrationForm): Result<Unit> {
        return try {
            val response =
                loginApi.register("https://api.nutabe.com/auth/account/register", registration)
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                if (response.code() == USER_EXISTS_ERROR) {
                    Result.Error(Exception("Número  de celular o cedula en uso."))
                } else {
                    Result.Error(Exception("Se produjó un fallo en el registro."))
                }
            }
        } catch (e: Exception) {
            Result.Error(Exception("Error de servidor"))
        }
    }

    override suspend fun confirm(registrationCode: RegistrationCode): Result<Unit> {
        return try {
            val response =
                loginApi.confirm(
                    "https://api.nutabe.com/auth/account/phone/confirm",
                    registrationCode
                )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                Result.Error(Exception("El código ingresaso es inválido"))
            }
        } catch (e: IOException) {
            Result.Error(Exception("Error construyendo la petición: confirm"))
        }
    }

    override suspend fun requestResetCode(identification: String): Result<Unit> {
        return try {
            val response =
                loginApi.requestResetCode(
                    "https://api.nutabe.com/auth/account/password/reset",
                    identification
                )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                Result.Error(Exception("Se produjó un fallo solicitando el codigo para reestablecer la cuenta"))
            }
        } catch (e: IOException) {
            Result.Error(Exception("Los datos ingresados no son válidos."))
        }
    }

    override suspend fun requestCodeAgain(identification: String) {
        try {
            loginApi.requestCodeAgain(
                "https://api.nutabe.com/auth/account/phone/token/resend",
                ApiIdentification(identification)
            )
        } catch (e: IOException) {
        }
    }

    override suspend fun reestablishPassword(
        user: String,
        pass: String,
        code: String
    ): Result<Unit> {
        return try {
            val response =
                loginApi.reestablishPass(
                    "https://api.nutabe.com/auth/account/password/reset",
                    code,
                    UserPass(user, pass)
                )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                Result.Error(IOException())
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    override suspend fun getRole(): Result<List<String>> {
        return try {
            val response =
                loginApi.getRole("https://api.nutabe.com/auth/account/type")
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                when (response.code()) {
                    404 -> Result.Error(Exception("Usuario no encontrado"))
                    else -> Result.Error(Exception("Usuario sin validar"))
                }
            }
        } catch (e: IOException) {
            Result.Error(Exception("Error construyendo la petición: getRole"))
        }
    }

    override suspend fun sendRole(): Result<Unit> {
        return try {
            val response =
                loginApi.sendRoles(
                    "https://api.nutabe.com/auth/account/type",
                    ApiRoleTypes(arrayOf(RoleTypes.Driver.type))
                )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                Result.Error(Exception("Se produjó un fallo enviando el rol"))
            }
        } catch (e: IOException) {
            Result.Error(Exception("Error construyendo la petición: sendRole"))
        }
    }

    override suspend fun requestToken(): Result<Unit> {
        return try {
            val response =
                loginApi.requestNewToken("https://api.nutabe.com/auth/account/refresh/token")
            if (response.isSuccessful) {
                val apiResponse = response.body()!!
                saveCredentials(apiResponse)
                Result.Success(Unit)
            } else {
                Result.Error(Exception("Se produjó un error autorizando la sesión."))
            }
        } catch (e: IOException) {
            Result.Error(Exception("Se produjó un error autorizando la sesión."))
        }
    }

    override suspend fun requestPeriodicToken() {
        try {
            val response =
                loginApi.requestNewToken("https://api.nutabe.com/auth/account/refresh/token")
            if (response.isSuccessful) {
                val apiResponse = response.body()!!
                Log.d("workManagerRefreshToken", "token refreshed")
                saveCredentials(apiResponse)

            }
        } catch (e: IOException) {
        }
    }

    companion object {
        const val USER_EXISTS_ERROR = 409
    }

}