package com.jortegat.data.login.datasource.remote

import com.jortegat.base.helpers.Result
import com.jortegat.base.models.RegistrationCode
import com.jortegat.base.models.UserRegistrationForm
import com.jortegat.data.db.entity.DbUser

interface LoginRemoteDataSource {

    suspend fun login(user: String, pass: String): Result<MutableList<String>>

    suspend fun register(registration: UserRegistrationForm): Result<Unit>

    suspend fun confirm(registrationCode: RegistrationCode): Result<Unit>

    suspend fun requestResetCode(identification: String): Result<Unit>

    suspend fun requestCodeAgain(identification: String)

    suspend fun reestablishPassword(user: String, pass: String, code: String): Result<Unit>

    suspend fun getRole(): Result<List<String>>

    suspend fun sendRole(): Result<Unit>

    suspend fun requestToken(): Result<Unit>

    suspend fun requestPeriodicToken()
}