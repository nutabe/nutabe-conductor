package com.jortegat.data.login.repository

import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.RoleTypes
import com.jortegat.base.models.RegistrationCode
import com.jortegat.base.models.User
import com.jortegat.base.models.UserRegistrationForm
import com.jortegat.data.db.entity.DbUser
import com.jortegat.data.login.datasource.local.LoginLocalDataSource
import com.jortegat.data.login.datasource.remote.LoginRemoteDataSource
import com.jortegat.data.mappers.UserMappers
import com.jortegat.domain.login.repository.LoginRepository
import java.io.IOException

class LoginRepositoryImpl(
    private val loginRemoteDataSource: LoginRemoteDataSource,
    private val loginLocalDataSource: LoginLocalDataSource
) :
    LoginRepository {

    override suspend fun login(user: String, pass: String): Result<Unit> {

        return when (val result = loginRemoteDataSource.login(user, pass)) {
            is Result.Success -> {
                result.data.run {
                    val dbUser = DbUser(
                        name = this[0],
                        lastName = this[1],
                        identification = this[2].toLong(),
                        phone = this[3].toLong(),
                        role = RoleTypes.Driver.type
                    )

                    loginLocalDataSource.save(dbUser)
                }
                Result.Success(Unit)
            }

            is Result.Error -> Result.Error(result.exception)
        }
    }

    override suspend fun register(registration: UserRegistrationForm): Result<Unit> {
        return loginRemoteDataSource.register(registration)
    }

    override suspend fun confirm(registrationCode: RegistrationCode): Result<Unit> {
        return loginRemoteDataSource.confirm(registrationCode)
    }

    override suspend fun requestResetCode(identification: String): Result<Unit> {
        return loginRemoteDataSource.requestResetCode(identification)
    }

    override suspend fun requestCodeAgain(identification: String) {
        return loginRemoteDataSource.requestCodeAgain(identification)
    }

    override suspend fun reestablishPassword(
        user: String,
        pass: String,
        code: String
    ): Result<Unit> {
        return loginRemoteDataSource.reestablishPassword(user, pass, code)
    }

    override suspend fun getLoggedUser(): Result<User> {
        return when (val result = loginLocalDataSource.getLoggedUser()) {
            is Result.Success -> {
                Result.Success(UserMappers.mapDbToDomain(result.data))
            }
            is Result.Error -> Result.Error(IOException("Se produjó un error, por favor intentalo mas tarde"))
        }
    }

    override suspend fun validateRole(): Result<Unit> {
        return when (val result = loginRemoteDataSource.getRole()) {
            is Result.Success -> {
                if (roleIsNotAssigned(result.data)) {
                    return sendRole()
                } else {
                    if (isDriverRole(result.data)) {
                        Result.Success(Unit)
                    } else {
                        Result.Error(Exception("Tu perfil no tiene acceso a esta aplicación"))
                    }
                }
            }
            is Result.Error -> Result.Error(IOException("Se produjó un error, por favor intentalo mas tarde"))
        }
    }

    private fun isDriverRole(roles: List<String>): Boolean {
        roles.forEach { role ->
            if (role == RoleTypes.Driver.type) return true
        }
        return false
    }

    private suspend fun sendRole(): Result<Unit> {
        return loginRemoteDataSource.sendRole()
    }

    private fun roleIsNotAssigned(roles: List<String>): Boolean {
        return roles.isEmpty()
    }

    override fun setUserLogged() {
        loginLocalDataSource.setUserLogged()
    }

    override fun isUserLogged(): Boolean {
        return loginLocalDataSource.isUserLogged()
    }

    override suspend fun requestToken(): Result<Unit> {
        return loginRemoteDataSource.requestToken()
    }

    override suspend fun requestPeriodicToken() {
        return loginRemoteDataSource.requestPeriodicToken()
    }
}