package com.jortegat.data.login.datasource.local

import com.jortegat.base.helpers.Result
import com.jortegat.data.db.entity.DbUser

interface LoginLocalDataSource {

    suspend fun save(dbUser: DbUser): Result<Unit>

    suspend fun getLoggedUser(): Result<DbUser>

    fun setUserLogged()

    fun isUserLogged(): Boolean
}
