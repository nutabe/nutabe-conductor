package com.jortegat.data.driver.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.jortegat.base.enums.JourneyStatus
import com.jortegat.base.helpers.InternetManager
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.doIfSuccess
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.*
import com.jortegat.base.models.on_demand_material.ApiOnDemandMaterial
import com.jortegat.base.models.on_demand_material.OnDemandMaterial
import com.jortegat.data.db.entity.journey.DbJourney
import com.jortegat.data.db.entity.vehicle.DbVehicle
import com.jortegat.data.db.entity.vehicle.DbVehicleLocation
import com.jortegat.data.driver.datasource.local.DriverLocalDataSource
import com.jortegat.data.driver.datasource.models.create_on_demand_service.ApiDriverBuildingSite
import com.jortegat.data.driver.datasource.models.service_response.ApiService
import com.jortegat.data.driver.datasource.remote.DriverRemoteDataSource
import com.jortegat.data.mappers.*
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverBuildingSite
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandService
import com.jortegat.domain.driver.repository.DriverRepository

class DriverRepositoryImpl(
    private val driverRemoteDataSource: DriverRemoteDataSource,
    private val driverLocalDataSource: DriverLocalDataSource,
    private val internetManager: InternetManager
) :
    DriverRepository {
    override suspend fun uploadDriverData(
        licenseCategory: String,
        frontPhotoPath: String,
        backPhotoPath: String
    ): Result<Unit> {
        return driverRemoteDataSource.uploadDriverData(
            licenseCategory,
            frontPhotoPath,
            backPhotoPath
        )
    }

    override fun saveFrontalPhotoPath(key: String, path: String?) {
        return driverLocalDataSource.saveFrontalPhotoPath(key, path)
    }

    override fun saveBackPhotoPath(key: String, path: String?) {
        return driverLocalDataSource.saveBackPhotoPath(key, path)
    }

    override fun saveOnDemandOriginFilePath(key: String, path: String?) {
        return driverLocalDataSource.saveOnDemandOriginFilePath(key, path)
    }

    override fun saveOnDemandDestinationFilePath(key: String, path: String?) {
        return driverLocalDataSource.saveOnDemandDestinationFilePath(key, path)
    }

    override fun saveOnDemandSupportFilePath(key: String, path: String?) {
        return driverLocalDataSource.saveOnDemandSupportFilePath(key, path)
    }

    override fun getFrontPhotoPath(): String {
        return driverLocalDataSource.getFrontPhotoPath()
    }

    override fun getBackPhotoPath(): String {
        return driverLocalDataSource.getBackPhotoPath()
    }

    override fun getOnDemandOriginFilePath(): String {
        return driverLocalDataSource.getOnDemandOriginFilePath()
    }

    override fun getOnDemandDestinationFilePath(): String {
        return driverLocalDataSource.getOnDemandDestinationFilePath()
    }

    override suspend fun getListOfJourneys(vehicleId: String): LiveData<List<Journey>> {
        if (internetManager.isInternetOn()) {
            return when (val result = driverRemoteDataSource.getListOfServices(vehicleId)) {
                is Result.Success -> {
                    val resultServiceList = result.data
                    //In progress journey doesn't come in the same request as the not started journeys
                    when (val inProgressServiceResult =
                        driverRemoteDataSource.getStartedJourney(vehicleId)) {
                        is Result.Success -> {
                            val serviceList: MutableList<ApiService> =
                                resultServiceList.toMutableList()

                            //not always there's an in progress journey so we need to validate null response
                            inProgressServiceResult.data?.let {
                                val apiService: ApiService? =
                                    ServiceMappers.mapInProgressJourneyToApiService(it)
                                apiService?.let { service ->
                                    serviceList.add(service)
                                }
                            }

                            val tempJourneyList =
                                ServiceMappers.mapApiServiceToDomainJourneys(serviceList)
                            val journeyList =
                                fetchMaterialAndOwnerAndDestination(tempJourneyList)

                            //if empty, then this vehicle has nothing in the back so locally we must clear it too
                            if (journeyList.isEmpty()) clearJourneyList() else {
                                //always first save in db, then grab from db
                                saveJourneyList(journeyList)
                            }

                            transformJourneysFromDb(driverLocalDataSource.getJourneys())
                        }
                        is Result.Error -> {
                            val tempJourneyList =
                                ServiceMappers.mapApiServiceToDomainJourneys(resultServiceList)
                            val journeyList =
                                fetchMaterialAndOwnerAndDestination(tempJourneyList)

                            //always first save in db, then grab from db
                            saveJourneyList(journeyList)
                            transformJourneysFromDb(driverLocalDataSource.getJourneys())
                        }
                    }
                }
                is Result.Error -> {
                    return transformJourneysFromDb(driverLocalDataSource.getJourneys())
                }
            }
        } else {
            return transformJourneysFromDb(driverLocalDataSource.getJourneys())
        }
    }

    override suspend fun getListOfFinishedJourneys(vehicleId: String): Result<List<Journey>> {
        return when (val result = driverRemoteDataSource.getListOfFinishedServices(vehicleId)) {
            is Result.Success -> {
                val resultServiceList = result.data
                val tempJourneyList =
                    ServiceMappers.mapApiServiceToDomainJourneys(resultServiceList)
                val journeyList =
                    fetchMaterialAndOwnerAndDestination(tempJourneyList)

                Result.Success(journeyList)
            }
            is Result.Error -> {
                return Result.Error(result.exception)
            }
        }
    }

    private suspend fun clearJourneyList() {
        driverLocalDataSource.clearJourneyList()
    }

    private fun transformJourneysFromDb(journeys: LiveData<List<DbJourney>?>): LiveData<List<Journey>> {
        return Transformations.map(journeys) { ServiceMappers.mapFromDb(it) }
    }

    private suspend fun saveJourneyList(journeyList: List<Journey>): Boolean {
        return driverLocalDataSource.saveJourneyList(ServiceMappers.mapToDb(journeyList))
    }

    private suspend fun getVehicleInfo(): Vehicle? {
        return if (internetManager.isInternetOn()) {
            val vehicle = driverRemoteDataSource.getMyVehicle()
            if (vehicle != null) saveVehicleInDb(VehicleMappers.mapToDb(vehicle))
            return vehicle
        } else {
            val localResult = driverLocalDataSource.getVehicle()
            if (localResult.isEmpty()) return null else VehicleMappers.mapFromDb(localResult.first())
        }
    }

    private suspend fun saveVehicleInDb(vehicle: DbVehicle) {
        driverLocalDataSource.saveVehicle(vehicle)
    }

    private suspend fun fetchMaterialAndOwnerAndDestination(journeyList: List<Journey>): List<Journey> {
        journeyList.forEach {
            driverRemoteDataSource.getMaterialById(it.material.id)
                ?.run { it.material.name = name }
            driverRemoteDataSource.getUserById(it.ownerId)?.run {
                it.ownerName = toString()
            }
            driverRemoteDataSource.getJourneyDestination(it.journeyId.toString()).run {
                doIfSuccess { destination ->
                    it.origin.address = destination!!.origin.address
                    it.origin.businessName = destination.origin.name ?: destination.origin.address
                    it.origin.latitude = destination.origin.latitude
                    it.origin.longitude = destination.origin.longitude
                    it.destination.address = destination.destination.address
                    it.destination.businessName =
                        destination.destination.name ?: destination.destination.address
                    it.destination.latitude = destination.destination.latitude
                    it.destination.longitude = destination.destination.longitude
                }
            }
        }

        return journeyList
    }

    override suspend fun acceptJourney(id: String): Result<Unit> {
        return when (val result = driverRemoteDataSource.acceptJourney(id)) {
            is Result.Success -> {
                updateJourneyDriverStatus(id, JourneyStatus.ACCEPTED.name)
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    override suspend fun rejectJourney(id: String): Result<Unit> {
        return when (val result = driverRemoteDataSource.rejectJourney(id)) {
            is Result.Success -> {
                deleteJourney(id)
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    override suspend fun startJourney(id: String): Result<Unit> {
        return when (val result = driverRemoteDataSource.startJourney(id)) {
            is Result.Success -> {
                updateJourneyStatus(id, JourneyStatus.STARTED.name)
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    override suspend fun resumeJourney(id: String): Result<Unit> {
        return when (val result = driverRemoteDataSource.resumeJourney(id)) {
            is Result.Success -> {
                updateJourneyStatus(id, JourneyStatus.PROCESS.name)
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    override suspend fun finishJourney(id: String): Result<Unit> {
        return when (val result = driverRemoteDataSource.finishJourney(id)) {
            is Result.Success -> {
                updateJourneyStatus(id, JourneyStatus.FINISHED.name)
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    private suspend fun updateJourneyStatus(id: String, status: String) {
        driverLocalDataSource.updateJourneyStatus(id, status)
    }

    private suspend fun updateJourneyDriverStatus(id: String, status: String) {
        driverLocalDataSource.updateJourneyDriverStatus(id, status)
    }

    private suspend fun deleteJourney(id: String) {
        driverLocalDataSource.deleteJourney(id)
    }

    override suspend fun sendLocation(
        journeyId: String,
        vehicleLocation: VehicleLocation
    ) {

        val vehicleId = driverRemoteDataSource.getMyVehicle()?.id

        vehicleId?.let {
            val location = DbVehicleLocation(
                vehicleId = vehicleId,
                latitude = vehicleLocation.latitude,
                longitude = vehicleLocation.longitude,
                journeyId = journeyId
            )
            driverLocalDataSource.saveLocation(location)
            sendPendingLocations()
            // return driverRemoteDataSource.sendLocation(journeyId, vehicleLocation)
        }
    }

    override suspend fun sendPendingLocations() {
        if (internetManager.isInternetOn()) {
            val locations = driverLocalDataSource.getPendingLocations()
            if (locations.isNotEmpty()) {
                locations.forEach {
                    val apiLocation = VehicleLocation(it.vehicleId, it.latitude, it.longitude)
                    when (driverRemoteDataSource.sendLocation(it.journeyId, apiLocation)) {
                        is Result.Success -> {
                            driverLocalDataSource.deleteLocation(it.id)
                        }
                        is Result.Error -> {
                            if (internetManager.isInternetOn()) {
                                driverLocalDataSource.deleteLocation(it.id)
                            }
                        }
                    }
                }

            }
        }
    }

    override suspend fun getJourneyDetail(id: String): Result<CustomJourney?> {
        return driverRemoteDataSource.getJourneyDetail(id)
    }

    override suspend fun getJourneyDestination(id: String): Result<JourneyDestination> {
        return driverRemoteDataSource.getJourneyDestination(id)
    }

    override suspend fun getStartedJourney(): Result<CustomJourney?> {
        val vehicle = driverRemoteDataSource.getMyVehicle()
        return if (vehicle != null) driverRemoteDataSource.getStartedJourney(vehicle.id)
        else Result.Error(Exception("Error identificando el vehículo"))
    }

    override suspend fun getDriverInformation(): Result<Driver> {
        return driverRemoteDataSource.getDriverInformation()
    }

    override suspend fun validateDocumentsAreFilled(): Result<Boolean> {
        return driverRemoteDataSource.validateDocumentsAreFilled()
    }

    override suspend fun getOnDemandServices(): Result<List<OnDemandService>> {
        return when (val result = driverRemoteDataSource.getOnDemandServices()) {
            is Result.Success -> {
                val onDemandServices =
                    OnDemandServiceMappers.mapApiOnDemandServiceListToDomain(result.data)
                //onDemandServices includes the ids but not the names.
                val servicesWithBuildingSiteNames =
                    getListWithNamesOfBuildingSiteById(onDemandServices)

                val servicesWithBuildingSiteAndMaterialNames =
                    getListWithNamesOfMaterialsById(servicesWithBuildingSiteNames)
                Result.Success(servicesWithBuildingSiteAndMaterialNames)
            }
            is Result.Error -> Result.Error(Exception("Tuvimos un problema cargando los materiales. Intente de nuevo mas tarde."))

        }
    }

    private suspend fun getListWithNamesOfBuildingSiteById(onDemandServiceList: List<OnDemandService>): List<OnDemandService> {
        val result = onDemandServiceList.toMutableList()

        //lets make sure that we have in db the buildingSites to get the name from.
        when (getBuildingSites()) {
            is Result.Success -> {
                result.forEach {
                    it.buildingSiteName =
                        driverLocalDataSource.getBuildingSiteNameById(it.buildingSiteId)
                }
            }
            is Result.Error -> {}
        }
        return result
    }

    private suspend fun getListWithNamesOfMaterialsById(onDemandServiceList: List<OnDemandService>): List<OnDemandService> {
        val result = onDemandServiceList.toMutableList()

        //lets make sure that we have in db the materials to get the name from.
        when (getMaterials()) {
            is Result.Success -> {
                result.forEach {
                    it.materialName = driverLocalDataSource.getMaterialNameById(it.materialId)
                }
            }
            is Result.Error -> {}
        }
        return result
    }

    override suspend fun getBuildingSites(): Result<List<DriverBuildingSite>> {
        //first check the local db
        val localResult = driverLocalDataSource.getBuildingSites()
        if (localResult.isEmpty()) {
            val remoteResult = driverRemoteDataSource.getBuildingSites()
            return if (remoteResult != null) {
                saveBuildingSitesAndFrontLoads(remoteResult)
                Result.Success(
                    OnDemandServiceMappers.mapApiBuildingSitesToDomainBuildingSites(
                        remoteResult
                    )
                )
            } else Result.Error(Exception("Tuvimos un problema cargando las obras. Intente de nuevo mas tarde."))
        } else {
            return Result.Success(
                OnDemandServiceMappers.mapDbBuildingSitesToDomainBuildingSites(
                    localResult
                )
            )
        }
    }

    override suspend fun getPlacesByType(type: String): Result<List<Place>> {
        return driverRemoteDataSource.getPlacesByType(type)
    }

    override suspend fun getMaterials(): Result<List<OnDemandMaterial>> {
        //first check the local db
        val localResult = driverLocalDataSource.getMaterials()
        if (localResult.isEmpty()) {
            return when (val result = driverRemoteDataSource.getMaterials()) {
                is Result.Success -> {
                    saveMaterials(result.data)
                    Result.Success(MaterialMappers.mapListApiToDomain(result.data))
                }
                is Result.Error -> Result.Error(Exception("Tuvimos un problema cargando los materiales. Intente de nuevo mas tarde."))

            }
        } else {
            return Result.Success(
                MaterialMappers.mapDbMaterialsToDomainMaterials(
                    localResult
                )
            )
        }
    }

    override suspend fun createOnDemandService(service: CreateOnDemandService): Result<Int> {
        return driverRemoteDataSource.createOnDemandService(service)
    }

    override suspend fun uploadOriginDocument(serviceId: Int): Result<Unit> {
        val path = driverLocalDataSource.getOnDemandOriginPath()
        return if (path.isNullOrEmpty()) Result.Success(Unit) else driverRemoteDataSource.uploadOriginDocument(
            serviceId,
            path
        )
    }

    override suspend fun uploadDestinationDocument(serviceId: Int): Result<Unit> {
        val path = driverLocalDataSource.getOnDemandDestinationPath()
        return if (path.isNullOrEmpty()) Result.Success(Unit) else driverRemoteDataSource.uploadDestinationDocument(
            serviceId,
            path
        )
    }

    override suspend fun uploadSupportDocument(serviceId: Int): Result<Unit> {
        val path = driverLocalDataSource.getOnDemandSupportPath()
        return if (path.isNullOrEmpty()) Result.Success(Unit) else driverRemoteDataSource.uploadSupportDocument(
            serviceId,
            path
        )
    }

    private suspend fun saveBuildingSitesAndFrontLoads(buildingSiteList: List<ApiDriverBuildingSite>) {
        val dbBuildingSiteList =
            OnDemandServiceMappers.mapApiBuildingSitesToDbBuildingSites(buildingSiteList)
        val dbFrontLoadList = OnDemandServiceMappers.mapApiFrontLoadToDbFrontLoad(buildingSiteList)
        driverLocalDataSource.saveBuildingSitesAndFrontLoads(dbBuildingSiteList, dbFrontLoadList)
    }

    private suspend fun saveMaterials(materials: List<ApiOnDemandMaterial>) {
        val dbMaterials = MaterialMappers.mapApiMaterialsToDbMaterials(materials)
        driverLocalDataSource.saveMaterials(dbMaterials)
    }

    override suspend fun getVehicle(): Vehicle? {
        return getVehicleInfo()
    }

    override suspend fun updateJourneyInDb(journey: Journey) {
        driverLocalDataSource.updateJourneyInDb(ServiceMappers.mapToDb(journey))
    }

    override suspend fun updateJourneyInDb(
        journey: String,
        visitedOrigin: Boolean,
        visitedDestination: Boolean
    ) {
        val journeyDb = driverLocalDataSource.getJourney(journey)
        journeyDb?.let {
            it.hasVisitedOrigin = visitedOrigin
            it.hasVisitedDestination = visitedDestination
            driverLocalDataSource.updateJourneyInDb(it)
        }
    }

    override suspend fun updateJourneyWithRemission(journey: Journey): Result<Unit> {
        return when (val result = driverRemoteDataSource.updateJourneyWithRemission(
            journey.journeyId.toString(),
            journey.remissionDispatcher!!,
            journey.remissionNumber!!,
            journey.remissionDate!!,
            journey.remissionLoad!!,
            journey.remissionLoadType
        )) {
            is Result.Success -> {
                driverLocalDataSource.updateJourneyInDb(ServiceMappers.mapToDb(journey))
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    override suspend fun updateJourneyWithReceipt(journey: Journey): Result<Unit> {
        return when (val result = driverRemoteDataSource.updateJourneyWithReceipt(
            journey.journeyId.toString(),
            journey.receiptName!!,
            journey.receiptNumber!!,
            journey.receiptDate!!
        )) {
            is Result.Success -> {
                driverLocalDataSource.updateJourneyInDb(ServiceMappers.mapToDb(journey))
                Result.Success(Unit)
            }
            is Result.Error -> Result.Error(result.exception)

        }
    }

    override suspend fun getNotifications(): Result<List<Notification>> {
        return when (val result = driverRemoteDataSource.getNotifications()) {
            is Result.Success -> {
                val resultList = result.data
                Result.Success(NotificationMappers.mapApiNotificationToDomainNotification(resultList))
            }
            is Result.Error -> result
        }
    }

    override suspend fun updateNotificationsToSeen(unreadNotificationsId: List<String>) {
        unreadNotificationsId.forEach { id ->
            driverRemoteDataSource.updateNotificationsToSeen(id)
        }
    }

    override suspend fun handleVehicleInvitation(
        acceptInvitation: Boolean,
        vehicleInvitationId: String
    ): Result<Unit> {
        return driverRemoteDataSource.handleVehicleInvitation(acceptInvitation, vehicleInvitationId)
    }

    override suspend fun checkIfDriverHasPendingInvitations(): Result<List<PendingInvitation>> {
        return when (val result = driverRemoteDataSource.checkIfDriverHasPendingInvitations()) {
            is Result.Success -> {
                val resultList = result.data
                Result.Success(
                    PendingInvitationsMappers.mapApiInvitationsToDomainInvitations(
                        resultList
                    )
                )
            }
            is Result.Error -> result
        }
    }
}