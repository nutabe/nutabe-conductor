package com.jortegat.data.driver.datasource.models.service_response.documents_filled_response

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("background")
    val background: Boolean,
    @SerializedName("license")
    val license: Boolean,
    @SerializedName("runt")
    val runt: Boolean
)