package com.jortegat.data.driver.datasource.models.pending_notifications

import com.google.gson.annotations.SerializedName

data class ApiPendingInvitationsResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: List<ApiPendingInvitation>,
    @SerializedName("success")
    val success: Boolean
)