package com.jortegat.data.driver.datasource.models.notifications

import com.google.gson.annotations.SerializedName

data class ApiPayload(
    @SerializedName("journeyId")
    val journeyId: Int?,
    @SerializedName("truckDriverInvitationId")
    val truckDriverInvitationId: Int?,
    @SerializedName("vehicleId")
    val vehicleId: String?
)