package com.jortegat.data.driver.datasource.models.remission_receipt

import com.google.gson.annotations.SerializedName

/*This model is only used to send a POST to the back to update a journey in progress
* with this information of the remission */

data class JourneyRemission(
    val dispatcher: String,
    val number: String,
    val date: String,
    var load: Long,
    var loadType: String,
)