package com.jortegat.data.driver.datasource.models.notifications

import com.google.gson.annotations.SerializedName

data class ApiNotification(
    @SerializedName("__v")
    val __v: Int,
    @SerializedName("_id")
    val _id: String,
    @SerializedName("body")
    val body: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("identification")
    val identification: String,
    @SerializedName("link")
    val link: ApiLink,
    @SerializedName("payload")
    val payload: ApiPayload,
    @SerializedName("status")
    val status: String,
    @SerializedName("title")
    val title: String
)