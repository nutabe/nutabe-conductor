package com.jortegat.data.driver.datasource.models

import com.google.gson.annotations.SerializedName
import com.jortegat.base.models.domain.Driver

data class ApiDriverResponse(
        @SerializedName("code")
        val code: Long,
        @SerializedName("success")
        val success: Boolean,
        @SerializedName("data")
        val data: Driver
)
