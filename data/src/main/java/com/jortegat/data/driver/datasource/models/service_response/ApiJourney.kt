package com.jortegat.data.driver.datasource.models.service_response

import com.google.gson.annotations.SerializedName
import com.jortegat.base.models.domain.OrderGuide

data class ApiJourney(
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("driverStatus")
    val driverStatus: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("isActive")
    val isActive: Boolean,
    @SerializedName("journeyStatus")
    val journeyStatus: String,
    @SerializedName("ownerId")
    val ownerId: String,
    @SerializedName("receiptName")
    val receiptName: String?,
    @SerializedName("receiptNumber")
    val receiptNumber: String?,
    @SerializedName("receiptDate")
    val receiptDate: String?,
    @SerializedName("remissionDispatcher")
    val remissionDispatcher: String?,
    @SerializedName("remissionNumber")
    val remissionNumber: String?,
    @SerializedName("remissionDate")
    val remissionDate: String?,
    @SerializedName("vehicleId")
    val vehicleId: String,
    @SerializedName("load")
    var load: Long?,
    @SerializedName("loadType")
    var loadType: String?,
    @SerializedName("guide")
    val guide: OrderGuide?,
    @SerializedName("serial")
    val serial: Int
)