package com.jortegat.data.driver.datasource.models.notifications

import com.google.gson.annotations.SerializedName

data class ApiLink(
    @SerializedName("_id")
    val _id: String,
    @SerializedName("androidUrl")
    val androidUrl: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("package")
    val `package`: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("userType")
    val userType: String,
    @SerializedName("webUrl")
    val webUrl: String?
)