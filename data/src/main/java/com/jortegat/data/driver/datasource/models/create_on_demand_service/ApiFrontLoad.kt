package com.jortegat.data.driver.datasource.models.create_on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiFrontLoad(
    @SerializedName("assistant")
    val assistant: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("isActive")
    val isActive: Boolean,
    @SerializedName("isDump")
    val isDump: Boolean,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("location")
    val location: String,
    @SerializedName("longitude")
    val longitude: Double
)