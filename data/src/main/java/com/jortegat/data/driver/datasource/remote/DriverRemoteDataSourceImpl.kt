package com.jortegat.data.driver.datasource.remote

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.jortegat.base.helpers.Result
import com.jortegat.base.helpers.UploadFilesManager
import com.jortegat.base.helpers.isPdf
import com.jortegat.base.models.ApiDriver
import com.jortegat.base.models.FavoriteOwner
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.*
import com.jortegat.base.models.material_by_id.ApiMaterialById
import com.jortegat.base.models.on_demand_material.ApiOnDemandMaterial
import com.jortegat.data.driver.datasource.models.create_on_demand_service.ApiDriverBuildingSite
import com.jortegat.data.driver.datasource.models.notifications.ApiNotification
import com.jortegat.data.driver.datasource.models.on_demand_service.ApiOnDemandService
import com.jortegat.data.driver.datasource.models.pending_notifications.ApiPendingInvitation
import com.jortegat.data.driver.datasource.models.remission_receipt.JourneyReceipt
import com.jortegat.data.driver.datasource.models.remission_receipt.JourneyRemission
import com.jortegat.data.driver.datasource.models.service_response.ApiService
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import com.jortegat.domain.helper.PreferencesManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import java.io.IOException

class DriverRemoteDataSourceImpl(
    private val driverApi: DriverApi,
    private val preferencesManager: PreferencesManager,
    private val uploadFilesManager: UploadFilesManager
) : DriverRemoteDataSource {

    private lateinit var frontBitmap: Bitmap
    private lateinit var backBitmap: Bitmap
    private lateinit var frontFileExtension: String
    private lateinit var backFileExtension: String

    override suspend fun uploadDriverData(
        licenseCategory: String,
        frontPhotoPath: String,
        backPhotoPath: String
    ): Result<Unit> {
        frontFileExtension = frontPhotoPath.substring(frontPhotoPath.length - 3)
        if (frontFileExtension.isPdf().not()) frontBitmap = BitmapFactory.decodeFile(frontPhotoPath)
        backFileExtension = backPhotoPath.substring(backPhotoPath.length - 3)
        if (backFileExtension.isPdf().not()) backBitmap = BitmapFactory.decodeFile(backPhotoPath)

        return try {
            val response = driverApi.sendLicenseCategory(
                "https://api.nutabe.com/driver/truckDriver",
                ApiDriver(licenseCategory)
            )
            if (response.isSuccessful) {
                sendPhotos(frontPhotoPath, backPhotoPath)
            } else {
                Result.Error(IOException())
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    private suspend fun sendPhotos(frontPhotoPath: String, backPhotoPath: String): Result<Unit> {

        val frontFile =
            uploadFilesManager.createLocalFile("front", frontFileExtension, frontPhotoPath)
        if (frontFileExtension.isPdf().not()) uploadFilesManager.compressFile(
            frontFile,
            frontBitmap,
            frontFileExtension
        )

        val backFile = uploadFilesManager.createLocalFile("back", backFileExtension, backPhotoPath)
        if (backFileExtension.isPdf().not()) uploadFilesManager.compressFile(
            backFile,
            backBitmap,
            backFileExtension
        )

        val frontBody: MultipartBody.Part =
            uploadFilesManager.createMultiPart(frontFile, frontFileExtension, "front")
        val backBody: MultipartBody.Part =
            uploadFilesManager.createMultiPart(backFile, backFileExtension, "back")

        val imageResponse = driverApi.uploadFiles(
            "https://api.nutabe.com/driver/truckDriver/license",
            frontBody, backBody
        )
        return if (imageResponse.isSuccessful) {
            preferencesManager.setHasDocumentsFilled()
            Result.Success(Unit)
        } else {
            Result.Error(IOException())
        }
    }

    override suspend fun validateDocumentsAreFilled(): Result<Boolean> {
        return try {
            val response =
                driverApi.areDocumentsFilled("https://api.nutabe.com/driver/truckDriver/documents/isVerified")
            if (response.isSuccessful) {
                val documentList = response.body()?.data
                if (documentList != null) {
                    Result.Success(documentList.license && documentList.runt)
                } else {
                    Result.Error(Exception("Se produjó un fallo obteniendo la lista de documentos del conductor"))
                }
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo la lista de documentos del conductor"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun getListOfServices(vehicleId: String): Result<List<ApiService>> {
        return try {
            val response =
                driverApi.getListOfServices("https://api.nutabe.com/order/service/vehicle/$vehicleId/accepted?page=1&limit=20")
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo el listado de servicios"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun getListOfFinishedServices(vehicleId: String): Result<List<ApiService>> {
        return try {
            val response =
                driverApi.getListOfServices("https://api.nutabe.com/order/service/vehicle/$vehicleId/finished?page=1&limit=20")
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo el listado de servicios"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun getMaterialById(id: Int): ApiMaterialById? {
        return try {
            val response =
                driverApi.getMaterialById("https://api.nutabe.com/builder/material/$id/public")
            if (response.isSuccessful) {
                response.body()!!.data
            } else {
                null
            }
        } catch (e: IOException) {
            null
        }
    }

    override suspend fun getUserById(id: String): FavoriteOwner? {
        return try {
            val response =
                driverApi.getUserById("https://api.nutabe.com/auth/account/profile?identification=$id")
            if (response.isSuccessful) {
                response.body()!!.data
            } else {
                null
            }
        } catch (e: IOException) {
            null
        }
    }

    override suspend fun getMyVehicle(): Vehicle? {
        return if (vehicle != null) vehicle
        else try {
            val response =
                driverApi.getMyVehicle("https://api.nutabe.com/driver/truckDriver/vehicle/me")
            if (response.isSuccessful) {
                response.body()!!.data.vehicle.also {
                    vehicle = it
                }
            } else {
                null
            }
        } catch (e: IOException) {
            null
        }
    }

    override suspend fun acceptJourney(id: String): Result<Unit> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response =
                driverApi.acceptJourney("https://api.nutabe.com/order/journey/$id/driver/accept")
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun rejectJourney(id: String): Result<Unit> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response =
                driverApi.rejectJourney("https://api.nutabe.com/order/journey/$id/driver/deny")
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun startJourney(id: String): Result<Unit> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = driverApi.startJourney("https://api.nutabe.com/order/journey/$id/start")
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun resumeJourney(id: String): Result<Unit> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response =
                driverApi.resumeJourney("https://api.nutabe.com/order/journey/$id/resume")
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun finishJourney(id: String): Result<Unit> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response =
                driverApi.finishJourney("https://api.nutabe.com/order/journey/$id/driver/finish")
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                response.code()
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun sendLocation(
        journeyId: String,
        vehicleLocation: VehicleLocation
    ): Result<Unit> {
        return try {
            val response = driverApi.sendLocation(
                "https://api.nutabe.com/route/journey/$journeyId/phone",
                vehicleLocation
            )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                Result.Error(Exception(ERROR))
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    override suspend fun getJourneyDetail(id: String): Result<CustomJourney?> {
        return try {
            val response = driverApi.getJourneyDetail(
                "https://api.nutabe.com/order/journey/$id"
            )
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception(ERROR))
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    override suspend fun getJourneyDestination(id: String): Result<JourneyDestination> {
        return try {
            val response = driverApi.getJourneyDestination(
                "https://api.nutabe.com/order/journey/$id/geoposition/client"
            )
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception(ERROR))
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    override suspend fun getStartedJourney(id: String): Result<CustomJourney?> {
        return try {
            val response = driverApi.getStartedJourney(
                "https://api.nutabe.com/order/journey/vehicle/$id/doing"
            )
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception(ERROR))
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    override suspend fun getDriverInformation(): Result<Driver> {
        return try {
            val response =
                driverApi.getDriverInformation("https://api.nutabe.com/driver/truckDriver")
            if (response.isSuccessful) {
                val driver = getDriverAdditionalInfo(response.body()!!.data)
                Result.Success(driver)
            } else {
                Result.Error(Exception(ERROR))
            }
        } catch (e: IOException) {
            Result.Error(e)
        }
    }

    private suspend fun getDriverAdditionalInfo(driver: Driver): Driver {
        getUserById(driver.identification)?.run {
            driver.name = "$firstName $lastName"
        }
        driver.vehicle = getMyVehicle()
        return driver
    }

    override suspend fun getBuildingSites(): List<ApiDriverBuildingSite>? {
        return try {
            val response =
                driverApi.getBuildingSites("https://api.nutabe.com/builder/buildingSite/all/withFrontloads?page=1&limit=30")
            if (response.isSuccessful) {
                response.body()!!.driverBuildingSites
            } else {
                null
            }
        } catch (e: IOException) {
            null
        }
    }

    override suspend fun getOnDemandServices(): Result<List<ApiOnDemandService>> {
        return try {
            val response =
                driverApi.getListOfOnDemandServices("https://api.nutabe.com/order/journey/nonScheduled/me/driver")
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo el listado de servicios."))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun getPlacesByType(type: String): Result<List<Place>> {
        return try {
            val response =
                driverApi.getPlacesByType("https://api.nutabe.com/builder/place?page=1&limit=10&type=$type")
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo el listado de lugares por $type"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun getMaterials(): Result<List<ApiOnDemandMaterial>> {
        return try {
            val response =
                driverApi.getOnDemandMaterials("https://api.nutabe.com/builder/material/general/all?page=1&limit=30")
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo el listado de materiales"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun createOnDemandService(service: CreateOnDemandService): Result<Int> {
        return try {
            val response =
                driverApi.createOnDemandService(
                    "https://api.nutabe.com/order/journey/nonScheduled",
                    service
                )
            if (response.isSuccessful) {
                Result.Success(response.body()!!.data)
            } else {
                Result.Error(Exception("Se produjó un fallo creando el servicio"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun updateJourneyWithRemission(
        id: String,
        remissionName: String,
        remissionNumber: String,
        remissionDate: String,
        remissionLoad: Long,
        remissionLoadType: String
    ): Result<Unit> {
        return try {
            val remission = JourneyRemission(
                remissionName,
                remissionNumber,
                remissionDate,
                remissionLoad,
                remissionLoadType
            )
            val response =
                driverApi.updateJourneyWithRemission(
                    "https://api.nutabe.com/order/journey/$id/remission",
                    remission
                )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                response.code()
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun updateJourneyWithReceipt(
        id: String,
        receiptName: String,
        receiptNumber: String,
        receiptDate: String
    ): Result<Unit> {
        return try {
            val receipt = JourneyReceipt(receiptName, receiptNumber, receiptDate)
            val response =
                driverApi.updateJourneyWithReceipt(
                    "https://api.nutabe.com/order/journey/$id/receipt",
                    receipt
                )
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                response.code()
                val errorBody = response.errorBody()?.string() ?: ""
                val errorMessage = getErrorMessage(errorBody)
                Result.Error(Exception(errorMessage))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun uploadOriginDocument(serviceId: Int, path: String): Result<Unit> {
        lateinit var fileBitmap: Bitmap

        val fileExtension: String = path.substring(path.length - 3)
        if (fileExtension.isPdf().not()) fileBitmap = BitmapFactory.decodeFile(path)

        val file =
            uploadFilesManager.createLocalFile("document", fileExtension, path)
        if (fileExtension.isPdf().not()) uploadFilesManager.compressFile(
            file,
            fileBitmap,
            fileExtension
        )

        val body: MultipartBody.Part =
            uploadFilesManager.createMultiPart(file, fileExtension, "document")

        val uploadResponse = driverApi.uploadOnDemandFiles(
            "https://api.nutabe.com/order/journey/nonScheduled/$serviceId/remission",
            body
        )
        return if (uploadResponse.isSuccessful) {
            Result.Success(Unit)
        } else {
            Result.Error(IOException())
        }
    }

    override suspend fun uploadDestinationDocument(serviceId: Int, path: String): Result<Unit> {
        lateinit var fileBitmap: Bitmap

        val fileExtension: String = path.substring(path.length - 3)
        if (fileExtension.isPdf().not()) fileBitmap = BitmapFactory.decodeFile(path)

        val file =
            uploadFilesManager.createLocalFile("document", fileExtension, path)
        if (fileExtension.isPdf().not()) uploadFilesManager.compressFile(
            file,
            fileBitmap,
            fileExtension
        )

        val body: MultipartBody.Part =
            uploadFilesManager.createMultiPart(file, fileExtension, "document")

        val uploadResponse = driverApi.uploadOnDemandFiles(
            "https://api.nutabe.com/order/journey/nonScheduled/$serviceId/receipt",
            body
        )
        return if (uploadResponse.isSuccessful) {
            Result.Success(Unit)
        } else {
            Result.Error(IOException())
        }
    }

    override suspend fun uploadSupportDocument(serviceId: Int, path: String): Result<Unit> {
        lateinit var fileBitmap: Bitmap

        val fileExtension: String = path.substring(path.length - 3)
        if (fileExtension.isPdf().not()) fileBitmap = BitmapFactory.decodeFile(path)

        val file =
            uploadFilesManager.createLocalFile("document", fileExtension, path)
        if (fileExtension.isPdf().not()) uploadFilesManager.compressFile(
            file,
            fileBitmap,
            fileExtension
        )

        val body: MultipartBody.Part =
            uploadFilesManager.createMultiPart(file, fileExtension, "document")

        val uploadResponse = driverApi.uploadOnDemandFiles(
            "https://api.nutabe.com/order/journey/nonScheduled/$serviceId/supportDocument",
            body
        )
        return if (uploadResponse.isSuccessful) {
            Result.Success(Unit)
        } else {
            Result.Error(IOException())
        }
    }

    override suspend fun getNotifications(): Result<List<ApiNotification>> {
        return try {
            val response =
                driverApi.getNotifications("https://api.nutabe.com/notification/feed?page=1&limit=30")
            if (response.isSuccessful) {
                val listOfNotifications = response.body()!!.data
                Result.Success(listOfNotifications)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo las notificaciones"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun checkIfDriverHasPendingInvitations(): Result<List<ApiPendingInvitation>> {
        return try {
            val response =
                driverApi.getPendingInvitations("https://api.nutabe.com/driver/truckDriverInvitation/me")
            if (response.isSuccessful) {
                val listOfInvitations = response.body()!!.data
                Result.Success(listOfInvitations)
            } else {
                Result.Error(Exception("Se produjó un fallo obteniendo las invitaciones a ser conductor"))
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    override suspend fun updateNotificationsToSeen(id: String) {
        driverApi.updateNotificationToSeen("https://api.nutabe.com/notification/feed/$id/seen")
    }

    override suspend fun handleVehicleInvitation(
        acceptInvitation: Boolean,
        vehicleInvitationId: String
    ): Result<Unit> {
        return try {
            if (acceptInvitation) {

                val response =
                    driverApi.acceptInvitation("https://api.nutabe.com/driver/truckDriverInvitation/$vehicleInvitationId/accepted")
                if (response.isSuccessful) {
                    Result.Success(Unit)
                } else {
                    val errorBody = response.errorBody()?.string() ?: ""
                    val errorMessage = getErrorMessage(errorBody)
                    Result.Error(Exception(errorMessage))
                }
            } else {
                val response =
                    driverApi.rejectInvitation("https://api.nutabe.com/driver/truckDriverInvitation/$vehicleInvitationId/denied")
                if (response.isSuccessful) {
                    Result.Success(Unit)
                } else {
                    val errorBody = response.errorBody()?.string() ?: ""
                    val errorMessage = getErrorMessage(errorBody)
                    Result.Error(Exception(errorMessage))
                }
            }
        } catch (e: IOException) {
            Result.Error(IOException(ERROR))
        }
    }

    private fun getErrorMessage(errorBody: String): String {
        return when {
            errorBody.contains("Driver cant accept journey at this status") ->
                "No es posible aceptar este viaje por el estado en el que se encuentra"
            errorBody.contains("Unable to deny journey, invalid status to deny") ->
                "No es posible rechazar este viaje por el estado en el que se encuentra"
            errorBody.contains("Unable to find vehicle") ->
                "No fue posible encontrar su vehículo"
            errorBody.contains("Invalid journey status to start") ->
                "No fue posible iniciar este viaje por el estado en el que se encuentra"
            errorBody.contains("Unable to finish journey that isnt in process status") ->
                "Este viaje aún no ha sido iniciado"
            errorBody.contains("Vehicle didnt pass throught all fences service directions") ->
                "Aún no has completado este viaje"
            errorBody.contains("Unable to validate if journey is finished") ->
                "No pudimos validar si el viaje ya fue completado. Por favor comunicate con tu contratista."
            errorBody.contains("Vehicle didnt pass throught all fences service directions") ->
                "El vehiculo no visitó todos los cercos."
            errorBody.contains("Unable to validate if journey is finished") ->
                "No pudimos validar si visitó todos los cercos. Por favor comunicate con tu contratista."
            errorBody.contains("Journey already have receipt") ->
                "Este viaje ya tiene un recibo."
            errorBody.contains("Journey already have remission") ->
                "Este viaje ya tiene una remission."
            errorBody.contains("Unable to find driver by identification") ->
                "No fue posible encontrar el conductor"
            errorBody.contains("Unable to find truck driver invitation") ->
                "No fue posible encontrar la invitación"
            errorBody.contains("Unable to update invitation") ->
                "Invitación previamente aceptada o rechazada"
            else -> ERROR
        }
    }

    companion object {
        const val ERROR = "Tuvimos un error, intentalo mas tarde"
        var vehicle: Vehicle? = null
    }
}