package com.jortegat.data.driver.datasource.models.pending_notifications

import com.google.gson.annotations.SerializedName

data class ApiPendingInvitation(
    @SerializedName("id")
    val id: Int,
    @SerializedName("truckDriverIdentification")
    val truckDriverIdentification: String,
    @SerializedName("vehicleId")
    val vehicleId: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("createdAt")
    val createdAt: String
)