package com.jortegat.data.driver.datasource.models.remission_receipt

/*This model is only used to send a POST to the back to update a journey in progress
* with this information of the receipt */

data class JourneyReceipt(
    val name: String,
    val number: String,
    val date: String,
)