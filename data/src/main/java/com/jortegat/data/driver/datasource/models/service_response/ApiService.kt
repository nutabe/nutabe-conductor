package com.jortegat.data.driver.datasource.models.service_response

import com.google.gson.annotations.SerializedName

data class ApiService(
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("createdBy")
    val createdBy: String,
    @SerializedName("destination")
    val destination: String,
    @SerializedName("destinationType")
    val destinationType: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("load")
    val load: Int,
    @SerializedName("materialId")
    val materialId: Int,
    @SerializedName("orderId")
    val orderId: String,
    @SerializedName("origin")
    val origin: String,
    @SerializedName("originType")
    val originType: String,
    @SerializedName("programs")
    val apiPrograms: List<ApiProgram>,
    @SerializedName("serviceType")
    val serviceType: String,
    @SerializedName("vehicleType")
    val vehicleType: String
)