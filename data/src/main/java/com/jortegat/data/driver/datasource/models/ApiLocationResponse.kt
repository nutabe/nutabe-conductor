package com.jortegat.data.driver.datasource.models

import com.google.gson.annotations.SerializedName

data class ApiLocationResponse (
    @SerializedName("date")
    val date: String,
    @SerializedName("code")
    val code: Long,
    @SerializedName("message")
    val message: String
)
