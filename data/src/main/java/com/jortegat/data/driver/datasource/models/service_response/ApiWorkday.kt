package com.jortegat.data.driver.datasource.models.service_response

import com.google.gson.annotations.SerializedName

data class ApiWorkday(
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("endHour")
    val endHour: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("startHour")
    val startHour: String
)