package com.jortegat.data.driver.datasource.remote

import com.jortegat.base.helpers.Result
import com.jortegat.base.models.FavoriteOwner
import com.jortegat.base.models.Place
import com.jortegat.base.models.domain.*
import com.jortegat.base.models.material_by_id.ApiMaterialById
import com.jortegat.base.models.on_demand_material.ApiOnDemandMaterial
import com.jortegat.data.driver.datasource.models.create_on_demand_service.ApiDriverBuildingSite
import com.jortegat.data.driver.datasource.models.notifications.ApiNotification
import com.jortegat.data.driver.datasource.models.on_demand_service.ApiOnDemandService
import com.jortegat.data.driver.datasource.models.pending_notifications.ApiPendingInvitation
import com.jortegat.data.driver.datasource.models.service_response.ApiService
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService

interface DriverRemoteDataSource {

    suspend fun uploadDriverData(
        licenseCategory: String,
        frontPhotoPath: String,
        backPhotoPath: String
    ): Result<Unit>

    suspend fun getListOfServices(vehicleId: String): Result<List<ApiService>>

    suspend fun getListOfFinishedServices(vehicleId: String): Result<List<ApiService>>

    suspend fun getMaterialById(id: Int): ApiMaterialById?

    suspend fun getUserById(id: String): FavoriteOwner?

    suspend fun getMyVehicle(): Vehicle?

    suspend fun acceptJourney(id: String): Result<Unit>

    suspend fun rejectJourney(id: String): Result<Unit>

    suspend fun startJourney(id: String): Result<Unit>

    suspend fun resumeJourney(id: String): Result<Unit>

    suspend fun finishJourney(id: String): Result<Unit>

    suspend fun sendLocation(journeyId: String, vehicleLocation: VehicleLocation): Result<Unit>

    suspend fun getJourneyDetail(id: String): Result<CustomJourney?>

    suspend fun getJourneyDestination(id: String): Result<JourneyDestination>

    suspend fun getStartedJourney(id: String): Result<CustomJourney?>

    suspend fun getDriverInformation(): Result<Driver>

    suspend fun validateDocumentsAreFilled(): Result<Boolean>

    suspend fun getOnDemandServices(): Result<List<ApiOnDemandService>>

    suspend fun getBuildingSites(): List<ApiDriverBuildingSite>?

    suspend fun getPlacesByType(type: String): Result<List<Place>>

    suspend fun getMaterials(): Result<List<ApiOnDemandMaterial>>

    suspend fun createOnDemandService(service: CreateOnDemandService): Result<Int>

    suspend fun uploadOriginDocument(serviceId: Int, path: String): Result<Unit>

    suspend fun uploadDestinationDocument(serviceId: Int, path: String): Result<Unit>

    suspend fun uploadSupportDocument(serviceId: Int, path: String): Result<Unit>

    suspend fun updateJourneyWithRemission(
        id: String,
        remissionName: String,
        remissionNumber: String,
        remissionDate: String,
        remissionLoad: Long,
        remissionLoadType: String
    ): Result<Unit>

    suspend fun updateJourneyWithReceipt(
        id: String,
        receiptName: String,
        receiptNumber: String,
        receiptDate: String
    ): Result<Unit>

    suspend fun getNotifications(): Result<List<ApiNotification>>

    suspend fun updateNotificationsToSeen(id: String)

    suspend fun handleVehicleInvitation(
        acceptInvitation: Boolean,
        vehicleInvitationId: String
    ): Result<Unit>

    suspend fun checkIfDriverHasPendingInvitations(): Result<List<ApiPendingInvitation>>
}