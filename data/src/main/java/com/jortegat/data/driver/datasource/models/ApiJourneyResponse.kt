package com.jortegat.data.driver.datasource.models

import com.google.gson.annotations.SerializedName
import com.jortegat.base.models.domain.CustomJourney

data class ApiJourneyResponse(
    @SerializedName("code")
    val code: Long,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("data")
    val data: CustomJourney?
)
