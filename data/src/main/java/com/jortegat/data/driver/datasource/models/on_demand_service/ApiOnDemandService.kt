package com.jortegat.data.driver.datasource.models.on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiOnDemandService(
    @SerializedName("buildingSiteId")
    val buildingSiteId: Int,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("createdBy")
    val createdBy: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("destination")
    val destination: String,
    @SerializedName("destinationType")
    val destinationType: String,
    @SerializedName("driverId")
    val driverId: String,
    @SerializedName("endHorometer")
    val endHorometer: Int?,
    @SerializedName("endHour")
    val endHour: String,
    @SerializedName("endKilometers")
    val endKilometers: Int,
    @SerializedName("endPk")
    val endPk: String?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("isActive")
    val isActive: Boolean,
    @SerializedName("load")
    val load: Int,
    @SerializedName("materialId")
    val materialId: Int,
    @SerializedName("origin")
    val origin: String,
    @SerializedName("originType")
    val originType: String,
    @SerializedName("receipt")
    val receipt: ApiOnDemandDocument?,
    @SerializedName("remission")
    val remission: ApiOnDemandDocument?,
    @SerializedName("serviceType")
    val serviceType: String,
    @SerializedName("startHorometer")
    val startHorometer: Int?,
    @SerializedName("startHour")
    val startHour: String,
    @SerializedName("startKilometers")
    val startKilometers: Int,
    @SerializedName("startPk")
    val startPk: String?,
    @SerializedName("supportDocument")
    val supportDocument: ApiOnDemandDocument?,
    @SerializedName("vehicleId")
    val vehicleId: String
)