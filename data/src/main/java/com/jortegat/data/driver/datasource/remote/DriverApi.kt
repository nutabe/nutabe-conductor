package com.jortegat.data.driver.datasource.remote

import com.jortegat.base.models.ApiDriver
import com.jortegat.base.models.ApiPlacesResponse
import com.jortegat.base.models.GeneralResponseModel
import com.jortegat.base.models.api.ApiJourneyDestinationResponse
import com.jortegat.base.models.api.ApiJourneyOperationResponse
import com.jortegat.base.models.api.ApiUserResponse
import com.jortegat.base.models.domain.VehicleLocation
import com.jortegat.base.models.material_by_id.ApiMaterialByIdResponse
import com.jortegat.base.models.on_demand_material.ApiOnDemandMaterialResponse
import com.jortegat.data.driver.datasource.models.ApiDriverResponse
import com.jortegat.data.driver.datasource.models.ApiJourneyResponse
import com.jortegat.data.driver.datasource.models.ApiLocationResponse
import com.jortegat.data.driver.datasource.models.create_on_demand_service.ApiListOfDriverBuildingSitesResponse
import com.jortegat.data.driver.datasource.models.notifications.ApiGetNotificationsResponse
import com.jortegat.data.driver.datasource.models.on_demand_service.ApiCreateOnDemandServiceResponse
import com.jortegat.data.driver.datasource.models.on_demand_service.ApiOnDemandServiceResponse
import com.jortegat.data.driver.datasource.models.pending_notifications.ApiPendingInvitationsResponse
import com.jortegat.data.driver.datasource.models.remission_receipt.JourneyReceipt
import com.jortegat.data.driver.datasource.models.remission_receipt.JourneyRemission
import com.jortegat.data.driver.datasource.models.service_response.ApiServiceResponse
import com.jortegat.data.driver.datasource.models.service_response.documents_filled_response.ApiDriverFilledDocumentsResponse
import com.jortegat.domain.driver.model.services.create_on_demand_service.CreateOnDemandService
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface DriverApi {

    @POST
    suspend fun sendLicenseCategory(
        @Url url: String,
        @Body driver: ApiDriver
    ): Response<GeneralResponseModel>

    @Multipart
    @POST
    suspend fun uploadFiles(
        @Url url: String,
        @Part front: MultipartBody.Part,
        @Part back: MultipartBody.Part
    ): Response<GeneralResponseModel>

    @GET
    suspend fun getMaterialById(
        @Url url: String
    ): Response<ApiMaterialByIdResponse>

    @GET
    suspend fun getListOfServices(
        @Url url: String
    ): Response<ApiServiceResponse>

    @GET
    suspend fun areDocumentsFilled(
        @Url url: String
    ): Response<ApiDriverFilledDocumentsResponse>

    @GET
    suspend fun getUserById(
        @Url url: String
    ): Response<ApiUserResponse>

    @GET
    suspend fun getMyVehicle(
        @Url url: String
    ): Response<ApiDriverResponse>

    @POST
    suspend fun acceptInvitation(
        @Url url: String
    ): Response<GeneralResponseModel>

    @POST
    suspend fun rejectInvitation(
        @Url url: String
    ): Response<GeneralResponseModel>

    @POST
    suspend fun acceptJourney(
        @Url url: String
    ): Response<ApiJourneyOperationResponse>

    @POST
    suspend fun rejectJourney(
        @Url url: String
    ): Response<ApiJourneyOperationResponse>

    @POST
    suspend fun startJourney(
        @Url url: String
    ): Response<ApiJourneyOperationResponse>

    @POST
    suspend fun resumeJourney(
        @Url url: String
    ): Response<ApiJourneyOperationResponse>

    @POST
    suspend fun finishJourney(
        @Url url: String
    ): Response<ApiJourneyOperationResponse>

    @POST
    suspend fun sendLocation(
        @Url url: String,
        @Body vehicleLocation: VehicleLocation
    ): Response<ApiLocationResponse>

    @GET
    suspend fun getJourneyDetail(
        @Url url: String
    ): Response<ApiJourneyResponse>

    @GET
    suspend fun getJourneyDestination(
        @Url url: String
    ): Response<ApiJourneyDestinationResponse>

    @GET
    suspend fun getStartedJourney(
        @Url url: String
    ): Response<ApiJourneyResponse>

    @GET
    suspend fun getDriverInformation(
        @Url url: String
    ): Response<ApiDriverResponse>

    @GET
    suspend fun getBuildingSites(
        @Url url: String
    ): Response<ApiListOfDriverBuildingSitesResponse>

    @GET
    suspend fun getListOfOnDemandServices(
        @Url url: String
    ): Response<ApiOnDemandServiceResponse>

    @GET
    suspend fun getPlacesByType(
        @Url url: String
    ): Response<ApiPlacesResponse>

    @GET
    suspend fun getOnDemandMaterials(
        @Url url: String
    ): Response<ApiOnDemandMaterialResponse>

    @POST
    suspend fun createOnDemandService(
        @Url url: String,
        @Body createOnDemandService: CreateOnDemandService
    ): Response<ApiCreateOnDemandServiceResponse>

    @POST
    suspend fun updateJourneyWithRemission(
        @Url url: String,
        @Body remission: JourneyRemission
    ): Response<ApiJourneyOperationResponse>

    @POST
    suspend fun updateJourneyWithReceipt(
        @Url url: String,
        @Body receipt: JourneyReceipt
    ): Response<ApiJourneyOperationResponse>

    @Multipart
    @POST
    suspend fun uploadOnDemandFiles(
        @Url url: String,
        @Part document: MultipartBody.Part
    ): Response<GeneralResponseModel>

    @GET
    suspend fun getNotifications(
        @Url url: String
    ): Response<ApiGetNotificationsResponse>

    @GET
    suspend fun getPendingInvitations(
        @Url url: String
    ): Response<ApiPendingInvitationsResponse>

    @PUT
    suspend fun updateNotificationToSeen(
        @Url url: String
    ): Response<GeneralResponseModel>
}