package com.jortegat.data.driver.datasource.models.service_response

import com.google.gson.annotations.SerializedName

data class ApiProgram(
    @SerializedName("createdAt")
    val createdAt: String?,
    @SerializedName("date")
    val date: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("journeys")
    val apiJourneys: List<ApiJourney>,
    @SerializedName("workday")
    val apiWorkday: ApiWorkday
)