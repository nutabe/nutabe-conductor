package com.jortegat.data.driver.datasource.models.create_on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiListOfDriverBuildingSitesResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val driverBuildingSites: List<ApiDriverBuildingSite>,
    @SerializedName("success")
    val success: Boolean
)