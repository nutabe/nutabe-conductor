package com.jortegat.data.driver.datasource.models.notifications

import com.google.gson.annotations.SerializedName

data class ApiGetNotificationsResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: List<ApiNotification>,
    @SerializedName("success")
    val success: Boolean
)