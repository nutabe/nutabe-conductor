package com.jortegat.data.driver.datasource.models.on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiOnDemandDocument(
    @SerializedName("comment")
    val comment: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("images")
    val images: List<String>,
    @SerializedName("status")
    val status: String,
    @SerializedName("type")
    val type: String
)