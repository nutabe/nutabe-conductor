package com.jortegat.data.driver.datasource.models.on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiOnDemandServiceResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: List<ApiOnDemandService>,
    @SerializedName("success")
    val success: Boolean
)