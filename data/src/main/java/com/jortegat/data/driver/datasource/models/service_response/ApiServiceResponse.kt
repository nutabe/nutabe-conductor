package com.jortegat.data.driver.datasource.models.service_response

import com.google.gson.annotations.SerializedName

data class ApiServiceResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: List<ApiService>,
    @SerializedName("success")
    val success: Boolean
)