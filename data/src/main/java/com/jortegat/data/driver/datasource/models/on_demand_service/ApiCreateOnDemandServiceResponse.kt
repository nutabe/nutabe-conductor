package com.jortegat.data.driver.datasource.models.on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiCreateOnDemandServiceResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: Int,
    @SerializedName("success")
    val success: Boolean
)