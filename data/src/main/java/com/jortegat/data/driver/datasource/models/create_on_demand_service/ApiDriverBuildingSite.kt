package com.jortegat.data.driver.datasource.models.create_on_demand_service

import com.google.gson.annotations.SerializedName

data class ApiDriverBuildingSite(
    @SerializedName("frontLoads")
    val frontLoads: List<ApiFrontLoad>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)