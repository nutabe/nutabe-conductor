package com.jortegat.data.driver.datasource.models.service_response.documents_filled_response

import com.google.gson.annotations.SerializedName

data class ApiDriverFilledDocumentsResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("success")
    val success: Boolean
)