package com.jortegat.data.driver.datasource.local

import androidx.lifecycle.LiveData
import com.jortegat.base.enums.JourneyStatus
import com.jortegat.data.db.dao.*
import com.jortegat.data.db.entity.journey.DbJourney
import com.jortegat.data.db.entity.material.DbMaterial
import com.jortegat.data.db.entity.on_demand_services.DbDriverBuildingSite
import com.jortegat.data.db.entity.on_demand_services.DbDriverFrontLoad
import com.jortegat.data.db.entity.on_demand_services.relations.BuildingSiteWithFrontLoads
import com.jortegat.data.db.entity.vehicle.DbVehicle
import com.jortegat.data.db.entity.vehicle.DbVehicleLocation
import com.jortegat.domain.helper.PreferencesManager
import  com.jortegat.base.helpers.Result

class DriverLocalDataSourceImpl(
    private val preferencesManager: PreferencesManager,
    private val buildingSiteDAO: BuildingSiteDAO,
    private val materialDAO: MaterialDAO,
    private val vehicleDAO: VehicleDAO,
    private val journeyDAO: JourneyDAO,
    private val locationsDAO: LocationsDAO
) : DriverLocalDataSource {

    override fun saveFrontalPhotoPath(key: String, path: String?) {
        preferencesManager.saveFrontalPhotoPath(key, path)
    }

    override fun saveBackPhotoPath(key: String, path: String?) {
        preferencesManager.saveBackPhotoPath(key, path)
    }

    override fun saveOnDemandOriginFilePath(key: String, path: String?) {
        preferencesManager.saveOnDemandOriginFilePath(key, path)
    }

    override fun saveOnDemandDestinationFilePath(key: String, path: String?) {
        preferencesManager.saveOnDemandDestinationFilePath(key, path)
    }

    override fun saveOnDemandSupportFilePath(key: String, path: String?) {
        preferencesManager.saveOnDemandSupportFilePath(key, path)
    }

    override fun getFrontPhotoPath(): String {
        return preferencesManager.getFrontPhotoPath()!!
    }

    override fun getBackPhotoPath(): String {
        return preferencesManager.getBackPhotoPath()!!
    }

    override fun getOnDemandOriginFilePath(): String {
        return preferencesManager.getOnDemandOriginPath()!!
    }

    override fun getOnDemandDestinationFilePath(): String {
        return preferencesManager.getOnDemandDestinationPath()!!
    }

    override fun validateDocumentsAreFilled(): Boolean {
        return preferencesManager.validateDocumentsAreFilled()
    }

    override fun getOnDemandOriginPath(): String? {
        return preferencesManager.getOnDemandOriginPath()
    }

    override fun getOnDemandDestinationPath(): String? {
        return preferencesManager.getOnDemandDestinationPath()
    }

    override fun getOnDemandSupportPath(): String? {
        return preferencesManager.getOnDemandSupportPath()
    }

    override suspend fun saveBuildingSitesAndFrontLoads(
        buildingSiteList: List<DbDriverBuildingSite>,
        frontLoadList: List<DbDriverFrontLoad>
    ) {
        buildingSiteList.forEach {
            buildingSiteDAO.insertBuildingSite(it)
        }
        frontLoadList.forEach {
            buildingSiteDAO.insertFrontLoad(it)
        }
    }

    override suspend fun getBuildingSites(): List<BuildingSiteWithFrontLoads> {
        return buildingSiteDAO.getAllBuildingSites()
    }

    override suspend fun getBuildingSiteNameById(id: Int): String {
        return buildingSiteDAO.getBuildingSiteNameById(id)
    }

    override suspend fun saveMaterials(materials: List<DbMaterial>) {
        materials.forEach {
            materialDAO.insertMaterial(it)
        }
    }

    override suspend fun getMaterials(): List<DbMaterial> {
        return materialDAO.getAllMaterials()
    }

    override suspend fun getMaterialNameById(id: Int): String {
        return materialDAO.getMaterialNameById(id)
    }

    override suspend fun getVehicle(): List<DbVehicle> {
        return vehicleDAO.getVehicle()
    }

    override suspend fun saveVehicle(vehicle: DbVehicle) {
        return vehicleDAO.saveVehicle(vehicle)
    }

    override suspend fun saveJourneyList(journeyList: List<DbJourney>): Boolean {
        var localJourneyInProgress: DbJourney? = null
        //it must be only one but we are getting a list of one element by default
        val journeyListInProgress =
            journeyList.filter { (it.journeyStatus == JourneyStatus.PROCESS.serverMessage) || it.journeyStatus == JourneyStatus.PAUSED.serverMessage }
        if (journeyListInProgress.isNotEmpty()) {
            localJourneyInProgress = getJourney(journeyListInProgress.first().journeyId)
        }

        clearJourneyList()

        journeyList.forEach {

            if (localJourneyInProgress != null && it.journeyId == localJourneyInProgress.journeyId) {
                //this two properties don't come from backend so we need to save them before override them, we also check,
                //it.hasVisitedOrigin and it.hasVisitedDestination because even they didnt come in the backed we are setting from
                //the serviceMapper the value of the remission != null and receipt != null
                it.hasVisitedOrigin = localJourneyInProgress.hasVisitedOrigin || it.hasVisitedOrigin
                it.hasVisitedDestination = localJourneyInProgress.hasVisitedDestination || it.hasVisitedDestination

                //if the back has no info about remission, maybe it was pending to be sent, so we save it
                //before override it
                if (it.remissionDispatcher.isNullOrEmpty()) {
                    it.remissionDispatcher = localJourneyInProgress.remissionDispatcher
                    it.remissionNumber = localJourneyInProgress.remissionNumber
                    it.receiptDate = localJourneyInProgress.receiptDate
                }

                //if the back has no info about receipt, maybe it was pending to be sent, so we save it
                //before override it
                if (it.receiptName.isNullOrEmpty()) {
                    it.receiptName = localJourneyInProgress.receiptName
                    it.receiptNumber = localJourneyInProgress.receiptNumber
                    it.receiptDate = localJourneyInProgress.receiptDate
                }
            }

            journeyDAO.insertJourney(it)
        }
        return true
    }

    private suspend fun getJourney(journeyId: Int): DbJourney? {
        return journeyDAO.getJourney(journeyId.toString())
    }

    override fun getJourneys(): LiveData<List<DbJourney>?> {
        return journeyDAO.getAllJourneys()
    }

    override suspend fun getJourney(id : String): DbJourney? {
        return journeyDAO.getJourney(id)
    }

    override suspend fun updateJourneyInDb(journey: DbJourney) {
        journeyDAO.insertJourney(journey)
    }

    override suspend fun updateJourneyStatus(id: String, status: String) {
        journeyDAO.updateJourneyStatus(id, status)
    }

    override suspend fun deleteJourney(id: String) {
        journeyDAO.deleteJourney(id)
    }

    override suspend fun updateJourneyDriverStatus(id: String, status: String) {
        journeyDAO.updateJourneyDriverStatus(id, status)
    }

    override suspend fun clearJourneyList() {
        journeyDAO.clearAll()
    }

    override suspend fun saveLocation(location: DbVehicleLocation): Result<Long> {
        val response = locationsDAO.insert(location)
        return if (response != 0L) {
            Result.Success(response)
        } else {
            Result.Error(Exception("Location not saved"))
        }
    }

    override suspend fun getPendingLocations(): List<DbVehicleLocation> {
        return locationsDAO.getPendingLocations()
    }

    override suspend fun deleteLocation(id: Int): Int {
        return locationsDAO.deleteLocation(id)
    }
}