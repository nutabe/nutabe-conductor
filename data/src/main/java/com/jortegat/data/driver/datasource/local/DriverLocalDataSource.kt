package com.jortegat.data.driver.datasource.local

import androidx.lifecycle.LiveData
import com.jortegat.base.helpers.Result
import com.jortegat.data.db.entity.journey.DbJourney
import com.jortegat.data.db.entity.material.DbMaterial
import com.jortegat.data.db.entity.on_demand_services.DbDriverBuildingSite
import com.jortegat.data.db.entity.on_demand_services.DbDriverFrontLoad
import com.jortegat.data.db.entity.on_demand_services.relations.BuildingSiteWithFrontLoads
import com.jortegat.data.db.entity.vehicle.DbVehicle
import com.jortegat.data.db.entity.vehicle.DbVehicleLocation

interface DriverLocalDataSource {

    fun saveFrontalPhotoPath(key: String, path: String?)

    fun saveBackPhotoPath(key: String, path: String?)

    fun saveOnDemandOriginFilePath(key: String, path: String?)

    fun saveOnDemandDestinationFilePath(key: String, path: String?)

    fun saveOnDemandSupportFilePath(key: String, path: String?)

    fun getFrontPhotoPath(): String

    fun getBackPhotoPath(): String

    fun getOnDemandOriginFilePath(): String

    fun getOnDemandDestinationFilePath(): String

    fun validateDocumentsAreFilled(): Boolean

    fun getOnDemandOriginPath(): String?

    fun getOnDemandDestinationPath(): String?

    fun getOnDemandSupportPath(): String?

    suspend fun saveBuildingSitesAndFrontLoads(
        buildingSiteList: List<DbDriverBuildingSite>,
        frontLoadList: List<DbDriverFrontLoad>
    )

    suspend fun getBuildingSites(): List<BuildingSiteWithFrontLoads>

    suspend fun getBuildingSiteNameById(id: Int): String

    suspend fun saveMaterials(materials: List<DbMaterial>)

    suspend fun getMaterials(): List<DbMaterial>

    suspend fun getMaterialNameById(id: Int): String

    suspend fun getVehicle(): List<DbVehicle>

    suspend fun saveVehicle(vehicle: DbVehicle)

    suspend fun saveJourneyList(journeyList: List<DbJourney>): Boolean

    fun getJourneys(): LiveData<List<DbJourney>?>

    suspend fun getJourney(id : String): DbJourney?

    suspend fun updateJourneyInDb(journey: DbJourney)

    suspend fun updateJourneyStatus(id: String, status: String)

    suspend fun deleteJourney(id: String)

    suspend fun updateJourneyDriverStatus(id: String, status: String)

    suspend fun clearJourneyList()

    suspend fun saveLocation(location: DbVehicleLocation): Result<Long>

    suspend fun getPendingLocations(): List<DbVehicleLocation>

    suspend fun deleteLocation(id: Int): Int
}