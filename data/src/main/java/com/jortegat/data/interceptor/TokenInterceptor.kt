package com.jortegat.data.interceptor

import com.jortegat.domain.helper.PreferencesManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 *  @author jortegat
 */
class TokenInterceptor(
    private val preferencesManager: PreferencesManager
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if (!(isRoleRequest(request)
                    || isUserProfileRequest(request)
                    || isVehicleRequest(request)
                    || isInvitationRequest(request)
                    || isCompanyRequest(request)
                    || isBuildingSiteRequest(request)
                    || isPlaceRequest(request)
                    || isFrontLoadRequest(request)
                    || isWorkDayRequest(request)
                    || isMaterialRequest(request)
                    || isServiceRequest(request)
                    || isProgrammingServiceRequest(request)
                    || isJourneyRequest(request)
                    || isAttendantRequest(request)
                    || isFirebaseTokenRequest(request)
                    || isNewTokenRequest(request)
                    || isDriverRequest(request)
                    || isOwnerServiceRequest(request)
                    || isStartJourneyRequest(request)
                    || isDestinationGeopositionRequest(request)
                    || isLocationRequest(request)
                    || isNotificationRequest(request))
        ) {
            return chain.proceed(request)
        }

        val token = if (!isNewTokenRequest(request)) {
            preferencesManager.token()
        } else {
            preferencesManager.refreshToken()
        }
        request = request.newBuilder()
            .addHeader("Authorization", "Bearer $token").build()
        return chain.proceed(request)
    }

    private fun isRoleRequest(request: Request) =
        request.url().toString().contains("account/type", true)

    private fun isUserProfileRequest(request: Request) =
        request.url().toString().contains("account/profile", true)

    private fun isVehicleRequest(request: Request) =
        request.url().toString().contains("vehicle", true)

    private fun isInvitationRequest(request: Request) =
        request.url().toString().contains("truckDriverInvitation", true)

    private fun isFirebaseTokenRequest(request: Request) =
        request.url().toString().contains("notification/device", true)

    private fun isCompanyRequest(request: Request) =
        request.url().toString().contains("builder/company", true)

    private fun isBuildingSiteRequest(request: Request) =
        request.url().toString().contains("builder/buildingSite", true)

    private fun isPlaceRequest(request: Request) =
        request.url().toString().contains("builder/place", true)

    private fun isFrontLoadRequest(request: Request) =
        request.url().toString().contains("builder/frontload", true)

    private fun isWorkDayRequest(request: Request) =
        request.url().toString().contains("order/workday", true)

    private fun isMaterialRequest(request: Request) =
        request.url().toString().contains("builder/material", true)

    private fun isServiceRequest(request: Request) =
        request.url().toString().contains("order/service", true)

    private fun isProgrammingServiceRequest(request: Request) =
        request.url().toString().contains("order/program", true)

    private fun isJourneyRequest(request: Request) =
        request.url().toString().contains("order/journey", true)

    private fun isAttendantRequest(request: Request) =
        request.url().toString().contains("builder/attendant", true)

    private fun isNewTokenRequest(request: Request) =
        request.url().toString().contains("account/refresh/token", true)

    private fun isDriverRequest(request: Request) =
        request.url().toString().contains("truckDriver", true)

    private fun isOwnerServiceRequest(request: Request) =
        request.url().toString().contains("order/service/owner/me", true)

    private fun isStartJourneyRequest(request: Request) =
        request.url().toString().contains("/start", true)

    private fun isDestinationGeopositionRequest(request: Request) =
        request.url().toString().contains("geoposition/client", true)

    private fun isLocationRequest(request: Request) =
        request.url().toString().contains("route/journey", true)

    private fun isNotificationRequest(request: Request) =
        request.url().toString().contains("notification", true)
}