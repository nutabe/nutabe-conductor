package com.jortegat.data.mappers

import com.jortegat.base.models.on_demand_material.ApiOnDemandMaterial
import com.jortegat.base.models.on_demand_material.OnDemandMaterial
import com.jortegat.data.db.entity.material.DbMaterial

object MaterialMappers {

    fun mapListApiToDomain(apiMaterials: List<ApiOnDemandMaterial>): List<OnDemandMaterial> {
        val domainMaterials = mutableListOf<OnDemandMaterial>()
        apiMaterials.forEach {
            domainMaterials.add(mapApiToDomain(it))
        }

        return domainMaterials
    }

    private fun mapApiToDomain(apiMaterial: ApiOnDemandMaterial): OnDemandMaterial {
        return OnDemandMaterial(apiMaterial.id, apiMaterial.name, apiMaterial.type)
    }

    fun mapDbMaterialsToDomainMaterials(dbMaterials: List<DbMaterial>): List<OnDemandMaterial> {
        val domainMaterials = mutableListOf<OnDemandMaterial>()
        dbMaterials.forEach {
            domainMaterials.add(mapDbToDomain(it))
        }

        return domainMaterials
    }

    private fun mapDbToDomain(dbMaterial: DbMaterial): OnDemandMaterial {
        return OnDemandMaterial(dbMaterial.id, dbMaterial.name, dbMaterial.type)
    }

    fun mapApiMaterialsToDbMaterials(apiMaterials: List<ApiOnDemandMaterial>): List<DbMaterial> {
        val dbMaterials = mutableListOf<DbMaterial>()
        apiMaterials.forEach {
            dbMaterials.add(mapApiToDb(it))
        }

        return dbMaterials
    }

    private fun mapApiToDb(apiMaterial: ApiOnDemandMaterial): DbMaterial {
        return DbMaterial(apiMaterial.id, apiMaterial.name, apiMaterial.type)
    }
}