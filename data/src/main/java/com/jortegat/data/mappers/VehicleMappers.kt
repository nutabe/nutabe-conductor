package com.jortegat.data.mappers

import com.jortegat.base.helpers.convertUTCDateToLocal
import com.jortegat.base.models.domain.Vehicle
import com.jortegat.data.db.entity.vehicle.DbVehicle

object VehicleMappers {

    fun mapFromDb(dbVehicle: DbVehicle): Vehicle {
        return dbVehicle.run {
            Vehicle(
                id,
                type,
                volumen,
                soatEndDate,
                technomechanicalEndDate,
                policyEndDate,
                civilLiabilityEndDate,
                isVerified,
                isActive,
                createdAt
            )
        }
    }

    fun mapToDb(vehicle: Vehicle): DbVehicle {
        return vehicle.run {
            DbVehicle(
                id,
                type,
                volumen,
                soatEndDate,
                technomechanicalEndDate,
                policyEndDate,
                civilLiabilityEndDate,
                isVerified,
                isActive,
                createdAt
            )
        }
    }
}