package com.jortegat.data.mappers

import com.jortegat.base.helpers.convertUTCDateToLocal
import com.jortegat.data.driver.datasource.models.pending_notifications.ApiPendingInvitation
import com.jortegat.domain.driver.model.pending_invitation.PendingInvitation

object PendingInvitationsMappers {

    fun mapApiInvitationsToDomainInvitations(apiPendingInvitations: List<ApiPendingInvitation>): MutableList<PendingInvitation> {
        val invitations = mutableListOf<PendingInvitation>()
        apiPendingInvitations.forEach { it ->
            it.run {
                invitations.add(
                    PendingInvitation(
                        id, truckDriverIdentification, vehicleId, status, createdAt.convertUTCDateToLocal()
                    )
                )
            }
        }
        return invitations
    }
}