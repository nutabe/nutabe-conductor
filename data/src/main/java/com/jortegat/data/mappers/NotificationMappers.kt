package com.jortegat.data.mappers

import com.jortegat.base.helpers.convertUTCDateToLocal
import com.jortegat.data.driver.datasource.models.notifications.ApiLink
import com.jortegat.data.driver.datasource.models.notifications.ApiNotification
import com.jortegat.data.driver.datasource.models.notifications.ApiPayload
import com.jortegat.domain.driver.model.notifications.Notification
import com.jortegat.domain.driver.model.notifications.NotificationLink
import com.jortegat.domain.driver.model.notifications.Payload

object NotificationMappers {

    fun mapApiNotificationToDomainNotification(apiNotificationList: List<ApiNotification>): MutableList<Notification> {
        val notifications = mutableListOf<Notification>()
        apiNotificationList.forEach { it ->
            notifications.add(
                Notification(
                    it._id,
                    it.body,
                    it.createdAt.convertUTCDateToLocal(),
                    it.identification,
                    mapApiLinkToNotificationLink(it.link),
                    mapApiPayloadToPayload(it.payload),
                    it.status,
                    it.title
                )
            )
        }
        return notifications
    }

    private fun mapApiPayloadToPayload(apiPayload: ApiPayload?): Payload? {
        var payload: Payload? = null
        apiPayload?.let {
            payload = Payload(it.journeyId, it.truckDriverInvitationId, it.vehicleId)
        }
        return payload
    }

    private fun mapApiLinkToNotificationLink(apiLink: ApiLink?): NotificationLink? {
        var notificationLink: NotificationLink? = null
        apiLink?.let {
            it.run {
                notificationLink =
                    NotificationLink(_id, androidUrl, createdAt.convertUTCDateToLocal(), `package`, type, userType, webUrl)
            }

        }
        return notificationLink
    }
}