package com.jortegat.data.mappers

import com.jortegat.base.models.User
import com.jortegat.data.db.entity.DbUser

object UserMappers {

    fun mapDbToDomain(dbUser: DbUser): User =
        dbUser.run {
            User(
                name, lastName, identification, phone, role
            )
        }

    fun mapDomainToDb(user: User): DbUser =
        user.run {
            DbUser(
                name = name,
                lastName = lastName,
                identification = identification,
                phone = phone,
                role = role
            )
        }
}