package com.jortegat.data.mappers

import com.jortegat.base.helpers.convertUTCDateToLocal
import com.jortegat.base.models.Place
import com.jortegat.base.models.WorkDay
import com.jortegat.base.models.domain.CustomJourney
import com.jortegat.data.db.entity.journey.DbJourney
import com.jortegat.data.db.entity.journey.DbPlace
import com.jortegat.data.db.entity.material.DbMaterial
import com.jortegat.data.driver.datasource.models.service_response.ApiJourney
import com.jortegat.data.driver.datasource.models.service_response.ApiProgram
import com.jortegat.data.driver.datasource.models.service_response.ApiService
import com.jortegat.data.driver.datasource.models.service_response.ApiWorkday
import com.jortegat.domain.driver.model.services.Journey
import com.jortegat.domain.driver.model.services.JourneyMaterial

object ServiceMappers {

    fun mapApiServiceToDomainJourneys(apiServiceList: List<ApiService>): MutableList<Journey> {
        val journeys = mutableListOf<Journey>()
        apiServiceList.forEach { apiService ->
            apiService.run {
                apiPrograms.forEach { apiProgram ->
                    apiProgram.apiJourneys.forEach { apiJourney ->
                        journeys.add(
                            Journey(
                                journeyId = apiJourney.id,
                                serviceCreationDate = createdAt.convertUTCDateToLocal(),
                                serviceOrder = orderId,
                                vehicleId = apiJourney.vehicleId,
                                vehicleType = vehicleType,
                                serviceType = serviceType,
                                company = "",
                                destination = Place(
                                    identification = destination,
                                    "",
                                    "",
                                    0.0,
                                    0.0,
                                    isQuarry = false,
                                    isDump = false,
                                    isActive = true,
                                    type = "",
                                    createdAt = ""
                                ),
                                origin = Place(
                                    identification = origin,
                                    "",
                                    "",
                                    0.0,
                                    0.0,
                                    isQuarry = false,
                                    isDump = false,
                                    isActive = true,
                                    type = "",
                                    createdAt = ""
                                ),
                                material = JourneyMaterial(
                                    materialId,
                                    "",
                                    ""
                                ),
                                workdayId = apiProgram.apiWorkday.id, apiProgram.apiWorkday.name,
                                ownerId = apiJourney.ownerId,
                                ownerName = "",
                                journeyStatus = apiJourney.journeyStatus,
                                driverStatus = apiJourney.driverStatus,
                                date = apiProgram.date,
                                receiptName = apiJourney.receiptName,
                                receiptNumber = apiJourney.receiptNumber,
                                receiptDate = apiJourney.receiptDate,
                                remissionDispatcher = apiJourney.remissionDispatcher,
                                remissionNumber = apiJourney.remissionNumber,
                                remissionDate = apiJourney.remissionDate,
                                remissionLoad = apiJourney.load,
                                remissionLoadType = apiJourney.loadType ?: "M3",
                                orderGuide = apiJourney.guide,
                                hasVisitedOrigin = apiJourney.remissionNumber != null,
                                hasVisitedDestination = apiJourney.receiptNumber != null,
                                serial = apiJourney.serial
                            )
                        )

                    }

                }

            }
        }
        return journeys
    }

    fun mapInProgressJourneyToApiService(customJourney: CustomJourney): ApiService? {
        customJourney.program?.service?.let {
            return customJourney.run {
                ApiService(
                    it.createdAt,
                    it.createdBy,
                    it.destination,
                    it.destinationType,
                    it.id,
                    it.load,
                    it.materialId,
                    it.orderId,
                    it.origin,
                    it.originType,
                    apiPrograms = listOf(
                        ApiProgram(
                            null,
                            program!!.date,
                            program!!.id,
                            apiJourneys = listOf(
                                ApiJourney(
                                    createdAt,
                                    driverStatus,
                                    this.id,
                                    isActive,
                                    journeyStatus,
                                    ownerId,
                                    receiptName,
                                    receiptNumber,
                                    receiptDate,
                                    remissionDispatcher,
                                    remissionNumber,
                                    remissionDate,
                                    vehicleId,
                                    load,
                                    loadType,
                                    guide,
                                    serial
                                )
                            ),
                            mapWorkdayToApiWorkday(program!!.workday)
                        )
                    ),
                    it.serviceType,
                    it.vehicleType
                )
            }
        }
        return null
    }

    private fun mapWorkdayToApiWorkday(workday: WorkDay): ApiWorkday {
        workday.run {
            return ApiWorkday(
                createdAt,
                endHour,
                id,
                name,
                startHour
            )
        }
    }

    fun mapFromDb(localJourneys: List<DbJourney>?): List<Journey> {
        val result = mutableListOf<Journey>()
        localJourneys?.forEach {
            it.run {
                result.add(
                    Journey(
                        journeyId,
                        serviceCreationDate,
                        serviceOrder,
                        vehicleId,
                        vehicleType,
                        serviceType,
                        company,
                        mapFromDbPlace(destination),
                        mapFromDbPlace(origin),
                        mapFromDbMaterial(material),
                        workdayId,
                        workdayName,
                        ownerId,
                        ownerName,
                        journeyStatus,
                        driverStatus,
                        date,
                        receiptName,
                        receiptNumber,
                        receiptDate,
                        remissionDispatcher,
                        remissionNumber,
                        remissionDate,
                        remissionLoad,
                        remissionLoadType,
                        null,
                        hasVisitedOrigin,
                        hasVisitedDestination,
                        serial
                    )
                )
            }
        }
        return result
    }

    fun mapToDb(journey: Journey): DbJourney {
        return journey.run {
            DbJourney(
                journeyId,
                serviceCreationDate,
                serviceOrder,
                vehicleId,
                vehicleType,
                serviceType,
                company,
                mapToDbPlace(destination),
                mapToDbPlace(origin),
                mapToDbMaterial(material),
                workdayId,
                workdayName,
                ownerId,
                ownerName,
                journeyStatus,
                driverStatus,
                date,
                receiptName,
                receiptNumber,
                receiptDate,
                remissionDispatcher,
                remissionNumber,
                remissionDate,
                remissionLoad,
                remissionLoadType,
                hasVisitedOrigin,
                hasVisitedDestination,
                serial
            )
        }
    }

    fun mapToDb(journeyList: List<Journey>): List<DbJourney> {
        val result = mutableListOf<DbJourney>()
        journeyList.forEach {
            it.run {
                result.add(
                    DbJourney(
                        journeyId,
                        serviceCreationDate,
                        serviceOrder,
                        vehicleId,
                        vehicleType,
                        serviceType,
                        company,
                        mapToDbPlace(destination),
                        mapToDbPlace(origin),
                        mapToDbMaterial(material),
                        workdayId,
                        workdayName,
                        ownerId,
                        ownerName,
                        journeyStatus,
                        driverStatus,
                        date,
                        receiptName,
                        receiptNumber,
                        receiptDate,
                        remissionDispatcher,
                        remissionNumber,
                        remissionDate,
                        remissionLoad,
                        remissionLoadType,
                        hasVisitedOrigin,
                        hasVisitedDestination,
                        serial
                    )
                )
            }
        }
        return result
    }

    private fun mapFromDbMaterial(dbMaterial: DbMaterial): JourneyMaterial {
        dbMaterial.run {
            return JourneyMaterial(id, name, type)
        }
    }

    private fun mapFromDbPlace(dbPlace: DbPlace): Place {
        dbPlace.run {
            return Place(
                identification = identification,
                address = address,
                businessName = businessName,
                latitude = latitude,
                longitude = longitude,
                isQuarry = isQuarry,
                isDump = isDump,
                isActive = isActive,
                type = type,
                createdAt = createdAt
            )
        }
    }

    private fun mapToDbMaterial(material: JourneyMaterial): DbMaterial {
        material.run {
            return DbMaterial(id, name, type)
        }
    }

    private fun mapToDbPlace(place: Place): DbPlace {
        place.run {
            return DbPlace(
                identification = identification,
                address = address,
                businessName = businessName,
                latitude = latitude,
                longitude = longitude,
                isQuarry = isQuarry,
                isDump = isDump,
                isActive = isActive,
                type = type,
                createdAt = createdAt
            )
        }
    }
}