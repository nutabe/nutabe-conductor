package com.jortegat.data.mappers

import com.jortegat.base.helpers.convertUTCDateToLocal
import com.jortegat.data.db.entity.on_demand_services.DbDriverBuildingSite
import com.jortegat.data.db.entity.on_demand_services.DbDriverFrontLoad
import com.jortegat.data.db.entity.on_demand_services.relations.BuildingSiteWithFrontLoads
import com.jortegat.data.driver.datasource.models.create_on_demand_service.ApiDriverBuildingSite
import com.jortegat.data.driver.datasource.models.create_on_demand_service.ApiFrontLoad
import com.jortegat.data.driver.datasource.models.on_demand_service.ApiOnDemandDocument
import com.jortegat.data.driver.datasource.models.on_demand_service.ApiOnDemandService
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverBuildingSite
import com.jortegat.domain.driver.model.services.create_on_demand_service.DriverFrontLoad
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandDocument
import com.jortegat.domain.driver.model.services.create_on_demand_service.OnDemandService

object OnDemandServiceMappers {

    fun mapApiOnDemandServiceListToDomain(services: List<ApiOnDemandService>): MutableList<OnDemandService> {
        val onDemandServices = mutableListOf<OnDemandService>()
        services.forEach { service ->
            service.run {
                onDemandServices.add(
                    OnDemandService(
                        null,
                        buildingSiteId,
                        null,
                        materialId,
                        load,
                        serviceType,
                        date,
                        mapApiOnDemandDocumentToDomain(remission),
                        mapApiOnDemandDocumentToDomain(receipt),
                        mapApiOnDemandDocumentToDomain(supportDocument)
                    )
                )
            }
        }
        return onDemandServicesSorted(onDemandServices)

    }

    private fun onDemandServicesSorted(onDemandServices: MutableList<OnDemandService>): MutableList<OnDemandService> {
        return onDemandServices.sortedByDescending { (it.journeyDate) }.toMutableList()
    }

    private fun mapApiOnDemandDocumentToDomain(document: ApiOnDemandDocument?): OnDemandDocument? {
        if (document == null) return null
        return document.run {
            OnDemandDocument(comment, id, images, status, type)
        }
    }

    fun mapApiBuildingSitesToDomainBuildingSites(apiBuildingSites: List<ApiDriverBuildingSite>): List<DriverBuildingSite> {
        val buildingSites = mutableListOf<DriverBuildingSite>()
        apiBuildingSites.forEach { buildingSite ->
            buildingSite.run {
                buildingSites.add(
                    DriverBuildingSite(
                        mapApiFrontLoadToDomainFrontLoad(frontLoads),
                        id,
                        name
                    )
                )
            }
        }
        return buildingSites
    }

    private fun mapApiFrontLoadToDomainFrontLoad(apiFrontLoads: List<ApiFrontLoad>): List<DriverFrontLoad> {
        val frontLoads = mutableListOf<DriverFrontLoad>()
        apiFrontLoads.forEach { apiFrontLoad ->
            apiFrontLoad.run {
                frontLoads.add(
                    DriverFrontLoad(
                        assistant,
                        createdAt.convertUTCDateToLocal(),
                        id,
                        isActive,
                        isDump,
                        latitude,
                        location,
                        longitude
                    )
                )
            }
        }
        return frontLoads
    }

    fun mapApiBuildingSitesToDbBuildingSites(apiBuildingSites: List<ApiDriverBuildingSite>): List<DbDriverBuildingSite> {
        val buildingSites = mutableListOf<DbDriverBuildingSite>()
        apiBuildingSites.forEach { buildingSite ->
            buildingSite.run {
                buildingSites.add(
                    DbDriverBuildingSite(id, name)
                )
            }
        }
        return buildingSites
    }

    fun mapApiFrontLoadToDbFrontLoad(apiBuildingSites: List<ApiDriverBuildingSite>): List<DbDriverFrontLoad> {
        val frontLoads = mutableListOf<DbDriverFrontLoad>()
        apiBuildingSites.forEach { buildingSite ->
            buildingSite.frontLoads.forEach { apiFrontLoad ->
                apiFrontLoad.run {
                    frontLoads.add(
                        DbDriverFrontLoad(
                            id,
                            buildingSite.id,
                            assistant,
                            createdAt.convertUTCDateToLocal(),
                            isActive,
                            isDump,
                            latitude,
                            location,
                            longitude
                        )
                    )
                }

            }
        }
        return frontLoads
    }

    fun mapDbBuildingSitesToDomainBuildingSites(bdBuildingSite: List<BuildingSiteWithFrontLoads>): List<DriverBuildingSite> {
        val buildingSites = mutableListOf<DriverBuildingSite>()
        bdBuildingSite.forEach {
            it.run {
                buildingSites.add(
                    DriverBuildingSite(
                        mapDbFrontLoadToDomainFrontLoad(frontLoadList),
                        buildingSite.buildingSiteId,
                        buildingSite.name
                    )
                )
            }
        }
        return buildingSites
    }

    private fun mapDbFrontLoadToDomainFrontLoad(dbFrontLoads: List<DbDriverFrontLoad>): List<DriverFrontLoad> {
        val frontLoads = mutableListOf<DriverFrontLoad>()
        dbFrontLoads.forEach { dbFrontLoad ->
            dbFrontLoad.run {
                frontLoads.add(
                    DriverFrontLoad(
                        assistant,
                        createdAt,
                        frontLoadId,
                        isActive,
                        isDump,
                        latitude,
                        location,
                        longitude
                    )
                )
            }
        }
        return frontLoads
    }

}